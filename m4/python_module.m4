# From starpu/m4/libs.m4
# AC_PYTHON_MODULE(modulename, [action-if-found], [action-if-not-found])
# Check if the given python module is available
AC_DEFUN([AC_PYTHON_MODULE],
[
	AC_MSG_CHECKING(for python3 $1 module)
	echo "import $1" | $PYTHON - 2>/dev/null
	if test $? -ne 0 ; then
		AC_MSG_RESULT(no)
	   	$3
	else
		AC_MSG_RESULT(yes)
		$2
	fi
])

