(define-module (guix-modules memory-contention)
    #:use-module (gnu packages autotools)
    #:use-module (gnu packages mpi)
    #:use-module (gnu packages pkg-config)
    #:use-module (guix build-system gnu)
    #:use-module (guix git-download)
    #:use-module (guix licenses)
    #:use-module (guix packages)
    #:use-module (inria storm)
    #:use-module (gnu packages python)
    #:use-module (gnu packages python-build)
    #:use-module (gnu packages python-science)
    #:use-module (gnu packages python-xyz))

; This file is a template. Use package transformations like --with-commit or
; --with-source to use a recent version of memory-contention.
(define-public memory-contention
    (package
        (name "memory-contention")
        (version "0.1")
        (home-page "https://gitlab.inria.fr/pswartva/memory-contention")
        (source
            (origin
                (method git-fetch)
                (uri
                    (git-reference
                        (url "https://gitlab.inria.fr/pswartva/memory-contention.git")
                        (commit "29c54dbb880b21c67d3c24719ee0fd4be537248e")))
                (file-name (string-append name "-checkout"))
                (sha256
                    (base32 "1w0h97rkagw1gm3cxn54g4diqlh60r2rnf6kiyvl99b1f2fy7g75"))))
        (build-system gnu-build-system)
        (arguments
          '(#:out-of-source? #t
            #:phases (modify-phases %standard-phases
                                    (add-before 'install 'pre-install
                                                (lambda _ (setenv "SOURCE_DATE_EPOCH" "315532800") #t)))))
        (native-inputs (list autoconf pkg-config automake python-wrapper python-pip python-setuptools python-wheel))
        (propagated-inputs
            (list `(,hwloc "lib")
                  openmpi
                  starpu
                  python-pandas
                  python-scipy
                  python-numpy
                  python-matplotlib
                  python-pillow))
        (synopsis "Set of benchmarks and tools to measure interferences between computations and communications")
        (description
          "memory-contention is composed of a set of benchmarks to measure
          the possible interferences between computations executed by CPU cores
          and MPI communications. It contains also Python scripts to easily
          plot and analyze the results of these benchmarks.")
        (license gpl3)))
