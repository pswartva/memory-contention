#!/usr/bin/env python3

import sys
import csv
import re
import matplotlib.pyplot as plt

from memory_contention import *



def man():
    print("Plot metrics collected by pmu-tools, according to a parameter.")
    print("Specify files to plot")
    print("Filenames must have format: *_<cursor>.*")
    sys.exit(1)


def parse_options(options):
    files = []

    for opt in options:
        files.append(opt)

    return files


metric_units = dict()


def parse_file(filename):
    print(filename)
    regex = re.compile(r"^(.+)_(\d+).+$")
    m = regex.match(filename)
    cursor = -1
    prefix = None
    if m is not None:
        prefix = m.group(1)
        cursor = int(m.group(2))
    else:
        print("Failed to find the thread number.")
        sys.exit(0)
    assert(cursor > -1)

    data = dict()
    with open(filename) as f:
        reader = csv.reader(f, delimiter=';')
        next(reader)

        for row in reader:
            if len(row) > 1:
                data[row[1]] = float(row[2])

                if row[1] not in metric_units:
                    metric_units[row[1]] = row[3]

    return cursor, data


files = parse_options(sys.argv[1:])

if len(files) == 0:
    man()

data_cursor = dict()

for f in files:
    cursor, data = parse_file(f)

    assert(cursor not in data_cursor)
    data_cursor[cursor] = data


nb_files = len(files)
assert(len(data_cursor) == nb_files)

working_directory = get_working_directory(files[0])

old_data_cursor = data_cursor
max_x = max([x for x in data_cursor])
data_cursor = dict()
for x in old_data_cursor:
    data_cursor[x / max_x] = old_data_cursor[x]

x = sorted([v for v in data_cursor])
global_data = dict()

for c in x:
    for metric in data_cursor[c]:
        if metric not in global_data:
            global_data[metric] = {'x': [], 'y': []}

        global_data[metric]['x'].append(c)
        global_data[metric]['y'].append(data_cursor[c][metric])

fig, ax = plt.subplots()
ax.set(xlabel="← Memory-bound ⇹ CPU-bound →", ylabel="%", title="pmutools metrics")

for metric in global_data:
    if 2 < max(global_data[metric]['y']) <= 100 and metric.startswith("Backend_Bound"):
        plt.plot(global_data[metric]['x'], global_data[metric]['y'], label=metric + " (" + metric_units[metric].replace(" <", "") + ")")

ax.legend(prop={'size': 8})
ax.grid()
plt.savefig(os.path.join(working_directory, "cursor_pmutools.png"), dpi=100)
plt.show()
