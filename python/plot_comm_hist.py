#!/usr/bin/env python3

import matplotlib.pyplot as plt
import sys
import os

from memory_contention import *

HIST_MIN_OPT = "--min="
HIST_MAX_OPT = "--max="
TOP_OPT = "--top="
SHOW_OUTLINERS_OPT = "--show-outliners"
NB_BINS_OPT = "--nbins="

hist_min = None
hist_max = None
top = None
show_outliners = False
nb_bins = 100

def man():
    print("Plot the distribution of communication latencies during computation.")
    print("Specify file to plot")
    print("Options:")
    print(SLIDE_FORMAT_OPT)
    print(MERGE_FORMAT_OPT)
    print(NO_DISPLAY_OPT)
    print(HIST_MIN_OPT)
    print(HIST_MAX_OPT)
    print(OUTPUT_FILENAME_OPT)
    print(TITLE_OPT)
    print(SHOW_OUTLINERS_OPT)
    print(NB_BINS_OPT)
    print("\tNone or both 'hist_' options have to be provided.")
    print(TOP_OPT)
    print(HELP_OPT + " " + HELP_SHORT_OPT)
    sys.exit(1)


def parse_options(options):
    global slide_format
    global merge_format
    global filename
    global display_graph
    global hist_min
    global hist_max
    global top
    global user_title
    global user_figname
    global show_outliners
    global nb_bins

    for opt in options:
        if opt == SLIDE_FORMAT_OPT:
            slide_format = True
        elif opt == MERGE_FORMAT_OPT:
            merge_format = True
        elif opt == NO_DISPLAY_OPT:
            display_graph = False
        elif opt.startswith(HIST_MIN_OPT):
            hist_min = int(opt[len(HIST_MIN_OPT):])
        elif opt.startswith(HIST_MAX_OPT):
            hist_max = int(opt[len(HIST_MAX_OPT):])
        elif opt.startswith(TOP_OPT):
            top = int(opt[len(TOP_OPT):])
        elif opt.startswith(NB_BINS_OPT):
            nb_bins = int(opt[len(NB_BINS_OPT):])
        elif opt.startswith(TITLE_OPT):
            user_title = opt[len(TITLE_OPT):].replace('"', '').replace("'", "")
        elif opt == SHOW_OUTLINERS_OPT:
            show_outliners = True
        elif opt.startswith(OUTPUT_FILENAME_OPT):
            user_figname = opt[len(OUTPUT_FILENAME_OPT):]
        elif opt == HELP_OPT or opt == HELP_SHORT_OPT:
            man()
        else:
            filename = opt


if len(sys.argv) < 2:
    man()


parse_options(sys.argv[1:])

if filename is None \
        or (hist_min is None and hist_max is not None) or (hist_max is None and hist_min is not None) \
        or (hist_min is not None and hist_max is not None and hist_min >= hist_max):
    man()



comm_durations = None

with open(filename, 'r') as f:
    lines = f.readlines()
    _, comm_durations = get_comm_durations(lines)


working_directory = get_working_directory(filename)
figname = filename.split(".")[0]
title = get_title_prefix(working_directory) + "/" + figname
if user_title is not None:
    title = user_title
if user_figname is not None:
    figname = user_figname
else:
    figname += "_hist.png"

stats = compute_stats(comm_durations[1])

r = None
if hist_min is not None:
    assert(hist_max is not None)
    assert(hist_min < hist_max)
    r = (hist_min, hist_max)

durations = None
if show_outliners:
    durations = comm_durations[1]
else:
    durations = [d for d in comm_durations[1] if d >= stats['d1'] and d <= stats['d9']]
    # durations = [d for d in comm_durations[1] if d <= 650]

fig, ax = plt.subplots()
ax.hist(durations, range=r, bins=nb_bins, rwidth=0.8)
plt.xlabel("Communication latency (µs)")
plt.ylabel("Number of occurences")
plt.title(title)

if top is not None:
    ax.set_ylim(top=top)


if not slide_format:
    if merge_format:
        # medium size
        fig.set_size_inches(10, 6)
    else:
        # full size:
        fig.set_size_inches(15, 9)
    # slide format is the smallest format

plt.savefig(os.path.join(working_directory, figname), dpi=100)

if display_graph:
    plt.show()
