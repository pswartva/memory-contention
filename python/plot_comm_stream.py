#!/usr/bin/env python3

import matplotlib.pyplot as plt
import sys
import os
import re

from memory_contention import *

SHOW_STREAM_RESULTS_OPT = "--show-stream"

show_stream = False


def man():
    print("Plot each communication duration and distinguish phases (without STREAM running, with STREAM running, and without STREAM running because it already finished).")
    print("Specify file to plot")
    print("Options:")
    print(SLIDE_FORMAT_OPT)
    print(MERGE_FORMAT_OPT)
    print(TOP_OPT)
    print(BOTTOM_OPT)
    print(STREAM_TOP_OPT)
    print(NO_DISPLAY_OPT)
    print(SHOW_STREAM_RESULTS_OPT)
    print(HELP_OPT + " " + HELP_SHORT_OPT)
    sys.exit(1)


def parse_options(options):
    global slide_format
    global merge_format
    global top_limit
    global bottom_limit
    global stream_top_limit
    global filename
    global display_graph
    global show_stream

    for opt in options:
        if opt == SLIDE_FORMAT_OPT:
            slide_format = True
        if opt == MERGE_FORMAT_OPT:
            merge_format = True
        elif opt.startswith(TOP_OPT):
            top_limit = int(opt[len(TOP_OPT):])
        elif opt.startswith(BOTTOM_OPT):
            bottom_limit = int(opt[len(BOTTOM_OPT):])
        elif opt.startswith(STREAM_TOP_OPT):
            stream_top_limit = int(opt[len(STREAM_TOP_OPT):])
        elif opt == NO_DISPLAY_OPT:
            display_graph = False
        elif opt == SHOW_STREAM_RESULTS_OPT:
            show_stream = True
        elif opt == HELP_OPT or opt == HELP_SHORT_OPT:
            man()
        else:
            filename = opt

if len(sys.argv) < 2:
    man()


parse_options(sys.argv[1:])

if filename is None:
    man()


results = []
label_legends = []
stream_results = {}
nb = 0
with open(filename, 'r') as f:
    lines = f.readlines()
    pattern = re.compile('^\d+\.\d* us')
    step = []
    previous_line = None

    for line in lines:
        m = pattern.match(line)
        if m is not None:
            if previous_line is not None and len(step) == 0:
                label_legends.append(previous_line.strip())
            splitted = line.strip().split(" | ")
            step.append(float(splitted[0].split()[0].strip()))
            nb += 1
        else:
            results.append(step)
            step = []
            previous_line = line

    if show_stream:
        stream_results = get_stream_results(lines)


if len(step) > 0:  # if there isn't a line after the last data, we need to save it:
    results.append(step)

if nb == 0:
    print("No interesting line found. Exiting...")
    sys.exit(1)


working_directory = get_working_directory(filename)
figname = filename.split(".")[0]
title = get_title_prefix(working_directory) + "/" + figname



color_iter = iter(plt.rcParams["axes.prop_cycle"].by_key()["color"])
fig, ax = plt.subplots()
ax.set(xlabel='Iteration', ylabel='Communication duration (µs)', title=title)

start_x = 0
i = 0
legends = []
for step in results:
    if len(step) > 0:
        legends.append(plt.plot(range(start_x, start_x+len(step)), step, '.', color=next(color_iter), label=label_legends[i]))
        start_x += len(step)
        i += 1

if top_limit is not None:
    ax.set_ylim(top=top_limit)
if bottom_limit is not None:
    ax.set_ylim(bottom=bottom_limit)

final_legend = legends[0]
for l in legends[1:]:
    final_legend += l

ax.legend(final_legend, [l.get_label() for l in final_legend])



if show_stream:
## Plot Stream results (embedded plot):
    width = 0.35
    ax = plt.axes([.2, .6, .2, .2])
    ax.bar(
        [i - (width / 2) for i in range(4)],
        [stream_results["without_comm"][op][1] for op in ["copy", "scale", "add", "triad"]],
        width,
        label="Without communication",
        yerr=[
            [stream_results["without_comm"][op][1]-stream_results["without_comm"][op][0] for op in ["copy", "scale", "add", "triad"]],
            [stream_results["without_comm"][op][2]-stream_results["without_comm"][op][1] for op in ["copy", "scale", "add", "triad"]]
        ],
        color=next(color_iter)
    )
    ax.bar(
        [i + (width / 2) for i in range(4)],
        [stream_results["with_comm"][op][1] for op in ["copy", "scale", "add", "triad"]],
        width,
        label="With communications",
        yerr=[
            [stream_results["with_comm"][op][1]-stream_results["with_comm"][op][0] for op in ["copy", "scale", "add", "triad"]],
            [stream_results["with_comm"][op][2]-stream_results["with_comm"][op][1] for op in ["copy", "scale", "add", "triad"]]
        ],
        color=next(color_iter)
    )
    ax.set_xticks([0, 1, 2, 3])
    ax.set_xticklabels(["Copy", "Scale", "Add", "Triad"])
    if stream_top_limit is not None:
        ax.set_ylim(top=stream_top_limit)
    plt.xlabel("Operation")
    plt.ylabel("Memory Bandwidth (MB/s)")
    plt.title("STREAM results")
    plt.legend(loc="upper center", bbox_to_anchor=(0.5, -0.24))


if not slide_format:
    if merge_format:
        # medium size
        fig.set_size_inches(10, 6)
    else:
        # full size:
        fig.set_size_inches(15, 9)
    # slide format is the smallest format

plt.savefig(os.path.join(working_directory, figname), dpi=100)

if display_graph:
    plt.show()
