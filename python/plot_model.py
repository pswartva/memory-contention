#!/usr/bin/env python3

import argparse
import glob
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import statistics

from memory_contention import *


def get_model(comp_kernel, numa_node, nb_cores_to_consider):
    files_for_model = [
        f"{comp_kernel}/comp_{numa_node}_comm_{numa_node}_{i}_threads.out"
        for i in range(1, nb_cores_to_consider+1)
    ]
    return CommCompModel(files_for_model)


def compute_error(observation, model):
    return statistics.mean(map(lambda o, m: abs(o-m)/o, observation, model))


cli_parser = argparse.ArgumentParser()
cli_parser.add_argument("comp_kernel", help="computing kernel - folder where the input files are stored")
cli_parser.add_argument("hwloc_file", help="hwloc XML file")
cli_parser.add_argument('--model', action=argparse.BooleanOptionalAction, help="Wether to generate or not the model", default=True)
cli_parser.add_argument('--only-first-socket', action=argparse.BooleanOptionalAction, help="Wether to plot or not only cores of first socket", default=True)
cli_parser.add_argument('--combine', action=argparse.BooleanOptionalAction, help="Wether to combine or not all plots on the same picture", default=False)
cli_parser.add_argument('--print-values', action=argparse.BooleanOptionalAction, help="Wether to print values plotted on graphs", default=False)
cli_parser.add_argument('--alone', action=argparse.BooleanOptionalAction, help="Wether to display curves of lonely executions", default=True)
cli_parser.add_argument('--comp', action=argparse.BooleanOptionalAction, help="Wether to display curves of computations", default=True)
cli_parser.add_argument('--comment', type=str, help="Comment to display on the figure", default=None)
cli_parser.add_argument('--output', type=str, help="Name of the output figure file", default=None)
cli_args = cli_parser.parse_args()

if cli_args.model and not cli_args.only_first_socket:
    print("Can't combine options --model and --no-only-first-socket")
    sys.exit(1)


print("** Parsing Hwloc topology...")

topo = HwlocTopology(cli_args.hwloc_file, levels_to_remove=["L3Cache"])

print(f"{topo.nb_sockets} sockets")
print(f"{topo.nb_numa_nodes_total} NUMA nodes")
print(f"{topo.nb_cores_total} cores")

nb_cores_to_consider = topo.nb_cores_per_numa_node * topo.nb_numa_nodes_per_socket
if not cli_args.only_first_socket:
    nb_cores_to_consider *= topo.nb_sockets
if topo.nb_numa_nodes_total == 1 or not cli_args.only_first_socket:
    nb_cores_to_consider -= 1  # one core is used by communication thread
print(f"Will consider the performance with up to {nb_cores_to_consider} cores")


picture_file_suffix = ".png"
if cli_args.model:
    picture_file_suffix = "_model.png"

    print("** Modeling local accesses...")

    model_local = get_model(cli_args.comp_kernel, 0, nb_cores_to_consider)
    model_local.print_params()

    model_remote = None
    if topo.nb_numa_nodes_total > 1:
        print("** Modeling remote accesses...")
        try:
            model_remote = get_model(cli_args.comp_kernel, topo.nb_numa_nodes_per_socket, nb_cores_to_consider)
            model_remote.print_params()
        except Exception as e:
            print(f"Can't model remote accesses: {e}")

    combined_model = CombinedCommCompModel(topo, model_local, model_remote)
else:
    print("** Skipping model building")
    combined_model = None


if cli_args.output is not None:
    picture_file_suffix = f"_{cli_args.output}{picture_file_suffix}"


print("** Modeling all placement combinaisons...")

max_comp = 0
max_comm = 0
graphs = []
comm_errors = []
comm_errors_sample = []
comp_errors = []
comp_errors_sample = []

# Increase font size of all plots:
if cli_args.combine:
    default_size = plt.rcParams["figure.figsize"]
    if topo.nb_numa_nodes_total == 4:
        sizes = {
            "SMALL_SIZE": 17,
            "MEDIUM_SIZE": 17,
            "LEGEND_SIZE": 19,
            "LEFT_LABEL_X": 0.09,
            "RIGHT_LABEL_X": 0.94,
            "LEGEND_Y": -0.07,
            "X_LABEL_Y": 0.07,
            "COMMENT_Y": 0.01,
            "FIG_WIDTH": default_size[0] * 3,
            "FIG_HEIGHT": default_size[1] * 3,
        }
    elif topo.nb_numa_nodes_total == 2:
        sizes = {
            "SMALL_SIZE": 14,
            "MEDIUM_SIZE": 14,
            "LEGEND_SIZE": 14,
            "LEFT_LABEL_X": 0.07,
            "RIGHT_LABEL_X": 0.96,
            "LEGEND_Y": -0.15,
            "X_LABEL_Y": 0.05,
            "COMMENT_Y": -0.04,
            "FIG_WIDTH": default_size[0] * 1.5,
            "FIG_HEIGHT": default_size[1] * 1.5,
        }
    else:
        print("Provide figure sizes for this number of NUMA nodes !")
        sys.exit(1)
else:
    sizes = {
        "SMALL_SIZE": 11,
        "MEDIUM_SIZE": 12,
        "LEGEND_SIZE": 10,
    }
plt.rc('font', size=sizes["SMALL_SIZE"])          # controls default text sizes
plt.rc('axes', titlesize=sizes["SMALL_SIZE"])     # fontsize of the axes title
plt.rc('axes', labelsize=sizes["MEDIUM_SIZE"])    # fontsize of the x and y labels
plt.rc('xtick', labelsize=sizes["SMALL_SIZE"])    # fontsize of the tick labels
plt.rc('ytick', labelsize=sizes["SMALL_SIZE"])    # fontsize of the tick labels
plt.rc('legend', fontsize=sizes["LEGEND_SIZE"])    # legend fontsize
plt.rc('legend', title_fontsize=sizes["LEGEND_SIZE"])    # legend fontsize
plt.rc('figure', titlesize=sizes["MEDIUM_SIZE"])  # fontsize of the figure title


MARKER_SIZE = 5
MARKER_SIZE_PAR = 7
MARKER = "o"
MARKER_PAR = "v"

if cli_args.combine:
    fig, axs = plt.subplots(topo.nb_numa_nodes_total, topo.nb_numa_nodes_total, sharex=True, sharey=True, gridspec_kw = {'wspace':0.05, 'hspace':0.27})

all_values = []

for numa_comp in range(topo.nb_numa_nodes_total):
    graphs.append([])
    for numa_comm in range(topo.nb_numa_nodes_total):
        print(f"Data for computations on NUMA node #{numa_comp} and data for communications on NUMA node #{numa_comm}")

        all_files = [
            f"{cli_args.comp_kernel}/comp_{numa_comp}_comm_{numa_comm}_{i}_threads.out"
            for i in range(1, nb_cores_to_consider+1)
        ]

        try:
            parser = FilesParser(all_files)
        except Exception as e:
            print(f"Error in parsing files: {e}")
            if len(graphs) > 0 and len(graphs[0]) > 0:
                graphs[numa_comp].append(None)
                continue
            else:
                raise Exception("Can't find file for first configuration, can't continue.")

        real_x_values = parser.x_values[:nb_cores_to_consider]

        if cli_args.combine:
            plot_title = f"Comp. data on NUMA node {numa_comp}\nComm. data on NUMA node {numa_comm}"
        else:
            plot_title = f"Comp. data on NUMA node {numa_comp} - Comm. data on NUMA node {numa_comm}"

        x_span_zone = topo.nb_cores_per_numa_node if topo.nb_numa_nodes_per_socket > 1 else None
        title_fontweight = "bold" if cli_args.model and combined_model.is_sample(numa_comm, numa_comp) else "normal"
        graph = CommCompGraph(
            real_x_values,
            parser.x_type,
            CommCompGraphCommType.BANDWIDTH,
            parser.compute_bench_type,
            parser.compute_bench_type.default_metric,
            title=plot_title,
            title_fontweight=title_fontweight,
            comp_legend_title=f"Computation Bandwidth - {cli_args.comp_kernel}",
            comm_legend_title="Network Bandwidth",
            legend_fontsize=sizes["LEGEND_SIZE"],
            x_span_zone=x_span_zone,
            giga_bw=True,
            fig=fig if cli_args.combine else None,
            ax=axs[numa_comm,numa_comp] if cli_args.combine else None,
            ax_right=axs[numa_comm,numa_comp].twinx() if cli_args.combine else None)

        if cli_args.alone:
            if len(parser.comm_bw_alone_results) > 0:
                comm_alone_upper_values = [parser.comm_bw_alone_results[x]['d9'] for x in real_x_values]
                values = [parser.comm_bw_alone_results[x]['med'] for x in real_x_values]
                all_values.append({
                    "stream type": "comm",
                    "run": "alone    ",
                    "type": "measure",
                    "numa_comm": numa_comm,
                    "numa_comp": numa_comp,
                    "values": values,
                })
                max_comm = max(max_comm, max(comm_alone_upper_values))
                graph.add_comm_curve(
                    values,
                    "Measure (without Computations)",
                    CommCompGraphCurveType.ALONE,
                    False,
                    [parser.comm_bw_alone_results[x]['d1'] for x in real_x_values],
                    comm_alone_upper_values,
                    display_line=False,
                    markersize=MARKER_SIZE,
                    marker=MARKER
                )
            else:
                all_values.append({
                    "stream type": "comm",
                    "run": "alone    ",
                    "type": "measure",
                    "numa_comm": numa_comm,
                    "numa_comp": numa_comp,
                    "value": parser.comm_bw_with_comp_results[1]['med'],
                })
                graph.add_comm_curve(
                    [parser.comm_bw_with_comp_results[1]['med']] * len(real_x_values),
                    "Measure (without Computations)",
                    CommCompGraphCurveType.ALONE,
                    False,
                    display_line=False,
                    markersize=MARKER_SIZE,
                    marker=MARKER
                )
                max_comm = max(max_comm, parser.comm_bw_with_comp_results[1]['med'])

        comm_with_comp_values = [parser.comm_bw_with_comp_results[x]['med'] for x in real_x_values]
        comm_with_comp_upper_values = [parser.comm_bw_with_comp_results[x]['d9'] for x in real_x_values]
        max_comm = max(max_comm, max(comm_with_comp_upper_values))
        all_values.append({
            "stream type": "comm",
            "run": "with comp",
            "type": "measure",
            "numa_comm": numa_comm,
            "numa_comp": numa_comp,
            "values": comm_with_comp_values,
        })
        graph.add_comm_curve(
            comm_with_comp_values,
            "Measure (with Computations)",
            CommCompGraphCurveType.PARALLEL,
            True,
            [parser.comm_bw_with_comp_results[x]['d1'] for x in real_x_values],
            comm_with_comp_upper_values,
            display_line=False,
            markersize=MARKER_SIZE_PAR,
            marker=MARKER_PAR
        )

        if cli_args.model:
            comm_values_model = combined_model.comm_with_comp(numa_comm, numa_comp)
            all_values.append({
                "stream type": "comm",
                "run": "with comp",
                "type": "model  ",
                "numa_comm": numa_comm,
                "numa_comp": numa_comp,
                "values": comm_values_model,
            })
            graph.add_comm_curve(
                comm_values_model,
                "Model (with Computations)",
                CommCompGraphCurveType.ALONE,
                True,
                display_line=True
            )

            error = compute_error(comm_with_comp_values, comm_values_model)
            print(f"Communication model error: {error*100:3.2f}%")
            if combined_model.is_sample(numa_comm, numa_comp):
                comm_errors_sample.append(error)
            else:
                comm_errors.append(error)

        op = "memset"  # TODO: handle other cases
        metric = parser.compute_bench_type.default_metric
        if cli_args.comp:
            comp_alone_upper_values = [parser.comp_alone_results[op][x][metric]['max'] for x in real_x_values]
            max_comp = max(max_comp, max(comp_alone_upper_values))
            if cli_args.alone:
                values = [parser.comp_alone_results[op][x][metric]['avg'] for x in real_x_values]
                all_values.append({
                    "stream type": "comp",
                    "run": "alone    ",
                    "type": "measure",
                    "numa_comm": numa_comm,
                    "numa_comp": numa_comp,
                    "values": values,
                })
                graph.add_comp_curve(
                    values,
                    "Measure (without Communications)",
                    CommCompGraphCurveType.ALONE,
                    False,
                    [parser.comp_alone_results[op][x][metric]['min'] for x in real_x_values],
                    comp_alone_upper_values,
                    display_line=False,
                    markersize=MARKER_SIZE,
                    marker=MARKER
                )

            comp_with_comm_values = [parser.comp_with_comm_results[op][x][metric]['avg'] for x in real_x_values]
            all_values.append({
                "stream type": "comp",
                "run": "with comm",
                "type": "measure",
                "numa_comm": numa_comm,
                "numa_comp": numa_comp,
                "values": comp_with_comm_values,
            })
            graph.add_comp_curve(
                comp_with_comm_values,
                "Measure (with Communications)",
                CommCompGraphCurveType.PARALLEL,
                cli_args.alone,
                [parser.comp_with_comm_results[op][x][metric]['min'] for x in real_x_values],
                [parser.comp_with_comm_results[op][x][metric]['max'] for x in real_x_values],
                display_line=False,
                markersize=MARKER_SIZE_PAR,
                marker=MARKER_PAR
            )

            if cli_args.model:
                comp_values_model = combined_model.comp_with_comm(numa_comm, numa_comp)
                total_model = combined_model.total(numa_comp)
                all_values.append({
                    "stream type": "comp",
                    "run": "with comm",
                    "type": "model  ",
                    "numa_comm": numa_comm,
                    "numa_comp": numa_comp,
                    "values": comp_values_model,
                })
                graph.add_comp_curve(
                    comp_values_model,
                    "Model (with Communications)",
                    CommCompGraphCurveType.ALONE,
                    True,
                    display_line=True
                )

                error = compute_error(comp_with_comm_values, comp_values_model)
                print(f"Computation model error: {error*100:3.2f}%")
                if combined_model.is_sample(numa_comm, numa_comp):
                    comp_errors_sample.append(error)
                else:
                    comp_errors.append(error)

        if combined_model is not None and combined_model.is_sample(numa_comm, numa_comp):
            ax = graph.gca()
            for axis in ['top', 'bottom', 'left', 'right']:
                ax.spines[axis].set_linewidth(2)

        graphs[numa_comp].append(graph)

        # Stacked plot:
        if cli_args.comp:
            colors = iter(plt.rcParams["axes.prop_cycle"].by_key()["color"])
            next(colors)  # skip the two already used colors
            next(colors)
            fig_stack = plt.figure(1024, clear=True)
            ax = fig_stack.gca()
            plt.stackplot(
                real_x_values,
                [[parser.comp_with_comm_results[op][x][metric]['avg'] for x in real_x_values], [parser.comm_bw_with_comp_results[x]['med'] for x in real_x_values]],
                labels=["Computations (with communications)", "Communications (with computations)"],
                colors=[graphs[0][0].last_comp_color, graphs[0][0].last_comm_color],
            )
            plt.plot(real_x_values, [parser.comp_alone_results[op][x][metric]['avg'] for x in real_x_values], color=next(colors), label="Computations (alone)")
            if cli_args.model:
                plt.plot(real_x_values, comp_values_model, MARKER_PAR, color=next(colors), label="Computations (with communications) - model")
                plt.plot(real_x_values, [sum(x) for x in zip(comp_values_model, comm_values_model)], MARKER_PAR, color=next(colors), label="Communications (with computations) - model")
                plt.plot(real_x_values, total_model, MARKER_PAR, color=next(colors), label="Total - model")
            handles, labels = ax.get_legend_handles_labels()
            legend_order = [1, 2, 0]
            ax.legend([handles[i] for i in legend_order], [labels[i] for i in legend_order], loc="upper center", bbox_to_anchor=(0.5, -0.15), ncol=2, fontsize=10)
            ax.grid(axis='y')
            ax.xaxis.set_major_locator(MaxNLocator(integer=True))
            ax.yaxis.set_major_formatter(CommCompGraph.mega2giga_formatter)
            ax.set_axisbelow(True)
            ax.set(title=plot_title, xlabel="Number of computing Cores", ylabel="Memory Bandwidth (GB/s)")
            plt.subplots_adjust(bottom=0.22)
            plt.savefig(f"{cli_args.comp_kernel}/comp_{numa_comp}_comm_{numa_comm}_stacked{picture_file_suffix}")


print("** Saving all images...")

max_comp *= 1.1
max_comm *= 1.1

merged_image = ImageMerger(f"{cli_args.comp_kernel}/merged{picture_file_suffix}", topo.nb_numa_nodes_total**2, topo.nb_numa_nodes_total)
if cli_args.comp:
    merged_stacked_image = ImageMerger(f"{cli_args.comp_kernel}/merged_stack{picture_file_suffix}", topo.nb_numa_nodes_total**2, topo.nb_numa_nodes_total)

handles_left = None
labels_left = None
handles_right = None
labels_right = None

for numa_comp in range(topo.nb_numa_nodes_total):
    for numa_comm in range(topo.nb_numa_nodes_total):
        figname = f"{cli_args.comp_kernel}/comp_{numa_comp}_comm_{numa_comm}{picture_file_suffix}"

        if graphs[numa_comp][numa_comm] is not None:
            graphs[numa_comp][numa_comm].comm_top_limit = max_comm
            graphs[numa_comp][numa_comm].comp_top_limit = max_comp

            if cli_args.comp:
                merged_stacked_image[numa_comm, numa_comp] = f"{cli_args.comp_kernel}/comp_{numa_comp}_comm_{numa_comm}_stacked{picture_file_suffix}"

            if cli_args.combine:
                graphs[numa_comp][numa_comm]._plot()
                axs[numa_comp,numa_comm].label_outer()
                if numa_comp != (topo.nb_numa_nodes_total-1):
                    graphs[numa_comp][numa_comm].ax_right.yaxis.set_tick_params(which="both", labelright=False)
                graphs[numa_comp][numa_comm].ax.set_xlabel(None)
                graphs[numa_comp][numa_comm].ax.set_ylabel(None)
                graphs[numa_comp][numa_comm].ax_right.set_ylabel(None)

                handles_left, labels_left = axs[numa_comp, numa_comp].get_legend_handles_labels()
                handles_right, labels_right = graphs[numa_comp][numa_comm].ax_right.get_legend_handles_labels()
            else:
                graphs[numa_comp][numa_comm].save(figname)

                merged_image[numa_comm, numa_comp] = figname

if cli_args.combine:
    fig.set_size_inches(sizes["FIG_WIDTH"], sizes["FIG_HEIGHT"])
    fig.text(0.5, sizes["X_LABEL_Y"], graphs[0][0].x_label, va='center', ha='center', fontsize=sizes["LEGEND_SIZE"])
    fig.text(sizes["LEFT_LABEL_X"], 0.5, graphs[0][0].left_y_label, va='center', ha='center', rotation='vertical', fontsize=sizes["LEGEND_SIZE"], color=graphs[0][0].last_comm_color)
    fig.text(sizes["RIGHT_LABEL_X"], 0.5, graphs[0][0].right_y_label, va='center', ha='center', rotation='vertical', fontsize=sizes["LEGEND_SIZE"], color=graphs[0][0].last_comp_color)
    if cli_args.comment is not None and cli_args.comment != "":
        fig.text(0.5, sizes["COMMENT_Y"], cli_args.comment, va='center', ha='center', fontsize=sizes["LEGEND_SIZE"])

    leg = fig.legend(handles_left, labels_left, loc='lower left', bbox_to_anchor=(0.05, sizes["LEGEND_Y"]), title=graphs[0][0].comm_legend_title)
    leg._legend_box.align = "left"
    leg = fig.legend(handles_right, labels_right, loc='lower right', bbox_to_anchor=(0.975, sizes["LEGEND_Y"]), title=graphs[0][0].comp_legend_title, markerfirst=False)
    leg._legend_box.align = "right"

    plt.savefig(f"{cli_args.comp_kernel}/combined{picture_file_suffix}", bbox_inches="tight")
else:
    merged_image.save()

if cli_args.comp:
    merged_stacked_image.save()

if cli_args.model:
    print("** Error of the model:")

    print(f"Communication model on sample executions: {statistics.mean(comm_errors_sample)*100:3.2f}%")
    if len(comm_errors) > 0:
        print(f"Communication model on non-sample executions: {statistics.mean(comm_errors)*100:3.2f}%")
    print(f"Communication model on all executions: {statistics.mean(comm_errors_sample+comm_errors)*100:3.2f}%")

    if len(comp_errors_sample) > 0:
        print(f"Computation model on sample executions: {statistics.mean(comp_errors_sample)*100:3.2f}%")
        if len(comp_errors) > 0:
            print(f"Computation model on non-sample executions: {statistics.mean(comp_errors)*100:3.2f}%")
        print(f"Computation model on all executions: {statistics.mean(comp_errors_sample+comp_errors)*100:3.2f}%")

if cli_args.print_values:
    print("** All values:")
    for row in all_values:
        v = row["values"]
        del row["values"]
        print(row, list(map(lambda n: f"{n:7.1f}", v)))
