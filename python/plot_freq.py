#!/usr/bin/env python3

import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
import numpy as np
import sys

TITLE_OPT = "--title="
HELP_SHORT_OPT = "-h"
HELP_LONG_OPT = "--help"
START_X_OPT = "--start="
END_X_OPT = "--end="
OUTPUT_FILENAME_OPT = "--o="

user_title = None
start_x = 0
end_x = 0
user_figname = "freq.png"


def man():
    print("Plot the obtained frequencies with --freq option of bench binary.")
    print("Specify file to plot")
    print(TITLE_OPT)
    print(START_X_OPT)
    print(END_X_OPT)
    print(HELP_LONG_OPT, HELP_SHORT_OPT)
    print(OUTPUT_FILENAME_OPT)
    sys.exit(1)


def parse_options(options):
    global user_title
    global start_x
    global end_x
    global user_figname
    filename = None

    for opt in options:
        if opt.startswith(TITLE_OPT):
            user_title = opt[len(TITLE_OPT):].replace('"', '').replace("'", "")
        elif opt.startswith(START_X_OPT):
            start_x = int(opt[len(START_X_OPT):])
        elif opt.startswith(END_X_OPT):
            end_x = int(opt[len(END_X_OPT):])
        elif opt == HELP_LONG_OPT or opt == HELP_SHORT_OPT:
            man()
        elif opt.startswith(OUTPUT_FILENAME_OPT):
            user_figname = opt[len(OUTPUT_FILENAME_OPT):]
        elif filename is None:
            filename = opt
        else:
            print("Warning; ignored option '" + opt + "'")

    if filename is None:
        man()

    return filename


class IterLineStyle:
    def __init__(self):
        self.color_id = 0
        self.ls_id = 0

    def __next__(self):
        ls = ['-', '--', '-.', ':']
        colors = plt.rcParams["axes.prop_cycle"].by_key()["color"]
        v = {'c': colors[self.color_id], 'ls': ls[self.ls_id]}

        self.color_id += 1
        if self.color_id == len(colors):
            self.color_id = 0
            self.ls_id += 1

            if self.ls_id == len(ls):
                self.ls_id = 0

        return v


def avg(l):
    s = 0
    for e in l:
        s += e

    return s / len(l)


if len(sys.argv) < 2:
    man()

filename = parse_options(sys.argv[1:])

header = None
probe_interval = None  # ms
binding = None
values = []
events = dict()  # t_id -> str

with open(filename, 'r') as f:
    lines = f.readlines()
    t_id = 0

    for l in lines:
        if l.startswith("[freq][header] "):
            header = l[len("[freq][header] "):].strip().split(",")
        elif l.startswith("[freq][probe_interval] "):
            probe_interval = int(int(l[len("[freq][probe_interval] "):].strip()) / 1000)
        elif l.startswith("[freq][binding] "):
            binding = l[len("[freq][binding] "):].strip().split(",")
        elif l.startswith("[freq][values] "):
            values.append([int(v) for v in l[len("[freq][values] "):].strip().split(",")])
            t_id += 1
        elif l.startswith("[freq][event] "):
            events[t_id] = l[len("[freq][event] "):].strip()
        else:
            print("Warning: ignored line '%s'".format(l))


curves = []

for i in range(len(header)):
    label = header[i]
    if i < len(binding) and len(binding[i]) > 0:
        label += " (" + binding[i] + ")"

    curves.append({
        'label': label,
        'values': [v[i] for v in values]
    })

line_styles = IterLineStyle()
fig, ax = plt.subplots()
title = "Frequencies"
if user_title is not None:
    title = user_title
ax.set(xlabel="Time (ms)", ylabel="Frequency (MHz)", title=title)

merged_curves = []
merges = [
    {'curves': range(0, 16), 'label': 'Cores 0-15 (OpenMP workers, socket 0)'},
    {'curves': range(16, 20), 'label': 'Core 16-19 (OpenMP workers, socket 1)'},
    {'curves': range(20, 34), 'label': 'Cores 20-33 (idle, socket 1)'},
    {'curves': [34], 'label': 'Core 34 (PingPong, socket 1)'},
    {'curves': [35], 'label': 'Core 35 (FreqMeter, socket 1)'},
    {'curves': [36], 'label': 'Uncore 0'},
    # {'curves': [37], 'label': 'Uncore 1'}
]

for m in merges:
    plt.plot(
        range(0, len(values)*probe_interval, probe_interval),
        [avg([curves[c]['values'][i] for c in m['curves']]) for i in range(len(values))],
        label=m['label'], **next(line_styles)
    )

"""
for c in curves:
    plt.plot(range(0, len(c['values'])*probe_interval, probe_interval), c['values'], label=c['label'], **next(line_styles))
"""

for e in events:
    print(events[e], e, "(", e*probe_interval, ")")
    plt.axvline(x=e*probe_interval, color='black', ls='--')


"""
annotations = [
    {'start': 22250, 'end': 22300, 'label': 'A'},
    {'start': 22300, 'end': 32050, 'label': 'B'},
    {'start': 32050, 'end': 34150, 'label': 'C'}
]

for a in annotations:
    ax.annotate('', xy=(a['start'],1900), xytext=(a['end'],1900), arrowprops={'arrowstyle': '<->'}, va='center')
    ax.text((a['start']+a['end'])/2, 1950, a['label'], ha='center', size=15)
"""

if end_x == 0:
    end_x = len(values) * probe_interval

ax.set_xlim(left=start_x, right=end_x)

box = ax.get_position()
ax.set_position([box.x0, box.y0 + box.height * 0.2, box.width, box.height * 0.8])
ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15), ncol=2)

plt.savefig(user_figname, dpi=100)
plt.show()
