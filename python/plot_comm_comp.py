#!/usr/bin/env python3

import sys

from memory_contention import *


FILL_ALPHA = 0.3

NETWORK_BANDWIDTH_OPT = "--network-bandwidth"
HIDE_COMP_CURVES_OPT = "--hide-comp"
HIDE_ALONE_OPT = "--hide-alone"
NO_LEGEND_OPT = "--no-legend"
NO_TITLE_OPT = "--no-title"
X_SPAN_ZONE_OPT = "--x-span-zone="
COMP_LEGEND_TITLE_OPT = "--comp-legend-title="

stream_bench_display = None
user_legend_loc = None
type_network_bench = CommCompGraphCommType.LATENCY
hide_comp_curves = False
hide_alone_curves = False
hide_legend = False
x_span_zone = None
comp_legend_title = None
display_title = True


def man():
    print("Plot communication duration and memory bandwidth (from output of ./bench executions) according to the number of OpenMP threads, arithmetic intensity or size of transmitted data.")
    print("Specify files to plot")
    print("Filenames must have format: *_<nb threads>_*")
    print("Options:")
    print(SLIDE_FORMAT_OPT)
    print(MERGE_FORMAT_OPT)
    print(NO_DISPLAY_OPT)
    print(TOP_OPT)
    print(STREAM_TOP_OPT)
    print(STREAM_BENCH_DISPLAY_OPT, "[copy|add|triad|scale]")
    print(PER_CORE_OPT, PER_THREAD_OPT, "[=<nb cores>]")
    print(OUTPUT_FILENAME_OPT)
    print(TITLE_OPT)
    print(NETWORK_BANDWIDTH_OPT)
    print(HIDE_COMP_CURVES_OPT)
    print(HIDE_ALONE_OPT)
    print(NO_LEGEND_OPT)
    print(X_SPAN_ZONE_OPT)
    print(COMP_LEGEND_TITLE_OPT)
    print(NO_TITLE_OPT)
    print(HELP_OPT + " " + HELP_SHORT_OPT)
    sys.exit(1)


def parse_options(options):
    global slide_format
    global merge_format
    global display_graph
    global top_limit
    global stream_top_limit
    global stream_bench_display
    global per_core
    global user_figname
    global user_title
    global user_per_core_nb
    global type_network_bench
    global hide_comp_curves
    global hide_alone_curves
    global hide_legend
    global x_span_zone
    global comp_legend_title
    global display_title
    files = []

    for opt in options:
        if opt.startswith(PER_THREAD_OPT):
            opt = opt.replace(PER_THREAD_OPT, PER_CORE_OPT)

        if opt == SLIDE_FORMAT_OPT:
            slide_format = True
        elif opt == MERGE_FORMAT_OPT:
            merge_format = True
        elif opt.startswith(TOP_OPT):
            top_limit = int(opt[len(TOP_OPT):])
        elif opt.startswith(STREAM_TOP_OPT):
            stream_top_limit = int(opt[len(STREAM_TOP_OPT):])
        elif opt == NO_DISPLAY_OPT:
            display_graph = False
        elif opt.startswith(STREAM_BENCH_DISPLAY_OPT):
            value = opt[len(STREAM_BENCH_DISPLAY_OPT):]
            if value.lower() not in ["copy", "scale", "add", "triad"]:
                man()
            stream_bench_display = value
        elif opt == PER_CORE_OPT:
            per_core = True
        elif opt.startswith(PER_CORE_OPT + "="):
            per_core = True
            user_per_core_nb = int(opt[len(PER_CORE_OPT+"="):])
        elif opt.startswith(X_SPAN_ZONE_OPT):
            x_span_zone = int(opt[len(X_SPAN_ZONE_OPT):])
        elif opt == NETWORK_BANDWIDTH_OPT:
            type_network_bench = CommCompGraphCommType.BANDWIDTH
        elif opt.startswith(OUTPUT_FILENAME_OPT):
            user_figname = opt[len(OUTPUT_FILENAME_OPT):]
        elif opt.startswith(TITLE_OPT):
            user_title = opt[len(TITLE_OPT):].replace('"', '').replace("'", "")
        elif opt.startswith(COMP_LEGEND_TITLE_OPT):
            comp_legend_title = opt[len(COMP_LEGEND_TITLE_OPT):].replace('"', '').replace("'", "")
        elif opt == HIDE_COMP_CURVES_OPT:
            hide_comp_curves = True
        elif opt == HIDE_ALONE_OPT:
            hide_alone_curves = True
        elif opt == NO_LEGEND_OPT:
            hide_legend = True
        elif opt == NO_TITLE_OPT:
            display_title = False
        elif opt == HELP_OPT or opt == HELP_SHORT_OPT:
            man()
        else:
            files.append(opt)

    return files


def format_comp_label(label):
    if comp_legend_title is not None:
        return ""
    if hasattr(label, "name"):
        return label.name
    elif type(label) != str:
        return label.__name__
    else:
        if label == "cursor":
            return "COMPUTATION"
        else:
            return label.upper()


if len(sys.argv) < 2:
    man()

files = parse_options(sys.argv[1:])

if len(files) == 0:
    man()


parser = FilesParser(files, per_core, user_per_core_nb)
metric = parser.compute_bench_type.default_metric

working_directory = get_working_directory(files[0])
title = get_title_prefix(working_directory) + "/" + parser.filename_prefix
if user_title is not None:
    title = user_title
if not display_title:
    title = None

comm_label = "Network Latency"
if type_network_bench == CommCompGraphCommType.BANDWIDTH:
    comm_label = "Network Bandwidth"

graph = CommCompGraph(parser.x_values, parser.x_type, type_network_bench, parser.compute_bench_type, metric, comm_label, title=title, x_span_zone=x_span_zone, comp_legend_title=comp_legend_title)

if type_network_bench == CommCompGraphCommType.BANDWIDTH:
    if len(parser.comm_bw_alone_results) > 0 and not hide_alone_curves:
        graph.add_comm_curve(
            [parser.comm_bw_alone_results[x]['med'] for x in parser.x_values],
            "alone",
            CommCompGraphCurveType.ALONE,
            False,
            [parser.comm_bw_alone_results[x]['d1'] for x in parser.x_values],
            [parser.comm_bw_alone_results[x]['d9'] for x in parser.x_values]
        )

    if len(parser.comm_bw_with_comp_results) > 0:
        graph.add_comm_curve(
            [parser.comm_bw_with_comp_results[x]['med'] for x in parser.x_values],
            "while Computations",
            CommCompGraphCurveType.PARALLEL,
            True,
            [parser.comm_bw_with_comp_results[x]['d1'] for x in parser.x_values],
            [parser.comm_bw_with_comp_results[x]['d9'] for x in parser.x_values]
        )
else:
    if len(parser.comm_lat_alone_results) > 0 and not hide_alone_curves:
        graph.add_comm_curve(
            [parser.comm_lat_alone_results[x]['med'] for x in parser.x_values],
            "alone",
            CommCompGraphCurveType.ALONE,
            False,
            [parser.comm_lat_alone_results[x]['d1'] for x in parser.x_values],
            [parser.comm_lat_alone_results[x]['d9'] for x in parser.x_values]
        )

    if len(parser.comm_lat_with_comp_results) > 0:
        graph.add_comm_curve(
            [parser.comm_lat_with_comp_results[x]['med'] for x in parser.x_values],
            "while Computations",
            CommCompGraphCurveType.PARALLEL,
            True,
            [parser.comm_lat_with_comp_results[x]['d1'] for x in parser.x_values],
            [parser.comm_lat_with_comp_results[x]['d9'] for x in parser.x_values]
        )


if not hide_comp_curves:
    op_to_plot = []
    if parser.compute_bench_type == Stream:
        # We keep only the intersection of op provided by user to this plotting script and op in the benchmark output
        op_to_plot_tmp = ["copy", "scale", "add", "triad"] if stream_bench_display is None else [stream_bench_display]
        for op in op_to_plot_tmp:
            if op in parser.comp_alone_results:
                op_to_plot.append(op)
    else:
        op_to_plot = [parser.compute_bench_type]

    for op in op_to_plot:
        if op in parser.comp_alone_results:
            graph.add_comp_curve(
                [parser.comp_alone_results[op][x][metric]['avg'] for x in parser.x_values],
                format_comp_label(op) + " alone",
                CommCompGraphCurveType.ALONE,
                False,
                [parser.comp_alone_results[op][x][metric]['min'] for x in parser.x_values],
                [parser.comp_alone_results[op][x][metric]['max'] for x in parser.x_values]
            )

        if op in parser.comp_with_comm_results:
            graph.add_comp_curve(
                [parser.comp_with_comm_results[op][x][metric]['avg'] for x in parser.x_values],
                format_comp_label(op) + " while Ping-Pongs",
                CommCompGraphCurveType.PARALLEL,
                True,
                [parser.comp_with_comm_results[op][x][metric]['min'] for x in parser.x_values],
                [parser.comp_with_comm_results[op][x][metric]['max'] for x in parser.x_values]
            )

if not slide_format:
    if merge_format:
        graph.fig_size = CommCompGraphFormat.MEDIUM
    else:
        graph.fig_size = CommCompGraphFormat.LARGE


if top_limit is not None:
    graph.comm_top_limit = top_limit
if stream_top_limit is not None:
    graph.comp_top_limit = stream_top_limit

if hide_legend:
    graph.display_legend = False

if user_figname is not None:
    fig_name = user_figname
else:
    fig_name = parser.filename_prefix
    if type_network_bench == CommCompGraphCommType.LATENCY and \
            (len(parser.comm_lat_with_comp_results) > 0 or len(parser.comm_lat_alone_results) > 0):
        fig_name += "_latency"
    elif type_network_bench == CommCompGraphCommType.BANDWIDTH and \
    (len(parser.comm_bw_with_comp_results) > 0 or len(parser.comm_bw_alone_results) > 0):
        fig_name += "_bandwith"
    fig_name += ".png"

graph.per_core = per_core
graph.save(fig_name)

if display_graph:
    graph.show()
