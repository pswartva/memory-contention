#!/usr/bin/env python3

import argparse
import math
import pandas as pd


cli_parser = argparse.ArgumentParser()
cli_parser.add_argument("bench_output", help="File containing ouput of basic_openmp_memset benchmark")
cli_parser.add_argument("nb_cores_per_numa", help="Number of cores per NUMA node", type=int)
args = cli_parser.parse_args()

df = pd.read_csv(args.bench_output, sep='\t', comment="#")

df_starts = df[['Thread', 'Core', 'Time start']]
df_starts['Type'] = "Start"
df_starts.rename(columns={"Time start": "Time"}, inplace=True)

df_ends = df[['Thread', 'Core', 'Time end']]
df_ends['Type'] = "End"
df_ends.rename(columns={"Time end": "Time"}, inplace=True)

final_df = pd.concat([df_starts, df_ends], ignore_index=True).sort_values(by=["Time"])

threads = final_df[["Thread", "Core"]].value_counts().reset_index(name='count')
threads.drop(columns=["count"], inplace=True)
threads.set_index("Thread", inplace=True)
binding_thread_to_core = threads.to_dict()['Core']

nb_numa_nodes = math.ceil(len(binding_thread_to_core) / args.nb_cores_per_numa)
print(f"Guessed {nb_numa_nodes} NUMA nodes, with {args.nb_cores_per_numa} cores each")

s = """
%EventDef PajeDefineContainerType 1
% Alias string
% ContainerType string
% Name string
%EndEventDef
%EventDef PajeDefineStateType 2
% Alias string
% ContainerType string
% Name string
%EndEventDef
%EventDef PajeDefineEntityValue 3
% Alias string
% EntityType string
% Name string
% Color color
%EndEventDef
%EventDef PajeCreateContainer 4
% Time date
% Alias string
% Type string
% Container string
% Name string
%EndEventDef
%EventDef PajeDestroyContainer 5
% Time date
% Name string
% Type string
%EndEventDef
%EventDef PajeSetState 6
% Time date
% Type string
% Container string
% Value string
%EndEventDef
1 CT_Node 0 "Node"
1 CT_NumaNode CT_Node "Numa Node"
1 CT_Core CT_NumaNode "Core"
1 CT_Thread CT_Core "Thread"
2 ST_ThreadState CT_Thread "Thread state"
3 S_1 ST_ThreadState "Idle" "0.500000 0.500000 0.500000"
3 S_2 ST_ThreadState "Memset" "1.000000 0.000000 0.000000"
4 0.000000 C_Node CT_Node 0 "Machine"
"""

for n in range(nb_numa_nodes):
    s += '4 0.000000 C_Numa{} CT_NumaNode C_Node "Numa Node {}"\n'.format(n, n)
for th in binding_thread_to_core:
    core = binding_thread_to_core[th]
    numa_node = core // args.nb_cores_per_numa
    s += '4 0.000000 C_Core{} CT_Core C_Numa{} "Core{}"\n'.format(core, numa_node, core)
    s += '4 0.000000 C_Thread{} CT_Thread C_Core{} "Thread{}"\n'.format(th, core, th)

for i, e in final_df.iterrows():
    if e['Type'] == "Start":
        s += '6 {} ST_ThreadState C_Thread{} S_2\n'.format(e["Time"] / 1000, e["Thread"])
    elif e['Type'] == "End":
        s += '6 {} ST_ThreadState C_Thread{} S_1\n'.format(e["Time"] / 1000, e["Thread"])

last_point = args.bench_output.rfind(".")
output_filename = args.bench_output[:last_point] + ".trace"
with open(output_filename, "w") as f:
    f.write(s)
