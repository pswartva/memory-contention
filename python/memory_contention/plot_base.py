import os
import re
import math
import numpy as np
from matplotlib.ticker import FuncFormatter


HELP_OPT = "--help"
HELP_SHORT_OPT = "-h"
SLIDE_FORMAT_OPT = "--slide-format"
MERGE_FORMAT_OPT = "--merge-format"
NO_DISPLAY_OPT = "--no-display"
TOP_OPT = "--top="
BOTTOM_OPT = "--bottom="
STREAM_TOP_OPT = "--stream-top="
STREAM_BOTTOM_OPT = "--stream-bottom="
PER_CORE_OPT = "--per-core"
PER_THREAD_OPT = "--per-thread"
STREAM_BENCH_DISPLAY_OPT = "--stream="
TITLE_OPT = "--title="
OUTPUT_FILENAME_OPT = "--o="

REMOVE_IN_PATH = ["/home/philippe/Documents/resultats/plafrim/exploited/", "/home/philippe/Documents/resultats/memory-contention/", "/home/philippe/Documents/resultats/"]

slide_format = False
display_graph = True
merge_format = False
top_limit = None
bottom_limit = None
stream_top_limit = None
stream_bottom_limit = None
per_core = False
user_per_core_nb = None
user_title = None
user_figname = None


def get_title_prefix(working_directory):
    for p in REMOVE_IN_PATH:
        if working_directory.startswith(p):
            return working_directory[len(p):]

    return working_directory.split('/')[-1]


def get_working_directory(path):
    if isinstance(path, list):
        if len(path) > 1:
            return os.getcwd()
        else:
            working_directory = os.path.dirname(path[0])
    else:
        working_directory = os.path.dirname(path)

    if working_directory == "":
        working_directory = os.getcwd()

    return working_directory


def get_compute_bench_type(lines):
    for line in lines[:20]:
        if line.strip().startswith("# Will do CPU intensive benchmark"):
            return "prime"
        elif line.strip().startswith("# Will do Cholesky benchmark"):
            return "cholesky"
        elif "STREAM" in line or "weak stream":
            return "stream"
        elif line.strip().startswith("# Will do scalar avx benchmark"):
            return "scalar avx"
        elif line.strip().startswith("# Will do scalar benchmark"):
            return "scalar"
        elif line.strip().startswith("# Will do Cursor benchmark"):
            return "cursor"

    raise Exception("Unable to find compute bench type.")


def get_time_results(lines, compute_bench_type=None):
    with_comm = [float(v) for v in lines[-2].split()[3:]]
    without_comm = [float(v) for v in lines[-1].split()[3:]]

    if compute_bench_type is not None:
        return {
            "with_comm": {compute_bench_type: with_comm},
            "without_comm": {compute_bench_type: without_comm}
        }
    else:
        return {
            "with_comm": with_comm,
            "without_comm": without_comm
        }


def get_cholesky_results(lines, nb_threads=None):
    def apply_scale(values):
        if nb_threads is not None:
            return list(map(lambda v: v / nb_threads, values))
        else:
            return values

    result = {}

    result["with_comm"] = {
        "cholesky": apply_scale([float(v) for v in lines[-2].split()[6:]])
    }
    result["without_comm"] = {
        "cholesky": apply_scale([float(v) for v in lines[-1].split()[6:]])
    }

    return result


def compute_stats(l):
    assert(len(l) > 0)

    return {
        'd1': np.percentile(l, 10),
        'med': np.median(l),
        'avg': np.mean(l),
        'd9': np.percentile(l, 90),
        'std': np.std(l)
    }


def get_comm_durations(lines):
    pattern = re.compile('^\d+\.\d* MB/s - \d+\.\d* us')
    bandwidths = [[]]
    latencies = [[]]
    nb_values = 0

    for line in lines:
        if line.strip() == "# Communications with computing":
            bandwidths.append([])
            latencies.append([])
        elif pattern.match(line) is not None:
            values = line.split("-")
            bandwidths[-1].append(float(values[0].split()[0].strip()))
            latencies[-1].append(float(values[1].split()[0].strip()))
            nb_values += 1
        else:
            if len(bandwidths[-1]) > 0:
                if len(bandwidths) == 2:
                    return bandwidths, latencies

                bandwidths.append([])
                latencies.append([])

    if nb_values == 0:
        # Fallback to old format, try latency:
        pattern = re.compile('^\d+\.\d* us')
        bandwidths = [[]]
        latencies = [[]]
        for line in lines:
            if pattern.match(line) is not None:
                nb_values += 1
                latencies[-1].append(float(line.split()[0]))
            else:
                if len(latencies[-1]) > 0:
                    if len(latencies) == 2:
                        return bandwidths, latencies

                    latencies.append([])

    if nb_values == 0:
        # Fallback to old format, try bandwidth:
        pattern = re.compile('^\d+\.\d* MB/s')
        bandwidths = [[]]
        latencies = [[]]
        for line in lines:
            if pattern.match(line) is not None:
                bandwidths[-1].append(float(line.split()[0]))
            else:
                if len(bandwidths[-1]) > 0:
                    if len(bandwidths) == 2:
                        return bandwidths, latencies

                    bandwidths.append([])

    return bandwidths, latencies


def byte_formatter_func(x, pos):
    if x / (1024 * 1024) >= 1:
        return str(int(x / (1024 * 1024))) + " MB"
    elif x / 1024 >= 1:
        return str(int(x / 1024)) + " KB"
    else:
        return str(int(x)) + " B"

byte_formatter = FuncFormatter(byte_formatter_func)
