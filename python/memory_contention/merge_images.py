import math
from PIL import Image


class ImageMerger:
    def __init__(self, figname, nb_images, nb_columns):
        self.figname = figname
        self.nb_images = nb_images
        self.nb_columns = nb_columns
        self.nb_lines = math.ceil(nb_images / nb_columns)
        self.images = [[None for _ in range(self.nb_columns)] for _ in range(self.nb_lines)]
        self.width = None
        self.height = None

    def __setitem__(self, coord, figname):
        line, column = coord
        if line >= self.nb_lines:
            raise Exception("Line is out of bounds!")
        if column >= self.nb_columns:
            raise Exception("Column is out of bounds!")

        im = Image.open(figname)

        if self.width is None:
            self.width, self.height = im.size
        elif im.width != self.width or im.height != self.height:
            raise Exception("Size don't match!")

        self.images[line][column] = im

    def save(self):
        merged_image = Image.new('RGB', (self.nb_columns * self.width, self.nb_lines * self.height))

        for i in range(self.nb_lines):
            for j in range(self.nb_columns):
                if self.images[i][j] is not None:
                    merged_image.paste(self.images[i][j], (j * self.width, i * self.height))

        merged_image.save(self.figname)
