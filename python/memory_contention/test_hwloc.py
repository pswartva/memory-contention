import unittest
from .hwloc import *

class TestHwlocTopology(unittest.TestCase):
    def test_henri_subnuma(self):
        topo = HwlocTopology("test_data/henri-subnuma.xml", ["L3Cache"])
        self.assertEqual(topo.nb_sockets, 2)
        self.assertEqual(topo.nb_numa_nodes_total, 4)
        self.assertEqual(topo.nb_cores_total, 36)

    def test_henri(self):
        topo = HwlocTopology("test_data/henri.xml", ["L3Cache"])
        self.assertEqual(topo.nb_sockets, 2)
        self.assertEqual(topo.nb_numa_nodes_total, 2)
        self.assertEqual(topo.nb_cores_total, 36)


if __name__ == '__main__':
    unittest.main()
