import abc
from enum import Enum
import math
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter, MaxNLocator
import os
import re

from .plot_base import byte_formatter, byte_formatter_func, get_comm_durations, compute_stats


_mem_bw = "Memory Bandwidth (MB/s)"
_mem_bw_per_core = "Memory Bandwidth per Core (MB/s)"
_comp_time = "Computing Duration (ms)"
_comp_time_per_core = "Computing Duration per Core (ms)"
_flops = "GFlops"
_flops_per_core = "GFlops per Core"


class CompMetric(Enum):
    DEFAULT = "default"
    BANDWIDTH = "bandwidth"
    TIME = "time"
    FLOPS = "flops"

    def __repr__(self):
        return self.name


class CompType(abc.ABC):
    pass


class Stream(CompType):
    available_metrics = [CompMetric.BANDWIDTH, CompMetric.TIME]
    y_axis_label = {
        CompMetric.BANDWIDTH: _mem_bw,
        CompMetric.TIME: _comp_time
    }
    y_axis_label_per_core = {
        CompMetric.BANDWIDTH: _mem_bw_per_core,
        CompMetric.TIME: _comp_time_per_core
    }
    support_per_core = True
    default_metric = CompMetric.BANDWIDTH


class TimeBased(CompType):
    available_metrics = [CompMetric.TIME]
    y_axis_label = {
        CompMetric.TIME: _comp_time
    }
    support_per_core = False
    default_metric = CompMetric.TIME


class Prime(TimeBased):
    pass


class Scalar(TimeBased):
    pass


class ScalarAvx(TimeBased):
    name = "Scalar AVX"
    pass


class Cursor(TimeBased):
    pass


# We inherit from Stream, because metrics are the same, but we need a different
# class to handle differently the result lines.
class Memset(Stream):
    pass


class Cholesky(CompType):
    available_metrics = [CompMetric.FLOPS]
    y_axis_label = {
        CompMetric.FLOPS: _flops
    }
    y_axis_label_label = {
        CompMetric.FLOPS: _flops_per_core
    }
    support_per_core = True
    default_metric = CompMetric.FLOPS


class CommCompGraphCommType(Enum):
    LATENCY = 0
    BANDWIDTH = 1


class FilesParser:
    """
    A class used to parse the output files of bench_* programs.

    Attributes
    ----------
    comm_bw_alone_results : {x => [min, med, max]}
        The network bandwidth performances of communications executed alone.
    comm_bw_with_comp_results : {x => [min, med, max]}
        The network bandwidth performances of communications executed along with computations.
    comm_lat_alone_results : {x => [min, med, max]}
        The network latency performances of communications executed alone.
    comm_lat_with_comp_results : {x => [min, med, max]}
        The network latency performances of communications executed along with computations.
    comp_alone_results : {op => { x => {"time" => [min, avg, max], "other metric (gflops, bw, ...)" => [min, avg, max]}}}
        The computation performances when executed alone.
    comp_with_comm_results : {op => { x => {"time" => [min, avg, max], "other metric (gflops, bw, ...)" => [min, avg, max]}}}
        The computation performances when executed along with communications.
    comp_alone_per_thread_results : {op => { x => {"time" => {"min" => [], "avg" => [], "max" => []}, "other metric (gflops, bw, ...)" => {"min" => [], "avg" => [], "max" => []}}}}
        The computation performances per thread when executed alone.
    comp_with_comm_per_thread_results : {op => { x => {"time" => {"min" => [], "avg" => [], "max" => []}, "other metric (gflops, bw, ...)" => {"min" => [], "avg" => [], "max" => []}}}}
        The computation performances per thread when executed along with communications.
    machine : {key => int}
        Machine properties (#PUs, LLC size, #NUMA, #sockets).
    x_type : string
        The type of X values, among "cursor", "thread", "byte". It can be
        forced with the parameter of the constructor, otherwise it is
        defined according to filenames.
    filename_prefix : string
        The common prefix of parsed files, automatically guessed.
    compute_bench_type : CompType
        A child class of CompType representing the type of computations.
    x_values : [number]
        The sorted list of X values.
    """

    def __init__(self, files, per_core=False, user_per_core_nb=None, x_type=None):
        if len(files) == 0:
            raise Exception("You have to provide some files to parse !")

        self.comm_bw_alone_results = {}  # {x => [min, med, max]}
        self.comm_bw_with_comp_results = {}
        self.comm_lat_alone_results = {}
        self.comm_lat_with_comp_results = {}
        self.comp_alone_results = {}  # {op => { x => {"time" => [min, avg, max], "other metric (gflops, bw, ...)" => [min, avg, max]}}}
        self.comp_with_comm_results = {}
        self.comp_alone_per_thread_results = {}  # {op => { x => {"time" => {"min" => [], "avg" => [], "max" => []}, "other metric (gflops, bw, ...)" => {"min" => [], "avg" => [], "max" => []}}}}
        self.comp_with_comm_per_thread_results = {}
        self.machine = {
            "nb_pus": None,
            "l3_cache_size": None,  # in MB
            "nb_numa_nodes": None,
            "nb_sockets": None
        }
        self._forced_x_type = (x_type is not None)
        self.x_type = x_type
        self.filename_prefix = None
        self.compute_bench_type = None
        self.x_values = []
        self._nb_parsed_files = 0
        self._per_core = per_core
        self._user_per_core_nb = user_per_core_nb

        if self._user_per_core_nb is not None:
            self._per_core = True

        for f in files:
            try:
                self._parse_file(f)
            except Exception as e:
                raise Exception(f"Error during parsing of file {f}: {repr(e)}")

        if self.x_type == "cursor":
            # We convert cursor to arithmetic intensity:
            self._convert_x(lambda x: 3*(x+1)/16)

        self.x_values = sorted(self.x_values)

    def _get_nb_header_lines(self, lines):
        meet_sharp = False
        nb_header_lines = 0

        for l in lines:
            if l.startswith("# "):
                meet_sharp = True
            elif meet_sharp:
                # we parsed all the header of the file that could contains these informations,
                # no need to parse the whole file
                break

            nb_header_lines += 1

        return nb_header_lines

    @classmethod
    def _get_compute_bench_type(cls, lines):
        name_to_bench_type = {
            "cursor": Cursor,
            "prime": Prime,
            "scalar": Scalar,
            "scalar_avx": ScalarAvx,
            "stream": Stream,
            "stream_weak": Stream,
            "cholesky": Cholesky,
            "memset": Memset
        }
        for line in lines:
            if line.strip().startswith("# Computing kernel:"):
                return name_to_bench_type[line.strip().split()[-1]]

        # legacy format:
        for line in lines:
            if line.strip().startswith("# Will do CPU intensive benchmark"):
                return Prime
            elif line.strip().startswith("# Will do Cholesky benchmark"):
                return Cholesky
            elif "STREAM" in line or "weak stream" in line:
                return Stream
            elif line.strip().startswith("# Will do scalar avx benchmark"):
                return ScalarAvx
            elif line.strip().startswith("# Will do scalar benchmark"):
                return Scalar
            elif line.strip().startswith("# Will do Cursor benchmark"):
               return Cursor

        raise Exception("Unable to find compute bench type.")

    @classmethod
    def _get_stream_results(cls, lines, nb_threads=None):
        def apply_scale(values):
            if nb_threads is not None:
                return list(map(lambda v: v / nb_threads, values))
            else:
                return values

        def parse_line(line):
            stream_type = line.split()[1].lower()[:-1] # exclude the last character (a semi-column)
            values = apply_scale([float(v) for v in line.split()[2:]])

            return stream_type, values

        def is_result_line(line):
            return line.split()[1].lower()[:-1] in ["copy", "triad", "add", "scale"]

        def values_to_dict(values):
            return {
                'min': values[0],
                'avg': values[1],
                'max': values[2]
            }

        result = {"without_comm": {}, "with_comm": {}}

        in_key = None
        for i in range(len(lines)-15, len(lines)):
            if lines[i].strip() == "# without communications:":
                in_key = "without_comm"
            elif lines[i].strip() == "# with communications:":
                in_key = "with_comm"
            elif in_key is not None and is_result_line(lines[i]):
                op, values = parse_line(lines[i])
                if len(values) == 3:
                    result[in_key][op] = {CompMetric.BANDWIDTH: values_to_dict(values)}
                elif len(values) == 6:
                    result[in_key][op] = {CompMetric.BANDWIDTH: values_to_dict(values[:3]), CompMetric.TIME: values_to_dict(values[3:])}

        return result["without_comm"], result["with_comm"]

    @classmethod
    def _get_time_results(cls, lines):
        def line_to_dict(l):
            values = list(map(float, l.strip().split()[-3:]))
            return {
                'min': values[0],
                'avg': values[1],
                'max': values[2]
            }

        results = {"without_comm": {}, "with_comm": {}}
        op = None

        for i in range(len(lines)-15, len(lines)):
            if "memset" in lines[i]:
                op = Memset
                break
            elif "scalar_avx" in lines[i]:
                op = ScalarAvx
                break
            elif "PRIME" in lines[i]:
                op = Prime
                break
            elif "cursor" in lines[i]:
                op = Cursor
                break

        if op is None:
            raise Exception("Could not find which operation is executed")

        nb_found = 0
        for i in range(len(lines)-15, len(lines)):
            if lines[i].strip().startswith("# without communications"):
                assert(op not in results["without_comm"])
                results["without_comm"][op] = {CompMetric.TIME: line_to_dict(lines[i])}
                nb_found += 1
            elif lines[i].strip().startswith("# with communications"):
                assert(op not in results["with_comm"])
                results["with_comm"][op] = {CompMetric.TIME: line_to_dict(lines[i])}
                nb_found += 1
            if nb_found == 2:
                break

        return results["without_comm"], results["with_comm"]

    @classmethod
    def _get_bw_time_results(cls, lines, op, nb_threads=None):
        def apply_scale(value):
            if value == "-":
                return None
            value = float(value)
            if nb_threads is not None:
                return value / nb_threads
            else:
                return value

        def line_to_dict(l):
            values = l.strip().split()[-6:]
            return {
                CompMetric.BANDWIDTH: {
                    'min': apply_scale(values[0]),
                    'avg': apply_scale(values[1]),
                    'max': apply_scale(values[2])
                },
                CompMetric.TIME: {
                    'min': apply_scale(values[3]),
                    'avg': apply_scale(values[4]),
                    'max': apply_scale(values[5])
                }
            }

        results = {"without_comm": {}, "with_comm": {}}
        nb_found = 0

        for i in range(max(len(lines)-15, 0), len(lines)):
            if lines[i].strip().startswith("# without communications"):
                assert(op not in results["without_comm"])
                results["without_comm"][op] = line_to_dict(lines[i])
                nb_found += 1
            elif lines[i].strip().startswith("# with communications"):
                assert(op not in results["with_comm"])
                results["with_comm"][op] = line_to_dict(lines[i])
                nb_found += 1
            if nb_found == 2:
                break

        return results["without_comm"], results["with_comm"]

    def _get_bw_time_per_thread_results(self, lines, op, nb_threads):
        def line_to_dict(l):
            words = l.strip().split()[-7:]
            values = list(map(float, words[1:]))
            r = {
                CompMetric.BANDWIDTH: {
                    'min': values[0],
                    'avg': values[1],
                    'max': values[2]
                },
                CompMetric.TIME: {
                    'min': values[3],
                    'avg': values[4],
                    'max': values[5]
                }
            }

            if self._per_core:
                r[CompMetric.BANDWIDTH]['min'] /= nb_threads
                r[CompMetric.BANDWIDTH]['avg'] /= nb_threads
                r[CompMetric.BANDWIDTH]['max'] /= nb_threads

            return int(words[0]), r

        def rearrange_dict(d):
            nb_threads = len(d)
            return {
                m: {
                    k: [d[i][m][k] for i in range(nb_threads)] for k in ['min', 'avg', 'max']
                } for m in [CompMetric.BANDWIDTH, CompMetric.TIME]
            }

        results = {"without_comm": {}, "with_comm": {}}
        nb_found = 0

        in_result_per_thread_with_comm = False
        in_result_per_thread_without_comm = False
        for i in range(len(lines)):
            if "results per thread (with comm):" in lines[i]:
                in_result_per_thread_with_comm = True
            elif "results per thread (without comm):" in lines[i]:
                in_result_per_thread_with_comm = False
                in_result_per_thread_without_comm = True
            elif in_result_per_thread_with_comm:
                key, value = line_to_dict(lines[i])
                if key in results["with_comm"]:
                    raise Exception(f"Key '{key}' already present in with_comm.")
                results["with_comm"][key] = value
                if len(results["with_comm"]) == nb_threads:
                    in_result_per_thread_with_comm = False
            elif in_result_per_thread_without_comm:
                key, value = line_to_dict(lines[i])
                if key in results["without_comm"]:
                    raise Exception(f"Key '{key}' already present in without_comm.")
                results["without_comm"][key] = value
                if len(results["without_comm"]) == nb_threads:
                    break

        return {op: rearrange_dict(results["without_comm"])}, {op: rearrange_dict(results["with_comm"])}

    def _parse_machine(self, lines):
        infos = {
            "nb_pus": "# Nb PUs",
            "l3_cache_size": "# L3 cache size",
            "nb_numa_nodes": "# Nb NUMA nodes",
            "nb_sockets": "# Nb sockets",
        }
        run = {
            "nb_workers": "# Nb workers",
        }
        run_values = {}

        for k in infos:
            for l in lines:
                if l.startswith(infos[k]):
                    if ":" in l:
                        v = int(l[l.find(":")+1:].split()[0])
                    elif "=" in l:
                        v = int(l[l.find("=")+1:].split()[0])
                    else:
                        raise Exception("Cannot parse machine parameter line")

                    if self.machine[k] is None:
                        self.machine[k] = v
                    elif self.machine[k] != v:
                        print("Warning: {} is different ({} vs {})".format(
                            k, self.machine['k'], v
                        ))
                        break

        for k in run:
            for l in lines:
                if l.startswith(run[k]):
                    run_values[k] = int(l[l.find(":")+1:].split()[0])
                    break

        return run_values


    def _parse_file(self, filename):
        if not os.path.exists(filename):
            raise Exception(f"File {filename} does not exist!")

        regex = re.compile(r"^(.+)_(\d+)_.+$")
        if self._forced_x_type:
            regexes = {
                'thread': re.compile(r"^(.+)_(\d+)_threads.+$"),
                'byte': re.compile(r"^(.+)_(\d+)_bytes.+$")
            }
            regex = regexes[self.x_type]

        m = regex.match(filename)
        x = -1
        prefix = None
        x_type = "thread"
        if m is not None:
            prefix = m.group(1)
            x = int(m.group(2))

            if not self._forced_x_type:
                if "_bytes." in filename:
                    x_type = "byte"
                elif "_cursor." in filename:
                    x_type = "cursor"
        else:
            raise Exception("Failed to find the number for x in file {}".format(filename))

        if x <= -1:
            raise Exception("x cannot be negative")
        elif x in self.x_values:
            raise Exception("We already have results for this x")

        self.x_values.append(x)

        if self.filename_prefix is None:
            self.filename_prefix = prefix
        elif self.filename_prefix != prefix:
            raise Exception("Filename prefix is not the same")

        if not self._forced_x_type:
            if self.x_type is None:
                self.x_type = x_type
            elif self.x_type != x_type:
                raise Exception("x type is not the same: '{}' (should be '{}')".format(x_type, self.x_type))

        nb_cores = None
        if self._per_core:
            if self.x_type == "byte":
                if self._user_per_core_nb is not None:
                    nb_cores = self._user_per_core_nb
            else:
                nb_cores = x

        with open(filename, 'r') as f:
            lines = f.readlines()
            nb_header_lines = self._get_nb_header_lines(lines)

            run_params = self._parse_machine(lines[:nb_header_lines])

            # Communications:
            bandwidths, latencies = get_comm_durations(lines)
            if len(bandwidths) >= 1:
                if len(bandwidths[0]) > 0:
                    self.comm_bw_alone_results[x] = compute_stats(bandwidths[0])
                if len(bandwidths) >= 2 and len(bandwidths[1]) > 0:
                    self.comm_bw_with_comp_results[x] = compute_stats(bandwidths[1])
            if len(latencies) >= 1:
                if len(latencies[0]) > 0:
                    self.comm_lat_alone_results[x] = compute_stats(latencies[0])
                if len(latencies) >= 2 and len(latencies[1]) > 0:
                    self.comm_lat_with_comp_results[x] = compute_stats(latencies[1])

            # Computations:
            compute_bench_type = self._get_compute_bench_type(lines[:nb_header_lines])
            if self.compute_bench_type is None:
                self.compute_bench_type = compute_bench_type
            elif self.compute_bench_type != compute_bench_type:
                raise Exception("Compute bench types are not the same")

            alone = None
            with_comm = None
            alone_per_thread = None
            with_comm_per_thread = None
            if compute_bench_type == Stream:
                alone, with_comm = self._get_stream_results(lines[nb_header_lines-1:], nb_cores)
            elif compute_bench_type == Memset:
                alone_per_thread, with_comm_per_thread = self._get_bw_time_per_thread_results(lines[nb_header_lines-1:], "memset", run_params['nb_workers'])
                alone, with_comm = self._get_bw_time_results(lines[nb_header_lines-1:], "memset", nb_cores)
                # If we measured the throughput, there is no global results in
                # the output of the benchmark, but the sum of bandwidth per
                # thread can be considered as the global bandwidth
                if len(alone_per_thread['memset'][CompMetric.BANDWIDTH]['min']) > 0 and alone['memset'][CompMetric.BANDWIDTH]['min'] is None:
                    alone['memset'] = {
                        m: {
                            k: sum(alone_per_thread['memset'][m][k]) for k in alone_per_thread['memset'][m]
                        } for m in alone_per_thread['memset']
                    }
                if len(with_comm_per_thread['memset'][CompMetric.BANDWIDTH]['min']) > 0 and with_comm['memset'][CompMetric.BANDWIDTH]['min'] is None:
                    with_comm['memset'] = {
                        m: {
                            k: sum(with_comm_per_thread['memset'][m][k]) for k in with_comm_per_thread['memset'][m]
                        } for m in with_comm_per_thread['memset']
                    }
            elif compute_bench_type in [ScalarAvx, Prime, Cursor]:
                alone, with_comm = self._get_time_results(lines)
            # elif compute_bench_type == "cholesky":
                # file_data["computing"] = get_cholesky_results(lines, nb_cores)
            # else:
                # file_data["computing"] = get_time_results(lines, compute_bench_type)
            else:
                raise Exception("Unknown compute_bench_type")

            for op in alone:
                if self._nb_parsed_files == 0:
                    self.comp_alone_results[op] = dict()
                    self.comp_alone_per_thread_results[op] = dict()
                elif op not in self.comp_alone_results:
                    raise Exception("Additionnal op not supported")

                if x in self.comp_alone_results[op]:
                    raise Exception("This op is already present for this x")

                self.comp_alone_results[op][x] = alone[op]

                if alone_per_thread is not None and len(alone_per_thread[op]) > 0:
                    self.comp_alone_per_thread_results[op][x] = alone_per_thread[op]

            for op in with_comm:
                if self._nb_parsed_files == 0:
                    self.comp_with_comm_results[op] = dict()
                    self.comp_with_comm_per_thread_results[op] = dict()
                elif op not in self.comp_with_comm_results:
                    raise Exception("Additionnal op not supported")

                if x in self.comp_with_comm_results[op]:
                    raise Exception("This op is already present for this x")

                self.comp_with_comm_results[op][x] = with_comm[op]

                if with_comm_per_thread is not None and len(with_comm_per_thread[op]) > 0:
                    self.comp_with_comm_per_thread_results[op][x] = with_comm_per_thread[op]

            assert(len(alone) == len(self.comp_alone_results))
            assert(len(with_comm) == len(self.comp_with_comm_results))

        self._nb_parsed_files += 1

    def _convert_x(self, scale):
        old_comm_bw_alone_results = self.comm_bw_alone_results
        old_comm_bw_with_comp_results = self.comm_bw_with_comp_results
        old_comm_lat_alone_results = self.comm_lat_alone_results
        old_comm_lat_with_comp_results = self.comm_lat_with_comp_results
        old_comp_alone_results = self.comp_alone_results
        old_comp_with_comm_results = self.comp_with_comm_results
        old_x_values = self.x_values

        self.comm_bw_alone_results = {}
        self.comm_bw_with_comp_results = {}
        self.comm_lat_alone_results = {}
        self.comm_lat_with_comp_results = {}
        self.comp_alone_results = {}
        self.comp_with_comm_results = {}
        self.x_values = []

        for op in old_comp_alone_results:
            self.comp_alone_results[op] = dict()
            self.comp_with_comm_results[op] = dict()

        for old_x in old_x_values:
            new_x = scale(old_x)

            self.x_values.append(new_x)

            if len(old_comm_bw_alone_results) > 0:
                self.comm_bw_alone_results[new_x] = old_comm_bw_alone_results[old_x]
            if len(old_comm_bw_with_comp_results) > 0:
                self.comm_bw_with_comp_results[new_x] = old_comm_bw_with_comp_results[old_x]
            if len(old_comm_lat_alone_results) > 0:
                self.comm_lat_alone_results[new_x] = old_comm_lat_alone_results[old_x]
            if len(old_comm_lat_with_comp_results) > 0:
                self.comm_lat_with_comp_results[new_x] = old_comm_lat_with_comp_results[old_x]

            for op in self.comp_alone_results:
                self.comp_alone_results[op][new_x] = old_comp_alone_results[op][old_x]
                self.comp_with_comm_results[op][new_x] = old_comp_with_comm_results[op][old_x]

    @property
    def flatten_results(self):
        flatten = dict()
        flatten["x"] = self.x_values

        if len(self.comm_bw_alone_results) > 0 or len(self.comm_bw_with_comp_results) > 0 or len(self.comm_lat_alone_results) > 0 or len(self.comm_lat_with_comp_results) > 0:
            flatten["comm"] = dict()

            if len(self.comm_bw_alone_results) > 0 or len(self.comm_lat_alone_results) > 0:
                flatten["comm"]["alone"] = dict()
                if len(self.comm_lat_alone_results) > 0:
                    flatten["comm"]["alone"]["lat"] = {
                        "d1": [self.comm_lat_alone_results[x]['d1'] for x in self.x_values],
                        "med": [self.comm_lat_alone_results[x]['med'] for x in self.x_values],
                        "d9": [self.comm_lat_alone_results[x]['d9'] for x in self.x_values],
                        "std": [self.comm_lat_alone_results[x]['std'] for x in self.x_values],
                    }
                if len(self.comm_bw_alone_results) > 0:
                    flatten["comm"]["alone"]["bw"] = {
                        "d1": [self.comm_bw_alone_results[x]['d1'] for x in self.x_values],
                        "med": [self.comm_bw_alone_results[x]['med'] for x in self.x_values],
                        "d9": [self.comm_bw_alone_results[x]['d9'] for x in self.x_values],
                        "std": [self.comm_bw_alone_results[x]['std'] for x in self.x_values]
                    }

            if len(self.comm_bw_with_comp_results) > 0 or len(self.comm_lat_with_comp_results) > 0:
                flatten["comm"]["with_comp"] = dict()
                if len(self.comm_lat_with_comp_results) > 0:
                    flatten["comm"]["with_comp"]["lat"] = {
                        "d1": [self.comm_lat_with_comp_results[x]['d1'] for x in self.x_values],
                        "med": [self.comm_lat_with_comp_results[x]['med'] for x in self.x_values],
                        "d9": [self.comm_lat_with_comp_results[x]['d9'] for x in self.x_values],
                        "std": [self.comm_lat_with_comp_results[x]['std'] for x in self.x_values]
                    }
                if len(self.comm_bw_with_comp_results) > 0:
                    flatten["comm"]["with_comp"]["bw"] = {
                        "d1": [self.comm_bw_with_comp_results[x]['d1'] for x in self.x_values],
                        "med": [self.comm_bw_with_comp_results[x]['med'] for x in self.x_values],
                        "d9": [self.comm_bw_with_comp_results[x]['d9'] for x in self.x_values],
                        "std": [self.comm_bw_with_comp_results[x]['std'] for x in self.x_values]
                    }

        if len(self.comp_alone_results) > 0 or len(self.comp_with_comm_results) > 0:
            flatten["comp"] = dict()

            if len(self.comp_alone_results) > 0:
                flatten["comp"]["alone"] = dict()
                for op in self.comp_alone_results:
                    flatten["comp"]["alone"][op] = dict()
                    for metric in self.comp_alone_results[op][self.x_values[0]]:
                        flatten["comp"]["alone"][op][metric.value] = {
                            "min": [self.comp_alone_results[op][x][metric]['min'] for x in self.x_values],
                            "avg": [self.comp_alone_results[op][x][metric]['avg'] for x in self.x_values],
                            "max": [self.comp_alone_results[op][x][metric]['max'] for x in self.x_values]
                        }

            if len(self.comp_with_comm_results) > 0:
                flatten["comp"]["with_comm"] = dict()
                for op in self.comp_with_comm_results:
                    flatten["comp"]["with_comm"][op] = dict()
                    for metric in self.comp_with_comm_results[op][self.x_values[0]]:
                        flatten["comp"]["with_comm"][op][metric.value] = {
                            "min": [self.comp_with_comm_results[op][x][metric]['min'] for x in self.x_values],
                            "avg": [self.comp_with_comm_results[op][x][metric]['avg'] for x in self.x_values],
                            "max": [self.comp_with_comm_results[op][x][metric]['max'] for x in self.x_values]
                        }

        return flatten


class CommCompGraphCurveType(Enum):
    ALONE = 0
    PARALLEL = 1


class CommCompGraphFormat(Enum):
    SMALL = 0
    MEDIUM = (10, 6)
    LARGE = (15, 9)


class CommCompGraph:
    """
    A class used to build a plot with two Y-axis: communication performance on the left one and computation performance on the right one.

    Attributes
    ----------
    comm_top_limit : int|float
        The top limit for the left Y-axis.
    comp_top_limit : int|float
        The top limit for the right Y-axis.
    fig_size : CommCompGraphFormat
        The format of the resulting plot.
    legend_fontsize : int|str
        The fontsize for the legend. Can be an integer or a string from {'xx-small', 'x-small', 'small', 'medium', 'large', 'x-large', 'xx-large'}.
    x_log_scale : bool
        Set according to the x_type value, can be overriden by the user, to tell if the X-axis has to have a log scale or not.
    x_start : int|float
        The left limit of the X-axis.
    x_end : int|float
        The right limit of the X-axis.
    display_legend : bool
        Whether the legend has to be displayed or not, default to True.
    title : str
        The plot title.
    title_fontweight : {a numeric value in range 0-1000, 'ultralight', 'light', 'normal', 'regular', 'book', 'medium', 'roman', 'semibold', 'demibold', 'demi', 'bold', 'heavy', 'extra bold', 'black'}
        The font weight of the plot title, default to "normal".
    giga_bw : bool
        Wether to use GB/s instead of MB/s in Y axis legends. Default to False.
    fig : Figure
        Optional figure to plot the graph
    ax : Axis
        Optional left axis
    ax_right : Axis
        Optional right axis
    legend_position : str
        A value from ['lower', 'upper'].
    """
    _X_LABELS = {
        "byte": "Size of transmitted data",
        "thread": "Number of computing Cores",
        "cursor": "Arithmetic Intensity (FLOP/B)\n← Memory-bound ⇹ CPU-bound →"
    }
    _FILL_ALPHA = 0.3
    _nb_instances = 1  # to interleave generation of several plots, plt.figure indexing starts with 1
    mega2giga_formatter = FuncFormatter(lambda x, _: int(x / 1000))

    def __init__(self,
            x_range,
            x_type: str,
            comm_type: CommCompGraphCommType,
            comp_type: CompType,
            comp_metric: CompMetric,
            comm_legend_title=None,
            comp_legend_title=None,
            title=None,
            title_fontweight="normal",
            legend_fontsize="medium",
            x_span_zone=None,
            giga_bw=False,
            fig=None,
            ax=None,
            ax_right=None,
            legend_position="lower"):
        if x_type not in self._X_LABELS:
            raise Exception("Unknown x_type " + x_type)

        plt.ioff()  # for usage in Jupyter

        self._nb_comm_curves = 0
        self._nb_comp_curves = 0
        self._colors = iter(plt.rcParams["axes.prop_cycle"].by_key()["color"])
        self._comm_type = comm_type
        self._comp_type = comp_type
        self._comp_metric = comp_metric
        self._x_type = None
        self.ax_right = None
        self._current_color = next(self._colors)
        self.per_core = False
        self.comm_top_limit = None
        self.comp_top_limit = None
        self.fig_size = CommCompGraphFormat.SMALL
        self._x_range = x_range
        self._x_type = x_type
        self._plotted = False
        self.comm_legend_title = comm_legend_title
        self.comp_legend_title = comp_legend_title
        self.legend_fontsize = legend_fontsize  # int or {'xx-small', 'x-small', 'small', 'medium', 'large', 'x-large', 'xx-large'}
        self.x_log_scale = None  # if user provides a boolean, it will override the default settings according to x_type
        self.x_start = None
        self.x_end = None
        self.display_legend = True
        self.title = title
        self.title_fontweight = title_fontweight
        self._left_y_max = 0
        self._right_y_max = 0
        self._instance_id = CommCompGraph._nb_instances
        self.giga_bw = giga_bw
        self._in_grid = False
        self.last_comm_color = None
        self.last_comp_color = None

        assert(legend_position in ["lower", "upper"])
        self.legend_position = legend_position

        if fig is not None:
            assert(ax is not None)
            assert(ax_right is not None)
            self.fig = fig
            self.ax = ax
            self.ax_right = ax_right
            self._in_grid = True
        else:
            self.fig = plt.figure(self._instance_id)
            self.ax = self.fig.gca()

            CommCompGraph._nb_instances += 1

        self.ax.grid()
        self.ax.set_xlabel(self.x_label)

        if x_span_zone is not None:
            nb_zones = math.ceil(max(self._x_range) / x_span_zone)
            for i in range(0, nb_zones, 2):
                self.ax.axvspan((i*x_span_zone) + 0.5, ((i+1)*x_span_zone)+0.5, facecolor="0.9", zorder=0.1)

    @property
    def last_used_color(self):
        return self._current_color

    @property
    def nb_curves(self):
        return self._nb_comm_curves + self._nb_comp_curves

    def _add_curve(self, ax, line_values, label, curve_type, use_previous_color, min_values, max_values, display_line, markersize, marker=None):
        if (min_values is None and max_values is not None) or (min_values is not None and max_values is None):
            raise Exception("You have to provide either both min and max values or none.")

        if (self.nb_curves > 0 and not use_previous_color) or (self.nb_curves == 0 and ax == self.ax_right):
            # We are plotting another curve, and we want to change color, so do it
            # or
            # the first curve is a comp one, so skip the first color, usually used for comm
            self._current_color = next(self._colors)

        line_style = "-"
        if curve_type == CommCompGraphCurveType.PARALLEL:
            line_style = "--"

        curve_style = line_style
        if not display_line:
            min_values = None
            max_values = None
            if marker is None:
                if curve_type == CommCompGraphCurveType.PARALLEL:
                    curve_style = "+"
                else:
                    curve_style = "."
            else:
                curve_style = marker

        ax.plot(self._x_range, line_values, curve_style, label=label, color=self._current_color, markersize=markersize)

        if min_values is not None:
            assert(max_values is not None)
            ax.fill_between(self._x_range, min_values, max_values, linestyle=line_style, color=self._current_color, alpha=self._FILL_ALPHA)

    def add_comm_curve(self, line_values, label, curve_type, use_previous_color=False, min_values=None, max_values=None, display_line=True, markersize=plt.rcParams['lines.markersize'], marker=None):
        self._add_curve(self.ax, line_values, label, curve_type, use_previous_color, min_values, max_values, display_line, markersize, marker)
        self._nb_comm_curves += 1
        self._left_y_max = max(self._left_y_max, max(line_values))
        self.last_comm_color = self.last_used_color


    def add_comp_curve(self, line_values, label, curve_type, use_previous_color=False, min_values=None, max_values=None, display_line=True, markersize=plt.rcParams['lines.markersize'], marker=None):
        if self.ax_right is None:
            self.ax_right = self.ax.twinx()

        self._add_curve(self.ax_right, line_values, label, curve_type, use_previous_color, min_values, max_values, display_line, markersize, marker)
        self._nb_comp_curves += 1
        self._right_y_max = max(self._right_y_max, max(line_values))
        self.last_comp_color = self.last_used_color

    def add_vertical_line(self, x, **kwargs):
        self.ax.axvline(x, **kwargs)

    def gca(self):
        return self.ax

    @property
    def right_y_label(self):
        if self.per_core:
            s = self._comp_type.y_axis_label_per_core[self._comp_metric]
        else:
            s = self._comp_type.y_axis_label[self._comp_metric]

        if self.giga_bw:
            assert("MB/s" in s)
            s = s.replace("MB/s", "GB/s")

        return s

    @property
    def left_y_label(self):
        if self._comm_type == CommCompGraphCommType.BANDWIDTH:
            if self.giga_bw:
                return "Network Bandwidth (GB/s)"
            else:
                return "Network Bandwidth (MB/s)"
        else:
            return "Network Latency (µs)"

    @property
    def x_label(self):
        return self._X_LABELS[self._x_type]

    def _plot(self):
        plt.figure(self._instance_id)

        shrink_plot_legend = {  # dict to tell how much to shrink the plot according to the number of legend rows:
            1: 0.8,
            2: 0.8,
            3: 0.77,
            4: 0.72,
            5: 0.65
        }

        legend_bbox_to_anchor = (-0.17, 1.02, 1.34, 0.2)
        if self.legend_position == "lower":
            legend_bbox_to_anchor = (-0.17, -0.36, 1.34, 0.2)

        if self.title is not None:
            if self.display_legend and not self._in_grid:
                self.fig.suptitle(self.title, fontweight=self.title_fontweight)
            else:
                self.ax.set_title(self.title, {'fontweight': self.title_fontweight})

        shrink_plot_x_label = 0.1
        if self._x_type == "cursor":
            shrink_plot_x_label = 0.15

        if (self._x_type == "byte" and (self.x_log_scale is None or self.x_log_scale is True)) or self.x_log_scale is True:
            # scale must be set before the formatter
            self.ax.set_xscale("log", base=2)

        if self._x_type == "byte":
            self.ax.xaxis.set_major_formatter(byte_formatter)
        elif self._x_type == "cursor":
            self.ax.set_xlim(left=self._x_range[0])
        elif self._x_type == "thread":
            # Force integers as X-axis labels:
            self.ax.xaxis.set_major_locator(MaxNLocator(integer=True))


        if self.x_start is not None:
            self.ax.set_xlim(left=self.x_start)
        if self.x_end is not None:
            self.ax.set_xlim(right=self.x_end)

        if self.fig_size != CommCompGraphFormat.SMALL and not self._in_grid:
            self.fig.set_size_inches(*self.fig_size.value)

        # Left Y axis (communications):
        nb_items_comm_legend = 0
        if self._nb_comm_curves > 0:
            self.ax.set_ylabel(self.left_y_label, color=self.last_comm_color)
            if self._comm_type == CommCompGraphCommType.BANDWIDTH and self.giga_bw:
                self.ax.yaxis.set_major_formatter(self.mega2giga_formatter)

            if self.display_legend:
                nb_items_comm_legend = self._nb_comm_curves
                if self.comm_legend_title is not None:
                    nb_items_comm_legend += 1
                if not self._in_grid:
                    leg = self.ax.legend(
                        bbox_to_anchor=legend_bbox_to_anchor,
                        loc=f"{self.legend_position} left",
                        title=self.comm_legend_title,
                        fontsize=self.legend_fontsize
                    )
                    leg._legend_box.align = "left"

            if self.comm_top_limit is not None:
                self.ax.set_ylim(top=self.comm_top_limit)
            else:
                self.ax.set_ylim(top=self._left_y_max*1.1)

            self.ax.set_ylim(bottom=0)
        else:
            self.ax.set_yticks([])

        # Right Y axis (computations):
        nb_items_comp_legend = 0
        if self._nb_comp_curves > 0:
            assert(self.ax_right is not None)

            if not self._in_grid:
                plt.subplots_adjust(right=0.85)  # otherwise the right Y-label is cropped on slide format

            if self._nb_comp_curves > 2:
                self.ax_right.set_ylabel(self.right_y_label)
            else:
                self.ax_right.set_ylabel(self.right_y_label, color=self.last_comp_color)

            if self.giga_bw:
                self.ax_right.yaxis.set_major_formatter(self.mega2giga_formatter)

            if self.display_legend:
                nb_items_comp_legend = self._nb_comp_curves
                if self.comp_legend_title is not None:
                    nb_items_comp_legend += 1
                if not self._in_grid:
                    leg = self.ax_right.legend(
                        bbox_to_anchor=legend_bbox_to_anchor,
                        loc=f"{self.legend_position} right",
                        markerfirst=False,
                        title=self.comp_legend_title,
                        fontsize=self.legend_fontsize
                    )
                    leg._legend_box.align = "right"

            self.ax_right.set_ylim(bottom=0)

            if self.comp_top_limit is not None:
                self.ax_right.set_ylim(top=self.comp_top_limit)
            else:
                self.ax_right.set_ylim(top=self._right_y_max*1.1)

            if self._nb_comm_curves == 0:
                self.ax_right.grid()

        if self.nb_curves > 0 and self.display_legend and not self._in_grid:
            if self.legend_position == "upper":
                plt.subplots_adjust(
                    top=shrink_plot_legend[max(nb_items_comm_legend, nb_items_comp_legend)],
                    bottom=shrink_plot_x_label
                )
            else:
                plt.subplots_adjust(bottom=0.24, top=0.93)

        self._plotted = True

    def show(self):
        plt.figure(self._instance_id)
        if not self._plotted:
            self._plot()

        plt.show()

    def save(self, filename):
        plt.figure(self._instance_id)
        if not self._plotted:
            self._plot()

        plt.savefig(filename, dpi=100)

    def close(self):
        plt.close(self.fig)
