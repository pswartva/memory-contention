import copy
import statistics

from .comm_comp import FilesParser, CompMetric
from .hwloc import HwlocTopology


class CommCompModel:
    """
    A class to build a model from output of bench_openmp and predict
    degradation of computations and communications when executed in parallel.
    """

    def __init__(self, files, predict_now=True):
        # Model parameters:
        self.max_together_i = 0
        self.max_together_value = 0
        self.max_comp_alone_i = 0
        self.max_comp_alone_value = 0  # only for comp_alone_model
        self.together_when_max_alone = 0
        self.impacted_network_ratio = 0
        self.degradation_per_core_left = 0
        self.degradation_per_core_right = 0
        self.comp_mem_req = 0
        self.comm_alone_bw = 0
        self.xs = []

        # Other:
        self.total_model = None
        self.comm_with_comp_model = None
        self.comp_with_comm_model = None
        self.comm_alone_model = None
        self.comp_alone_model = None
        self.x_model = None

        parser = FilesParser(files)
        self.xs = parser.flatten_results["x"]

        comm_with_comp_bw = parser.flatten_results['comm']['with_comp']['bw']['med']
        self.comm_alone_bw = comm_with_comp_bw[0] if 'alone' not in parser.flatten_results['comm'] else statistics.mean(parser.flatten_results['comm']['alone']['bw']['med'])

        comp_bw_alone = [sum(parser.comp_alone_per_thread_results['memset'][x][CompMetric.BANDWIDTH]['avg']) for x in self.xs]
        comp_bw_with_comm = [sum(parser.comp_with_comm_per_thread_results['memset'][x][CompMetric.BANDWIDTH]['avg']) for x in self.xs]

        total_parallel = [sum(v) for v in zip(comp_bw_with_comm, comm_with_comp_bw)]

        # 1/ Get by how much the network performance can be impacted in the worst case
        # In some experiments, the network performances can be slightly better with computations, so make sure
        self.impacted_network_ratio = min([comm_with_comp_bw[i] / self.comm_alone_bw for i in range(len(self.xs))])

        # 2/ Find what is the maximum memory throughput when both computations and communications are performed:
        # We consider performances when comp and comm are executed in parallel, because on inter-processor buses,
        # it can lead to a bandwidth higher than computations alone.
        for i in range(len(self.xs)):
            if comp_bw_alone[i] > self.max_comp_alone_value:
                self.max_comp_alone_value = comp_bw_alone[i]
                self.max_comp_alone_i = i
        # Consider max_together happens with less or same number of lonely computing cores:
        # (If it's not the case, it means we are on the plateau of the total
        # curve and it's only a minor variation. See occigen-bw/comp_1_comm1.)
        for i in range(self.max_comp_alone_i+1):
            if total_parallel[i] > self.max_together_value:
                self.max_together_value = total_parallel[i]
                self.max_together_i = i
        assert(self.max_together_i <= self.max_comp_alone_i)
        self.together_when_max_alone = total_parallel[self.max_comp_alone_i]

        # Memory controllers (for the RAM or UPI) don't scale well,
        # and their performances decrease when there are too many
        # parallel requests (too many cores)
        # We lineraly model this degradation, with curve change when
        # contention starts with even lonely computations. This allows
        # for a better model when comm+comp > comp_alone.
        if self.max_comp_alone_i > self.max_together_i:
            self.degradation_per_core_left = (self.max_together_value - total_parallel[self.max_comp_alone_i]) / (self.xs[self.max_comp_alone_i] - self.xs[self.max_together_i])
        self.degradation_per_core_right = (total_parallel[self.max_comp_alone_i] - total_parallel[-1]) / (self.xs[-1] + 1 - self.xs[self.max_comp_alone_i])

        # 4/ Characterize computations: what is the memory througput of one core,
        # and what the maximum memory throughput the system can deliver:
        assert(self.xs[0] == 1)
        self.comp_mem_req = comp_bw_alone[0]

        self.x_model = list(range(1, self.xs[-1]+1))
        self.comm_alone_model = [self.comm_alone_bw] * len(self.x_model)

        if predict_now:
            self.predict()

    def print_params(self):
        print(f"max_together_i:             {self.max_together_i:8d} [x={self.xs[self.max_together_i]}]")
        print(f"max_together_value:         {self.max_together_value:10.1f}")
        print(f"max_comp_alone_i:           {self.max_comp_alone_i:8d} [x={self.xs[self.max_comp_alone_i]}]")
        print(f"max_comp_alone_value:       {self.max_comp_alone_value:10.1f}")
        print(f"together_when_max_alone:    {self.together_when_max_alone:10.1f}")
        print(f"impacted_network_ratio:     {self.impacted_network_ratio:12.3f}")
        print(f"degradation_per_core_left:  {self.degradation_per_core_left:10.1f}")
        print(f"degradation_per_core_right: {self.degradation_per_core_right:10.1f}")
        print(f"comp_mem_req:               {self.comp_mem_req:10.1f}")
        print(f"comm_alone_bw:              {self.comm_alone_bw:10.1f}")
        print(f"xs:                         {self.xs}")


    def predict(self):
        self.__predict_total()
        self.__predict_comm_comp()


    def __predict_total(self):
        # Compute what is the total throughput the memory system can support:
        # This is just a trick to know later more easily if we reach the maximum
        # or not. The values can be different (higher) than lonely computations,
        # for inter-processors links, for instance.
        self.total_model = []
        for i in range(len(self.x_model)):
            if self.x_model[i] <= self.xs[self.max_together_i]:
                self.total_model.append(self.max_together_value)
            elif self.x_model[i] <= self.xs[self.max_comp_alone_i]:
                self.total_model.append(self.max_together_value - (self.degradation_per_core_left * (self.x_model[i]-self.xs[self.max_together_i])))
            else:
                self.total_model.append(self.together_when_max_alone - (self.degradation_per_core_right * (self.x_model[i]-self.xs[self.max_comp_alone_i])))


    def __predict_comm_comp(self):
        self.comm_with_comp_model = []
        self.comp_with_comm_model = []
        self.comp_alone_model = []

        last_comm_ratio_left_value = None
        last_comm_ratio_left_i = None

        for i in range(len(self.x_model)):
            # The comp alone perf can't be higher than the perfect scaling, the total measured and the max observed comp alone perf:
            self.comp_alone_model.append(min(self.comp_mem_req*self.x_model[i], self.total_model[i], self.max_comp_alone_value))

            if self.comp_mem_req*self.x_model[i] + self.impacted_network_ratio*self.comm_alone_bw < self.total_model[i]:
                # We didn't reach the memory system limit yet, the begining of the plateau.
                # Computation performances scale perfectly, and are not disturbed:
                self.comp_with_comm_model.append(self.comp_mem_req*self.x_model[i])
                # Communications get the remaining of the system capabilities,
                # but are throttled by their performances obtained without computations:
                self.comm_with_comp_model.append(min(self.comm_alone_bw, self.total_model[i]-self.comp_with_comm_model[-1]))
                last_comm_ratio_left_value = self.comm_with_comp_model[-1] / self.comm_alone_bw
                assert(last_comm_ratio_left_value <= 1)
                last_comm_ratio_left_i = i

                # Was true for experiences on several machines.
                # Let's keep it and see what is happening when this assertion will become false.
                assert(self.x_model[i] <= self.xs[self.max_together_i])
            else:
                # The communications can be affected, so apply the ratio:
                r = self.impacted_network_ratio
                if (self.max_comp_alone_i-self.max_together_i) > 1 and self.x_model[i] < self.xs[self.max_comp_alone_i]:
                    # The plateau of communication degradation isn't reached yet and
                    # the gap between max_together_i and max_comp_alone_i is large
                    # (case when sub-numa clustering is on):
                    # we lineraly approximate the communication degradation between
                    # the last ratio when system memory limit wasn't reached and when
                    # it will be reached.
                    # This is important because we start by modeling bandwidth for
                    # communications and then we give the remaining to computations.
                    assert(self.xs[self.max_comp_alone_i] != self.x_model[last_comm_ratio_left_i])
                    comm_ratio_degradation_per_core = (last_comm_ratio_left_value - self.impacted_network_ratio) / (self.xs[self.max_comp_alone_i] - self.x_model[last_comm_ratio_left_i])
                    r = last_comm_ratio_left_value - (comm_ratio_degradation_per_core * (self.x_model[i] - self.x_model[last_comm_ratio_left_i]))
                self.comm_with_comp_model.append(r*self.comm_alone_bw)

                # The remaining of the bandwidth is for computations:
                self.comp_with_comm_model.append(self.total_model[i]-self.comm_with_comp_model[i])


class CombinedCommCompModel:
    def __init__(self, topology: HwlocTopology, model_local: CommCompModel, model_remote: CommCompModel=None):
        self.topology = topology
        self.model_local = model_local
        self.model_remote = model_remote

    def comm_with_comp(self, numa_comm, numa_comp):
        if numa_comp == numa_comm and numa_comp >= self.topology.nb_numa_nodes_per_socket:
            # Remote accesses, same memory controller:
            return self.model_remote.comm_with_comp_model
        else:
            comm_model = copy.deepcopy(self.model_local)
            if numa_comm >= self.topology.nb_numa_nodes_per_socket:
                # On some machines (eg AMD with IB 200GB/s) the network
                # performance is very sensible to placement, so use the
                # nominal remote perf for all configuration where the comm
                # memory is bound remotely.
                comm_model.comm_alone_bw = self.model_remote.comm_alone_bw
                comm_model.predict()
            return comm_model.comm_with_comp_model

    def comp_with_comm(self, numa_comm, numa_comp):
        if numa_comp < self.topology.nb_numa_nodes_per_socket:
            # Computations do local accesses:
            if numa_comm == numa_comp:
                return self.model_local.comp_with_comm_model
            else:
                return self.model_local.comp_alone_model
        else:
            # Computations do remote acceses:
            if numa_comm == numa_comp:
                return self.model_remote.comp_with_comm_model
            else:
                return self.model_remote.comp_alone_model

    def total(self, numa_comp):
        if numa_comp < self.topology.nb_numa_nodes_per_socket:
            # Computations do local accesses:
            return self.model_local.total_model
        else:
            # Computations do remote acceses:
            return self.model_remote.total_model

    def is_sample(self, numa_comm, numa_comp):
        """Returns wether the couple (numa_comm, numa_comp) is one of those
        used to instanciate the model.

        TODO: improve this, by asking self.model_{local,remote} on which NUMA
        nodes was bound memory to instanciate the model and compares it to the
        method arguments.
        """
        return numa_comm == numa_comp and numa_comm in [0, self.topology.nb_numa_nodes_per_socket]
