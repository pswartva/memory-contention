#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "helper_stream.h"

char stream_bench_done[] = {0, 0, 0};
char bench_to_run[] = {1, 1, 1, 1};
volatile int* stream_comm_bench_ended = NULL;
double stream_perfs_warmup[4][3];
double stream_perfs_no_comm[4][3];
double stream_perfs_comm[4][3];
double stream_time_warmup[4][3];
double stream_time_no_comm[4][3];
double stream_time_comm[4][3];

void stream_set_comm_bench_ended_ptr(volatile int* _comm_bench_ended)
{
    stream_comm_bench_ended = _comm_bench_ended;
}

static void print_stream_result_lines(char bench_to_run[4], double results[4][3], double times[4][3])
{
    char* stream_labels[4] = {
	    "Copy:      ",
	    "Scale:     ",
	    "Add:       ",
	    "Triad:     "
    };

    for (int i = 0; i < 4; i++)
    {
	if (bench_to_run[i])
	{
	    printf("# %s\t"COMP_BW_FORMAT"\t"COMP_BW_FORMAT"\t"COMP_BW_FORMAT"\t"COMP_TIME_FORMAT"\t"COMP_TIME_FORMAT"\t"COMP_TIME_FORMAT"\n",
		stream_labels[i],
		results[i][0], results[i][1], results[i][2],
		times[i][0]*1000, times[i][1]*1000, times[i][2]*1000);
	}
    }
}

int stream_parse_arg(char* arg)
{
    if (strcmp(arg, "--no_stream_copy") == 0)
    {
	bench_to_run[COPY] = 0;
	return 1;
    }
    if (strcmp(arg, "--no_stream_add") == 0)
    {
	bench_to_run[ADD] = 0;
	return 1;
    }
    if (strcmp(arg, "--no_stream_scale") == 0)
    {
	bench_to_run[SCALE] = 0;
	return 1;
    }
    if (strcmp(arg, "--no_stream_triad") == 0)
    {
	bench_to_run[TRIAD] = 0;
	return 1;
    }

    return 0;
}

void stream_man()
{
    printf("STREAM-related options:\n");
    printf("\t--no_stream_copy\n");
    printf("\t--no_stream_add\n");
    printf("\t--no_stream_scale\n");
    printf("\t--no_stream_triad\n");
}

void stream_print_params()
{
    printf("# Stream kernels to execute: ");
    if (bench_to_run[COPY])
    {
	printf("copy ");
    }
    if (bench_to_run[ADD])
    {
	printf("add ");
    }
    if (bench_to_run[SCALE])
    {
	printf("scale ");
    }
    if (bench_to_run[TRIAD])
    {
	printf("triad ");
    }
    printf("\n");
}

void stream_print_results()
{
    printf("# STREAM results: Bandwidth MB/s (max, avg, min) Time ms (min, avg, max)\n");
    if (stream_bench_done[WARMUP])
    {
	printf("# warmup:\n");
	print_stream_result_lines(bench_to_run, stream_perfs_warmup, stream_time_warmup);
    }
    if (stream_bench_done[WITH_COMM])
    {
	printf("# with communications:\n");
	print_stream_result_lines(bench_to_run, stream_perfs_comm, stream_time_comm);
    }
    if (stream_bench_done[WITHOUT_COMM])
    {
	printf("# without communications:\n");
	print_stream_result_lines(bench_to_run, stream_perfs_no_comm, stream_time_no_comm);
    }
}

int stream_get_nb_runs(enum comm_bench_type comm_bench_type)
{
    return 20;
}
