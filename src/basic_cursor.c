/* Global idea: the cursor defines how much the CPU is computing
 * (= not making memory accesses) between each memory access. */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define ARRAY_TYPE double
#define ARRAY_SIZE 100000000  // * 8 Bytes (double) =~ 763 MB

#define NB_RUNS 20

static ARRAY_TYPE a[ARRAY_SIZE], b[ARRAY_SIZE], c[ARRAY_SIZE];

void bench(int cursor)
{
    #pragma omp parallel for
    for (int i = 0; i < ARRAY_SIZE; i++)
    {
	for (int j = 0; j < cursor + 1; j++)
	{
	    b[i] += (a[i] + j * c[i]);
	}
    }
}

int main(int argc, char* argv[])
{
    int cursor = 50;

    if (argc == 2 && strcmp(argv[1], "-h") != 0 && strcmp(argv[1], "--help") != 0)
    {
	cursor = atoi(argv[1]);
    }
    else
    {
	printf("Usage: %s <cursor>\n", argv[0]);
	printf("When cursor is 0, the bench is full memory-bound\n");
	printf("The higher the cursor is, the more the bench is CPU-bound\n");
	
	return EXIT_FAILURE;
    }

    printf("# Cursor is %d\n", cursor);

    #pragma omp parallel for
    for (int i = 0; i < ARRAY_SIZE; i++)
    {
	a[i] = 2.7;
	b[i] = 0.0;
	c[i] = 3.14;
    }

    printf("# End of init\n");

    for (int i = 0; i < NB_RUNS; i++)
    {
	bench(cursor);
    }

    printf("# End of bench\n");

    return EXIT_SUCCESS;
}
