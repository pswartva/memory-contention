#ifndef __STREAM_WEAK_H
#define __STREAM_WEAK_H

#include "helper.h"

int stream_weak_run(int nb_runs, enum bench_type bench_type);
struct computing_functions stream_weak_get_functions();

#endif // __STREAM_WEAK_H
