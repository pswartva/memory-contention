#ifndef __HELPER_OPENMP_WEAK_H
#define __HELPER_OPENMP_WEAK_H

#include "helper.h"


extern ARRAY_TYPE **weak_a, **weak_b, **weak_c;
extern int weak_array_size;
extern int nb_threads;

int weak_init();
void weak_release();

#endif /* __HELPER_OPENMP_WEAK_H */
