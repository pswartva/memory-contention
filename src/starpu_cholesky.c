#include <float.h>
#include <stdlib.h>
#include <stdio.h>
#include <starpu.h>

#include "cholesky.h"

/* Inspired a lot by StarPU's example/cholesky/cholesky_implicit.c */


/* FLOPS formulas come from Magma library (see above comment for more details) */
#define FMULS_POTRF(__n) ((double)(__n) * (((1. / 6.) * (double)(__n) + 0.5) * (double)(__n) + (1. / 3.)))
#define FADDS_POTRF(__n) ((double)(__n) * (((1. / 6.) * (double)(__n)      ) * (double)(__n) - (1. / 6.)))

#define FLOPS_SPOTRF(__n) (     FMULS_POTRF((__n)) +       FADDS_POTRF((__n)) )

#define FMULS_TRSM(__m, __n) (0.5 * (double)(__n) * (double)(__m) * ((double)(__m)+1.))
#define FADDS_TRSM(__m, __n) (0.5 * (double)(__n) * (double)(__m) * ((double)(__m)-1.))

#define FLOPS_STRSM(__m, __n) (     FMULS_TRSM((__m), (__n)) +       FADDS_TRSM((__m), (__n)) )

#define FMULS_GEMM(__m, __n, __k) ((double)(__m) * (double)(__n) * (double)(__k))
#define FADDS_GEMM(__m, __n, __k) ((double)(__m) * (double)(__n) * (double)(__k))

#define FLOPS_SGEMM(__m, __n, __k) (     FMULS_GEMM((__m), (__n), (__k)) +       FADDS_GEMM((__m), (__n), (__k)) )


static float* mat = NULL;
static unsigned size = 320;
static unsigned tile_size = 320;
static unsigned nblocks;
static starpu_data_handle_t mat_handle;

static double cholesky_duration_warmup[] = {FLT_MAX, 0, 0};
static double cholesky_duration_no_comm[] = {FLT_MAX, 0, 0};
static double cholesky_duration_comm[] = {FLT_MAX, 0, 0};
static char bench_done[] = {0, 0, 0};
static volatile int* comm_bench_ended = NULL;


void cholesky_set_comm_bench_ended_ptr(volatile int* _comm_bench_ended)
{
    comm_bench_ended = _comm_bench_ended;
}

static void init_task(void *descr[], void *cl_arg)
{
    unsigned m, n, global_i, global_j;
    starpu_codelet_unpack_args(cl_arg, &m, &n);
    float* sub = (float *)STARPU_MATRIX_GET_PTR(descr[0]);
    unsigned nx = STARPU_MATRIX_GET_NX(descr[0]);
    unsigned ny = STARPU_MATRIX_GET_NY(descr[0]);
    unsigned ld = STARPU_MATRIX_GET_LD(descr[0]);

    for (unsigned i = 0; i < ny; i++)
    {
	global_i = m*tile_size + i;
	for (unsigned j = 0; j < nx; j++)
	{
	    global_j = n*tile_size + j;
	    sub[i + j*ld] = (1.0f/(1.0f+global_i+global_j)) + ((global_i == global_j)?1.0f*size:0.0f);
	}
    }
}

/* Provided by the MKL: */
extern void spotrf_(const char*uplo, const int *n, float *a, const int *lda, int *info);

static void potrf_task(void *descr[], void *cl_arg)
{
	(void) cl_arg;

	float* sub11 = (float *)STARPU_MATRIX_GET_PTR(descr[0]);

	int nx = (int) STARPU_MATRIX_GET_NY(descr[0]);
	int ld = (int) STARPU_MATRIX_GET_LD(descr[0]);
	int info = 0;

	spotrf_("L", &nx, sub11, &ld, &info);

	if (info != 0)
	{
	    printf("Wrong result of POTRF kernel: info = %d\n", info);
	    fflush(stdout);
	}
	assert(info == 0);
}

/* Provided by the MKL: */
extern void strsm_ (const char *side, const char *uplo, const char *transa,
                   const char *diag, const int *m, const int *n,
                   const float *alpha, const float *A, const int *lda,
                   float *B, const int *ldb);

static void trsm_task(void *descr[], void *_args)
{
	(void)_args;

	float* sub11 = (float *)STARPU_MATRIX_GET_PTR(descr[0]);
	float* sub21 = (float *)STARPU_MATRIX_GET_PTR(descr[1]);
	float alpha = 1.0f;

	int ld11 = (int) STARPU_MATRIX_GET_LD(descr[0]);
	int ld21 = (int) STARPU_MATRIX_GET_LD(descr[1]);

	int nx21 = (int) STARPU_MATRIX_GET_NY(descr[1]);
	int ny21 = (int) STARPU_MATRIX_GET_NX(descr[1]);

	strsm_("R", "L", "T", "N", &nx21, &ny21, &alpha, sub11, &ld11, sub21, &ld21);
}

/* Provided by the MKL: */
extern void sgemm_ (const char *transa, const char *transb, const int *m,
                   const int *n, const int *k, const float *alpha,
                   const float *A, const int *lda, const float *B,
                   const int *ldb, const float *beta, float *C,
                   const int *ldc);

static void gemm_task(void *descr[], void *_args)
{
	(void)_args;
	float *left 	= (float *)STARPU_MATRIX_GET_PTR(descr[0]);
	float *right 	= (float *)STARPU_MATRIX_GET_PTR(descr[1]);
	float *center 	= (float *)STARPU_MATRIX_GET_PTR(descr[2]);

	int dx = (int) STARPU_MATRIX_GET_NY(descr[2]);
	int dy = (int) STARPU_MATRIX_GET_NX(descr[2]);
	int dz = (int) STARPU_MATRIX_GET_NY(descr[0]);

	int ld21 = (int) STARPU_MATRIX_GET_LD(descr[0]);
	int ld12 = (int) STARPU_MATRIX_GET_LD(descr[1]);
	int ld22 = (int) STARPU_MATRIX_GET_LD(descr[2]);

	float alpha = -1.0f;
	float beta = 1.0f;

	sgemm_("N", "T", &dy, &dx, &dz, &alpha, left, &ld21, right, &ld12, &beta, center, &ld22);
}

static struct starpu_codelet init_cl =
{
    .cpu_funcs = {init_task},
    .cpu_funcs_name = {"init_task"},
    .nbuffers = 1,
    .modes = {STARPU_W},
    .name = "init_matrix",
    .color = 0x800000 // maroon
};

static struct starpu_codelet potrf_cl =
{
    .cpu_funcs = {potrf_task},
    .cpu_funcs_name = {"potrf_task"},
    .nbuffers = 1,
    .modes = {STARPU_RW},
    .name = "potrf",
    .color = 0xffff00 // yellow
};

static struct starpu_codelet trsm_cl =
{
    .cpu_funcs = {trsm_task},
    .cpu_funcs_name = {"trsm_task"},
    .nbuffers = 2,
    .modes = {STARPU_R, STARPU_RW},
    .name = "trsm",
    .color = 0x808000 // olive
};

static struct starpu_codelet gemm_cl =
{
    .cpu_funcs = {gemm_task},
    .cpu_funcs_name = {"gemm_task"},
    .nbuffers = 3,
    .modes = {STARPU_R, STARPU_R, STARPU_RW},
    .name = "gemm",
    .color = 0x008000 // green
};

int cholesky_init()
{
    unsigned nb_workers = starpu_worker_get_count();
    nblocks = nb_workers * 2;
    size = tile_size * nblocks;

    printf("# %u workers, matrix size: %u*%u, %u blocks\n", nb_workers, size, size, nblocks);
    fflush(stdout);

    starpu_malloc((void**) &mat, (size_t) size*size*sizeof(float));
    starpu_matrix_data_register(&mat_handle, STARPU_MAIN_RAM, (uintptr_t) mat, size, size, size, sizeof(float));

    /* Split into blocks of complete rows first */
    struct starpu_data_filter f =
    {
	.filter_func = starpu_matrix_filter_block,
	.nchildren = nblocks
    };

    /* Then split rows into tiles */
    struct starpu_data_filter f2 =
    {
	/* Note: here "vertical" is for row-major, we are here using column-major. */
	.filter_func = starpu_matrix_filter_vertical_block,
	.nchildren = nblocks
    };

    starpu_data_map_filters(mat_handle, 2, &f, &f2);

    return 0;
}

static double cholesky_run_kernel()
{
    int ret;
    double start_time, end_time;

    /* Fill blocks to get an Hilbert matrix: */
    for (unsigned m = 0; m < nblocks; m++)
    {
	for (unsigned n = 0; n <= m; n++)
	{
	    starpu_data_handle_t sdatamn = starpu_data_get_sub_data(mat_handle, 2, m, n);

	    ret = starpu_task_insert(&init_cl, STARPU_W, sdatamn, STARPU_VALUE, &m, sizeof(unsigned), STARPU_VALUE, &n, sizeof(unsigned), 0);
	    if (ret != 0)
	    {
		printf("Error on starpu_task_insert\n");
		abort();
	    }
	}
    }

    starpu_task_wait_for_all();
    /* Starting tasks only after submitting all tasks gives better performance
     * and avoid "holes" in scheduling, so memory pressure should be more constant.
     */
    starpu_pause();

    for (unsigned k = 0; k < nblocks; k++)
    {
	starpu_data_handle_t sdatakk = starpu_data_get_sub_data(mat_handle, 2, k, k);

	ret = starpu_task_insert(
		&potrf_cl,
		STARPU_PRIORITY, (int)(2*nblocks - 2*k),
		STARPU_RW, sdatakk,
		STARPU_FLOPS, (double) FLOPS_SPOTRF(tile_size),
		0);
	if (ret != 0)
	{
	    abort();
	}

	for (unsigned m = k+1; m < nblocks; m++)
	{
	    starpu_data_handle_t sdatamk = starpu_data_get_sub_data(mat_handle, 2, m, k);

	    ret = starpu_task_insert(
		    &trsm_cl,
		    STARPU_PRIORITY, (int)(2*nblocks - 2*k - m),
		    STARPU_R, sdatakk,
		    STARPU_RW, sdatamk,
		    STARPU_FLOPS, (double) FLOPS_STRSM(tile_size, tile_size),
		    0);
	    if (ret != 0)
	    {
		abort();
	    }
	}
	starpu_data_wont_use(sdatakk);

	for (unsigned n = k+1; n < nblocks; n++)
	{
	    starpu_data_handle_t sdatank = starpu_data_get_sub_data(mat_handle, 2, n, k);
	    for (unsigned m = n; m < nblocks; m++)
	    {
		starpu_data_handle_t sdatamk = starpu_data_get_sub_data(mat_handle, 2, m, k);
		starpu_data_handle_t sdatamn = starpu_data_get_sub_data(mat_handle, 2, m, n);

		ret = starpu_task_insert(
			&gemm_cl,
			STARPU_PRIORITY, (int)(2*nblocks - 2*k - m - n),
			STARPU_R, sdatamk,
			STARPU_R, sdatank,
			STARPU_RW, sdatamn,
			STARPU_FLOPS, (double) FLOPS_SGEMM(tile_size, tile_size, tile_size),
			0);
		if (ret != 0)
		{
		    abort();
		}
	    }
	    starpu_data_wont_use(sdatank);
	}
    }

    start_time = starpu_timing_now(); // us
    starpu_resume();
    starpu_task_wait_for_all();
    end_time = starpu_timing_now();

    return end_time - start_time;
}

int cholesky_run(int nb_runs, enum bench_type bench_type)
{
    if (bench_type != WARMUP && bench_done[bench_type])
    {
	printf("Warning: this bench was already done.\n");
    }

    double avgtime = 0, maxtime = 0, mintime = FLT_MAX;
    int real_nb_runs = nb_runs;
    double duration;

    for (int k = 0; k < nb_runs; k++)
    {
	duration = cholesky_run_kernel();

	mintime = MIN(mintime, duration);
	avgtime += duration;
	maxtime = MAX(maxtime, duration);
    }

    if (bench_type == WITH_COMM && comm_bench_ended != NULL)
    {
	/* Keep computing while we need more pingpongs: */
	while (!*comm_bench_ended)
	{
	    duration = cholesky_run_kernel();

	    mintime = MIN(mintime, duration);
	    avgtime += duration;
	    maxtime = MAX(maxtime, duration);

	    real_nb_runs++;
	}
    }

    if (bench_done[bench_type])
    {
	return 0;
    }

    if (bench_type == WARMUP)
    {
	cholesky_duration_warmup[0] = mintime;
	cholesky_duration_warmup[1] = avgtime / (double) (real_nb_runs);
	cholesky_duration_warmup[2] = maxtime;
    }
    else if (bench_type == WITH_COMM)
    {
	cholesky_duration_comm[0] = mintime;
	cholesky_duration_comm[1] = avgtime / (double) (real_nb_runs);
	cholesky_duration_comm[2] = maxtime;
    }
    else if (bench_type == WITHOUT_COMM)
    {
	cholesky_duration_no_comm[0] = mintime;
	cholesky_duration_no_comm[1] = avgtime / (double) (real_nb_runs);
	cholesky_duration_no_comm[2] = maxtime;
    }
    else
    {
	abort();
    }

    bench_done[bench_type] = 1;

    return 0;
}

void cholesky_release()
{
    starpu_data_unpartition(mat_handle, STARPU_MAIN_RAM);
    starpu_data_unregister(mat_handle);
    starpu_free_noflag(mat, size*size*sizeof(float));
}

void cholesky_print_results()
{
    printf("# Cholesky performances:      duration (ms - min/avg/max)              Gflops (min/avg/max)\n");
    if (bench_done[WARMUP])
    {
	printf("# warmup:                  % 12.3f % 12.3f %12.3f  % 12.3f % 12.3f % 12.3f\n",
		cholesky_duration_warmup[0] / 1000,
		cholesky_duration_warmup[1] / 1000,
		cholesky_duration_warmup[2] / 1000,
		FLOPS_SPOTRF(size) / (cholesky_duration_warmup[2] * 1000),
		FLOPS_SPOTRF(size) / (cholesky_duration_warmup[1] * 1000),
		FLOPS_SPOTRF(size) / (cholesky_duration_warmup[0] * 1000));
    }
    if (bench_done[WITH_COMM])
    {
	printf("# with communications:     % 12.3f % 12.3f %12.3f  % 12.3f % 12.3f % 12.3f\n",
		cholesky_duration_comm[0] / 1000,
		cholesky_duration_comm[1] / 1000,
		cholesky_duration_comm[2] / 1000,
		FLOPS_SPOTRF(size) / (cholesky_duration_comm[2] * 1000),
		FLOPS_SPOTRF(size) / (cholesky_duration_comm[1] * 1000),
		FLOPS_SPOTRF(size) / (cholesky_duration_comm[0] * 1000));
    }
    if (bench_done[WITHOUT_COMM])
    {
	printf("# without communications:  % 12.3f % 12.3f %12.3f  % 12.3f % 12.3f % 12.3f\n",
		cholesky_duration_no_comm[0] / 1000,
		cholesky_duration_no_comm[1] / 1000,
		cholesky_duration_no_comm[2] / 1000,
		FLOPS_SPOTRF(size) / (cholesky_duration_no_comm[2] * 1000),
		FLOPS_SPOTRF(size) / (cholesky_duration_no_comm[1] * 1000),
		FLOPS_SPOTRF(size) / (cholesky_duration_no_comm[0] * 1000));
    }
}

int cholesky_get_nb_runs(enum comm_bench_type comm_bench_type)
{
    if (comm_bench_type == BANDWIDTH)
    {
	unsigned nb_workers = starpu_worker_get_count();
	if (nb_workers < 6)
	{
	    return 100;
	}
	else if (nb_workers < 10)
	{
	    return 50;
	}
	else if (nb_workers < 20)
	{
	    return 10;
	}
	else
	{
	    return 5;
	}
    }
    else // latency benchmark
    {
	return 10;
    }
}

struct computing_functions cholesky_get_functions()
{
    struct computing_functions s = {
	.init = &cholesky_init,
	.run = &cholesky_run,
	.print_results = &cholesky_print_results,
	.print_params = NULL,
	.release = &cholesky_release,
	.parse_arg = NULL,
	.man = NULL,
	.get_nb_runs = &cholesky_get_nb_runs,
	.set_comm_bench_ended_ptr = &cholesky_set_comm_bench_ended_ptr,
	.name = "cholesky"
    };

    return s;
}
