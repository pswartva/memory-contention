#include <float.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <omp.h>

#include "timing.h"
#include "stream.h"
#include "helper_openmp_weak.h"
#include "helper_stream.h"

extern ARRAY_TYPE **weak_a, **weak_b, **weak_c;
extern int weak_array_size;
extern int nb_threads;


static void stream_weak_run_kernel(double avgtime[4], double maxtime[4], double mintime[4])
{
    puk_tick_t start_time, end_time;
    double durations[4];
    ARRAY_TYPE scalar = 3.0;

    /* COPY */
    if (bench_to_run[COPY])
    {
	PUK_GET_TICK(start_time);
	#pragma omp parallel for
	for (int i = 0; i < nb_threads; i++)
	    for (int j = 0; j < weak_array_size; j++)
		weak_c[i][j] = weak_a[i][j];
	PUK_GET_TICK(end_time);
	durations[COPY] = PUK_TIMING_DELAY(start_time, end_time) / 1000000.0f;
    }

    /* SCALE */
    if (bench_to_run[SCALE])
    {
	PUK_GET_TICK(start_time);
	#pragma omp parallel for
	for (int i = 0; i < nb_threads; i++)
	    for (int j = 0; j < weak_array_size; j++)
		weak_b[i][j] = scalar*weak_c[i][j];
	PUK_GET_TICK(end_time);
	durations[SCALE] = PUK_TIMING_DELAY(start_time, end_time) / 1000000.0f;
    }

    /* ADD */
    if (bench_to_run[ADD])
    {
	PUK_GET_TICK(start_time);
	#pragma omp parallel for
	for (int i = 0; i < nb_threads; i++)
	    for (int j = 0; j < weak_array_size; j++)
		weak_c[i][j] = weak_a[i][j]+weak_b[i][j];
	PUK_GET_TICK(end_time);
	durations[ADD] = PUK_TIMING_DELAY(start_time, end_time) / 1000000.0f;
    }

    /* TRIAD */
    if (bench_to_run[TRIAD])
    {
	PUK_GET_TICK(start_time);
	#pragma omp parallel for
	for (int i = 0; i < nb_threads; i++)
	    for (int j = 0; j < weak_array_size; j++)
		weak_a[i][j] = weak_b[i][j]+scalar*weak_c[i][j];
	PUK_GET_TICK(end_time);
	durations[TRIAD] = PUK_TIMING_DELAY(start_time, end_time) / 1000000.0f;
    }

    for (int j = 0; j < 4; j++)
    {
	if (bench_to_run[j])
	{
	    avgtime[j] = avgtime[j] + durations[j];
	    mintime[j] = MIN(mintime[j], durations[j]);
	    maxtime[j] = MAX(maxtime[j], durations[j]);
	}
    }
}

static int stream_weak_run(int nb_runs, enum bench_type bench_type)
{
    if (bench_type != WARMUP && stream_bench_done[bench_type])
    {
	printf("Warning: this bench was already done.\n");
    }

    int real_nb_runs = nb_runs;
    double avgtime[4] = {0}, maxtime[4] = {0}, mintime[4] = {FLT_MAX,FLT_MAX,FLT_MAX,FLT_MAX};
    double bytes[4] = {
	2 * sizeof(ARRAY_TYPE) * weak_array_size * nb_threads,
	2 * sizeof(ARRAY_TYPE) * weak_array_size * nb_threads,
	3 * sizeof(ARRAY_TYPE) * weak_array_size * nb_threads,
	3 * sizeof(ARRAY_TYPE) * weak_array_size * nb_threads
    };

    for (int k = 0; k < nb_runs; k++)
    {
	stream_weak_run_kernel(avgtime, maxtime, mintime);
    }

    if (bench_type == WITH_COMM && stream_comm_bench_ended != NULL)
    {
	/* Keep computing while we need more pingpongs: */
	while (!*stream_comm_bench_ended)
	{
	    stream_weak_run_kernel(avgtime, maxtime, mintime);
	    real_nb_runs++;
	}
    }

    if (stream_bench_done[bench_type])
    {
	return 0;
    }

    for (int j = 0; j < 4; j++)
    {
	if (bench_to_run[j])
	{
	    avgtime[j] = avgtime[j]/(double)(real_nb_runs);

	    if (bench_type == WARMUP)
	    {
		stream_perfs_warmup[j][0] = 1.0E-06 * bytes[j] / mintime[j];
		stream_perfs_warmup[j][1] = 1.0E-06 * bytes[j] / avgtime[j];
		stream_perfs_warmup[j][2] = 1.0E-06 * bytes[j] / maxtime[j];

		stream_time_warmup[j][0] = mintime[j];
		stream_time_warmup[j][1] = avgtime[j];
		stream_time_warmup[j][2] = maxtime[j];
	    }
	    else if (bench_type == WITH_COMM)
	    {
		stream_perfs_comm[j][0] = 1.0E-06 * bytes[j] / mintime[j];
		stream_perfs_comm[j][1] = 1.0E-06 * bytes[j] / avgtime[j];
		stream_perfs_comm[j][2] = 1.0E-06 * bytes[j] / maxtime[j];

		stream_time_comm[j][0] = mintime[j];
		stream_time_comm[j][1] = avgtime[j];
		stream_time_comm[j][2] = maxtime[j];
	    }
	    else if (bench_type == WITHOUT_COMM)
	    {
		stream_perfs_no_comm[j][0] = 1.0E-06 * bytes[j] / mintime[j];
		stream_perfs_no_comm[j][1] = 1.0E-06 * bytes[j] / avgtime[j];
		stream_perfs_no_comm[j][2] = 1.0E-06 * bytes[j] / maxtime[j];

		stream_time_no_comm[j][0] = mintime[j];
		stream_time_no_comm[j][1] = avgtime[j];
		stream_time_no_comm[j][2] = maxtime[j];
	    }
	    else
	    {
		abort();
	    }
	}
    }

    stream_bench_done[bench_type] = 1;

    return 0;
}


struct computing_functions stream_weak_get_functions()
{
    struct computing_functions s = {
	.init = &weak_init,
	.run = &stream_weak_run,
	.print_results = &stream_print_results,
	.print_params = &stream_print_params,
	.release = &weak_release,
	.parse_arg = &stream_parse_arg,
	.man = &stream_man,
	.get_nb_runs = &stream_get_nb_runs,
	.set_comm_bench_ended_ptr = &stream_set_comm_bench_ended_ptr,
	.name = "stream_weak"
    };

    return s;
}
