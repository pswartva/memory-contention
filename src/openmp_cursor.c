#include <float.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <omp.h>

#include "timing.h"
#include "cursor.h"
#include "helper_cursor.h"

#define ARRAY_SIZE 100000000  // * 8 Bytes (double) =~ 763 MB


#ifdef ALLOC_STATIC
static ARRAY_TYPE a[ARRAY_SIZE], b[ARRAY_SIZE], c[ARRAY_SIZE];
#else
static ARRAY_TYPE *a, *b, *c;
#endif


static int cursor_init()
{
#ifndef ALLOC_STATIC
    a = comp_malloc(ARRAY_SIZE * sizeof(ARRAY_TYPE));
    b = comp_malloc(ARRAY_SIZE * sizeof(ARRAY_TYPE));
    c = comp_malloc(ARRAY_SIZE * sizeof(ARRAY_TYPE));
#endif

    #pragma omp parallel for
    for (int i = 0; i < ARRAY_SIZE; i++)
    {
	a[i] = 2.7;
	b[i] = 0.0;
	c[i] = 3.14;
    }

    return 0;
}

void cursor_release()
{
#ifndef ALLOC_STATIC
    comp_free(a, ARRAY_SIZE * sizeof(ARRAY_TYPE));
    comp_free(b, ARRAY_SIZE * sizeof(ARRAY_TYPE));
    comp_free(c, ARRAY_SIZE * sizeof(ARRAY_TYPE));
#endif
}

static double cursor_run_kernel()
{
    puk_tick_t start_time, end_time;

    PUK_GET_TICK(start_time);
    #pragma omp parallel for
    for (int i = 0; i < ARRAY_SIZE; i++)
    {
	/* Arithmetic intensity:
	 * - we move 4 floats (we read a, b, c and write b): 4*4 = 16 B
	 * - we do 3 FLOPs per j; so 3*(cursor+1) FLOP
	 * => arithmetic intensity = 3*(cursor+1)/16 FLOP/B
	 */
	for (int j = 0; j < cursor + 1; j++)
	{
	    b[i] += (a[i] + j * c[i]);
	}
    }
    PUK_GET_TICK(end_time);

    return PUK_TIMING_DELAY(start_time, end_time);
}

int cursor_run(int nb_runs, enum bench_type bench_type)
{
    if (bench_type != WARMUP && cursor_bench_done[bench_type])
    {
	printf("Warning: this bench was already done.\n");
    }

    double avgtime = 0, maxtime = 0, mintime = FLT_MAX;
    int real_nb_runs = nb_runs;
    double duration;

    for (int k = 0; k < nb_runs; k++)
    {
	duration = cursor_run_kernel();

	mintime = MIN(mintime, duration);
	avgtime += duration;
	maxtime = MAX(maxtime, duration);
    }

    if (bench_type == WITH_COMM && cursor_comm_bench_ended != NULL)
    {
	/* Keep computing while we need more pingpongs: */
	while (!*cursor_comm_bench_ended)
	{
	    duration = cursor_run_kernel();

	    mintime = MIN(mintime, duration);
	    avgtime += duration;
	    maxtime = MAX(maxtime, duration);

	    real_nb_runs++;
	}
    }

    if (cursor_bench_done[bench_type])
    {
	return 0;
    }

    if (bench_type == WARMUP)
    {
	cursor_perfs_warmup[0] = mintime;
	cursor_perfs_warmup[1] = avgtime / (double) (real_nb_runs);
	cursor_perfs_warmup[2] = maxtime;
    }
    else if (bench_type == WITH_COMM)
    {
	cursor_perfs_comm[0] = mintime;
	cursor_perfs_comm[1] = avgtime / (double) (real_nb_runs);
	cursor_perfs_comm[2] = maxtime;
    }
    else if (bench_type == WITHOUT_COMM)
    {
	cursor_perfs_no_comm[0] = mintime;
	cursor_perfs_no_comm[1] = avgtime / (double) (real_nb_runs);
	cursor_perfs_no_comm[2] = maxtime;
    }
    else
    {
	abort();
    }

    cursor_bench_done[bench_type] = 1;

    return 0;
}

struct computing_functions cursor_get_functions()
{
    struct computing_functions s = {
	.init = &cursor_init,
	.run = &cursor_run,
	.print_results = &cursor_print_results,
	.print_params = &cursor_print_params,
	.release = &cursor_release,
	.parse_arg = &cursor_parse_arg,
	.man = &cursor_man,
	.get_nb_runs = &cursor_get_nb_runs,
	.set_comm_bench_ended_ptr = &cursor_set_comm_bench_ended_ptr,
	.name = "cursor"
    };

    return s;
}
