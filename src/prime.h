#ifndef __PRIME_H
#define __PRIME_H

#include "helper.h"

int nb_prime_numbers();

int prime_init();
int prime_run(int nb_runs, enum bench_type bench_type);
void prime_print_results();
void prime_release();
struct computing_functions prime_get_functions();

#endif // __PRIME_H
