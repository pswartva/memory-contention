
#define PRIME_MAX 10000 // upper bound of interval within prime numbers are searched

static void __attribute__((optimize("O0"))) dummy(void* var, void* var2)
{
    (void) var;
    (void) var2;
}

int __attribute__((optimize("O0"))) nb_prime_numbers()
{
    /* Very dumb way of counting prime numbers, but that's just to give lot of work to the CPU. */
    /* (work not involving memory accesses) */

    int prime_number = 2;
    int dividers = 0;
    float value = 3.14f; // operations on this variable only to let the CPU makes some floatting operations

    for (int i = 4; i < PRIME_MAX; i++)
    {
	dividers = 0;
	for (int k = 2; k < i; k++)
	{
	    if (i % k == 0)
	    {
		dividers++;
		value *= 2.5f;
	    }
	}

	if (dividers == 0)
	{
	    prime_number++;
	    value /= 2.5f;
	}
    }

    dummy((void*) &prime_number, (void*) &value); // confuse the compiler to avoid optimizations

    return prime_number;
}

