/* From PM2's Puk timing interface */
#ifndef __TIMING_H
#define __TIMING_H

#include <stdlib.h>
#include <time.h>


#ifdef CLOCK_MONOTONIC_RAW
#  define CLOCK_TYPE CLOCK_MONOTONIC_RAW
#else
#  define CLOCK_TYPE CLOCK_MONOTONIC
#endif

#define PUK_GET_TICK(t) clock_gettime(CLOCK_TYPE, &(t))
#define PUK_TIMING_DELAY(start, end) puk_ticks2delay(&(start), &(end)) // microseconds

typedef struct timespec puk_tick_t;

double puk_ticks2delay(const puk_tick_t*t1, const puk_tick_t *t2);

#endif /* __TIMING_H */
