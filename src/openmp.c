#define _GNU_SOURCE // for sched_getcpu
#include <stdlib.h>
#include <mpi.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <omp.h>
#include <hwloc.h>
#include <sched.h>

#include "helper.h"
#include "timing.h"


static float* send_buffers[COMM_BUFFER_POOL_SIZE];
static float* recv_buffers[COMM_BUFFER_POOL_SIZE];
static int current_comm_buffer = 0;
static int buffer_size;
static int rank;
static enum pingpong_type_e pingpong_type;


int get_nb_openmp_threads()
{
    int nb = 0;
    #pragma omp parallel
    {
	#pragma omp master
	{
	    nb = omp_get_num_threads();
        }
    }

    /* Check worker binding in this function, to check it at the beginning of
     * the program, in all cases.
     * The check can be done several times, because if the binding is wrong,
     * threads may move, and only one check can fail. */
    get_worker_binding_ids(NULL, nb, NULL);

    return nb;
}


void get_worker_binding_ids(hwloc_topology_t topo, int nb_threads, int* _thread_binding)
{
    int* thread_binding = _thread_binding;
    if (_thread_binding == NULL)
    {
	thread_binding = malloc(nb_threads*sizeof(int));
    }

    #pragma omp parallel for schedule(static, 1)
    for (int i = 0; i < nb_threads; i++)
    {
	int core_os = sched_getcpu();
	if (topo == NULL)
	{
	    thread_binding[i] = core_os;
	}
	else
	{
	    /* sched_getcpu() returns the OS index of the PU (hyperthread)
	     * where the thread is running. Since we execute only one thread
	     * per core, we want the logical index of the core, not of the PU. */
	    hwloc_obj_t pu_obj = hwloc_get_pu_obj_by_os_index(topo, core_os);
	    assert(pu_obj->type == HWLOC_OBJ_PU);

	    hwloc_obj_t core_obj = pu_obj->parent;
	    assert(core_obj-> type == HWLOC_OBJ_CORE);

	    thread_binding[i] = core_obj->logical_index;
	}
    }

    // Check two threads are not bound on the same core:
    for (int i = 0; i < nb_threads-1; i++)
    {
	for (int j = i+1; j < nb_threads; j++)
	{
	    if (thread_binding[i] == thread_binding[j])
	    {
		fprintf(stderr, "Warning: OpenMP threads %d and %d are bound on the same core %s#%d.\n", i, j, (topo == NULL) ? "P" : "L", thread_binding[i]);
	    }
	}
    }

    if (_thread_binding == NULL)
    {
	free(thread_binding);
    }
}


void get_worker_binding(hwloc_topology_t topo, char* binding_str[64])
{
    #pragma omp parallel
    {
	int core_os = sched_getcpu();
	int logical_core = hwloc_get_pu_obj_by_os_index(topo, core_os)->logical_index;
	binding_str[logical_core] = "OpenMP worker";
    }
}


void bind_thread(struct machine_s machine, int thread_pu, char* name)
{
    hwloc_obj_t pu = hwloc_get_obj_by_depth(machine.topology, machine.topo_pu_depth, thread_pu);
    hwloc_cpuset_t cpuset = hwloc_bitmap_dup(pu->cpuset);
    hwloc_bitmap_singlify(cpuset);
    hwloc_set_cpubind(machine.topology, cpuset, HWLOC_CPUBIND_THREAD);
    hwloc_bitmap_free(cpuset);

    pthread_setname_np(pthread_self(), name);
}


void runtime_shutdown(hwloc_topology_t topo, int comm_enabled)
{
    hwloc_topology_destroy(topo);

    if (comm_enabled)
    {
	MPI_Finalize();
    }
}


void ping_pong_init(int _rank, enum pingpong_type_e _pingpong_type, int _buffer_size)
{
    buffer_size = _buffer_size;
    rank = _rank;
    pingpong_type = _pingpong_type;

    for (int i = 0; i < COMM_BUFFER_POOL_SIZE; i++)
    {
	if ((pingpong_type == PING_PONG) || (pingpong_type == PING && rank == 0) || (pingpong_type == PONG && rank == 1))
	{
	    send_buffers[i] = comm_malloc(buffer_size * sizeof(float));
	    if (send_buffers[i] == NULL)
	    {
		abort();
	    }

	    for (int j = 0; j < buffer_size; j++)
	    {
		send_buffers[i][j] = 3.14;
	    }
	}

	if ((pingpong_type == PING_PONG) || (pingpong_type == PING && rank == 1) || (pingpong_type == PONG && rank == 0))
	{
	    recv_buffers[i] = comm_malloc(buffer_size * sizeof(float));
	    if (recv_buffers[i] == NULL)
	    {
		abort();
	    }

	    memset(recv_buffers[i], 0, buffer_size * sizeof(float));
	}
    }
}


void ping_pong_free()
{
    for (int i = 0; i < COMM_BUFFER_POOL_SIZE; i++)
    {
	if ((pingpong_type == PING_PONG) || (pingpong_type == PING && rank == 0) || (pingpong_type == PONG && rank == 1))
	{
	    comm_free(send_buffers[i], buffer_size * sizeof(float));
	}

	if ((pingpong_type == PING_PONG) || (pingpong_type == PING && rank == 1) || (pingpong_type == PONG && rank == 0))
	{
	    comm_free(recv_buffers[i], buffer_size * sizeof(float));
	}
    }
}


double ping_pong(int comm_duplex)
{
    puk_tick_t start_time, end_time;
    double duration = 0;
    MPI_Request send_req, recv_req;
    int other_rank = (rank == 1) ? 0 : 1;
    float* send_buffer = send_buffers[current_comm_buffer];
    float* recv_buffer = recv_buffers[current_comm_buffer];

    if (rank == 0)
    {
	PUK_GET_TICK(start_time);
	if (comm_duplex)
	{
	    MPI_Isend(send_buffer, buffer_size, MPI_FLOAT, other_rank, 1, MPI_COMM_WORLD, &send_req);
	    MPI_Irecv(recv_buffer, buffer_size, MPI_FLOAT, other_rank, 2, MPI_COMM_WORLD, &recv_req);
	    MPI_Wait(&send_req, MPI_STATUS_IGNORE);
	    MPI_Wait(&recv_req, MPI_STATUS_IGNORE);
	}
	else
	{
	    if (pingpong_type == PING_PONG || pingpong_type == PING)
	    {
		MPI_Send(send_buffer, buffer_size, MPI_FLOAT, other_rank, 1, MPI_COMM_WORLD);
	    }
	    if (pingpong_type == PING_PONG || pingpong_type == PONG)
	    {
		MPI_Recv(recv_buffer, buffer_size, MPI_FLOAT, other_rank, 2, MPI_COMM_WORLD, NULL);
	    }
	}
	PUK_GET_TICK(end_time);

	duration = PUK_TIMING_DELAY(start_time, end_time);
	if (pingpong_type == PING_PONG)
	{
	    duration /= 2;
	}
    }
    else
    {
	if (comm_duplex)
	{
	    MPI_Irecv(recv_buffer, buffer_size, MPI_FLOAT, other_rank, 1, MPI_COMM_WORLD, &recv_req);
	    MPI_Isend(send_buffer, buffer_size, MPI_FLOAT, other_rank, 2, MPI_COMM_WORLD, &send_req);
	    MPI_Wait(&recv_req, MPI_STATUS_IGNORE);
	    MPI_Wait(&send_req, MPI_STATUS_IGNORE);
	}
	else
	{
	    if (pingpong_type == PING_PONG || pingpong_type == PING)
	    {
		MPI_Recv(recv_buffer, buffer_size, MPI_FLOAT, other_rank, 1, MPI_COMM_WORLD, NULL);
	    }
	    if (pingpong_type == PING_PONG || pingpong_type == PONG)
	    {
		MPI_Send(send_buffer, buffer_size, MPI_FLOAT, other_rank, 2, MPI_COMM_WORLD);
	    }
	}
    }

    if (pingpong_type == PING_PONG || (pingpong_type == PING && rank == 1) || (pingpong_type == PONG && rank == 0))
    {
	assert(fabs(recv_buffer[0] - 3.14) < EPSILON);
	assert(fabs(recv_buffer[buffer_size-1] - 3.14) < EPSILON);
    }

    current_comm_buffer = (current_comm_buffer + 1) % COMM_BUFFER_POOL_SIZE;

    return duration;
}
