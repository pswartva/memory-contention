#ifndef __CURSOR_WEAK_H
#define __CURSOR_WEAK_H

#include "helper.h"

int cursor_weak_run(int nb_runs, enum bench_type bench_type);
struct computing_functions cursor_weak_get_functions();

#endif // __CURSOR_WEAK_H
