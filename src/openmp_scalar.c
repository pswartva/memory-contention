#include <float.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <omp.h>
#if defined(__x86_64__)
#include <x86intrin.h>
#endif

#include "timing.h"
#include "scalar.h"

#define ARRAY_SIZE 128

// array to be accessed with bench_type enum to know if we already launched this bench
// to prevent erasing old data bench
static char bench_done[] = {0, 0, 0};

static double scalar_perfs_warmup[] = {FLT_MAX, 0, 0};
static double scalar_perfs_no_comm[] = {FLT_MAX, 0, 0};
static double scalar_perfs_comm[] = {FLT_MAX, 0, 0};

static volatile int* comm_bench_ended = NULL;


void scalar_set_comm_bench_ended_ptr(volatile int* _comm_bench_ended)
{
    comm_bench_ended = _comm_bench_ended;
}

static float __attribute__((optimize("O0"))) scalar_product(float* a, float* b)
{
#if defined(__AVX__)
    /* The instruction zeros the bits in position 128 and higher of all YMM
     * registers. The lower 128-bits of the registers (the corresponding XMM
     * registers) are unmodified.
     * This instruction is recommended when transitioning between AVX and
     * legacy SSE code - it will eliminate performance penalties caused by
     * false dependencies.
     * (https://www.felixcloutier.com/x86/vzeroupper)
     */
    __builtin_ia32_vzeroupper();
#endif

    float sum = 0.0;

    for (int j = 0; j < 200000; j++)
    {
	sum = 0.0;
	for (int i = 0; i < ARRAY_SIZE; i++)
	{
	    sum += (a[i]* 3.0) * (b[i] * 0.3);
	}
    }

    return sum;
}

static void __attribute__((optimize("O0"))) dummy(void* var)
{
    (void) var;
}

static double scalar_run_kernel()
{
    puk_tick_t start_time, end_time;

    PUK_GET_TICK(start_time);
    #pragma omp parallel
    {
	float* a = comp_malloc(ARRAY_SIZE * sizeof(float));
	float* b = comp_malloc(ARRAY_SIZE * sizeof(float));

	for (int i = 0; i < ARRAY_SIZE; i++)
	{
	    a[i] = i;
	    b[i] = i;
	}

	float result = scalar_product(a, b);
	dummy((void*) &result);

	comp_free(a, ARRAY_SIZE * sizeof(float));
	comp_free(b, ARRAY_SIZE * sizeof(float));
    }
    PUK_GET_TICK(end_time);

    return PUK_TIMING_DELAY(start_time, end_time);
}

int scalar_run(int nb_runs, enum bench_type bench_type)
{
    if (bench_type != WARMUP && bench_done[bench_type])
    {
	printf("Warning: this bench was already done.\n");
    }

    double avgtime = 0, maxtime = 0, mintime = FLT_MAX;
    int real_nb_runs = nb_runs;
    double duration;

    for (int k = 0; k < nb_runs; k++)
    {
	duration = scalar_run_kernel();

	mintime = MIN(mintime, duration);
	avgtime += duration;
	maxtime = MAX(maxtime, duration);
    }

    if (bench_type == WITH_COMM && comm_bench_ended != NULL)
    {
	/* Keep computing while we need more pingpongs: */
	while (!*comm_bench_ended)
	{
	    duration = scalar_run_kernel();

	    mintime = MIN(mintime, duration);
	    avgtime += duration;
	    maxtime = MAX(maxtime, duration);

	    real_nb_runs++;
	}
    }

    if (bench_done[bench_type])
    {
	return 0;
    }

    if (bench_type == WARMUP)
    {
	scalar_perfs_warmup[0] = mintime;
	scalar_perfs_warmup[1] = avgtime / (double) (real_nb_runs);
	scalar_perfs_warmup[2] = maxtime;
    }
    else if (bench_type == WITH_COMM)
    {
	scalar_perfs_comm[0] = mintime;
	scalar_perfs_comm[1] = avgtime / (double) (real_nb_runs);
	scalar_perfs_comm[2] = maxtime;
    }
    else if (bench_type == WITHOUT_COMM)
    {
	scalar_perfs_no_comm[0] = mintime;
	scalar_perfs_no_comm[1] = avgtime / (double) (real_nb_runs);
	scalar_perfs_no_comm[2] = maxtime;
    }
    else
    {
	abort();
    }

    bench_done[bench_type] = 1;

    return 0;
}

void scalar_print_results()
{
    printf("# scalar results: Time ms (min, avg, max)\n");
    if (bench_done[WARMUP])
    {
	printf("# warmup                 "COMP_TIME_FORMAT"\t"COMP_TIME_FORMAT"\t"COMP_TIME_FORMAT"\n",
		scalar_perfs_warmup[0] / 1000.0f, scalar_perfs_warmup[1] / 1000.0f, scalar_perfs_warmup[2] / 1000.0f);
    }
    if (bench_done[WITH_COMM])
    {
	printf("# with communications    "COMP_TIME_FORMAT"\t"COMP_TIME_FORMAT"\t"COMP_TIME_FORMAT"\n",
		scalar_perfs_comm[0] / 1000.0f, scalar_perfs_comm[1] / 1000.0f, scalar_perfs_comm[2] / 1000.0f);
    }
    if (bench_done[WITHOUT_COMM])
    {
	printf("# without communications "COMP_TIME_FORMAT"\t"COMP_TIME_FORMAT"\t"COMP_TIME_FORMAT"\n",
		scalar_perfs_no_comm[0] / 1000.0f, scalar_perfs_no_comm[1] / 1000.0f, scalar_perfs_no_comm[2] / 1000.0f);
    }
}


int scalar_get_nb_runs(enum comm_bench_type comm_bench_type)
{
    return 10;
}

struct computing_functions scalar_get_functions()
{
    struct computing_functions s = {
	.init = NULL,
	.run = &scalar_run,
	.print_results = &scalar_print_results,
	.print_params = NULL,
	.release = NULL,
	.parse_arg = NULL,
	.man = NULL,
	.get_nb_runs = &scalar_get_nb_runs,
	.set_comm_bench_ended_ptr = &scalar_set_comm_bench_ended_ptr,
	.name = "scalar"
    };

    return s;
}
