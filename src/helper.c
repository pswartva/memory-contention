#include <stdlib.h>
#include <stdio.h>

#include "helper.h"


int comp_double(const void* _a, const void* _b)
{
    const double* a = _a;
    const double* b = _b;

    if (*a < *b)
	return -1;
    else if (*a > *b)
	return 1;
    else
	return 0;
}

void hwloc_display_binding()
{
    system("hwloc-ps -t -c -a");
    printf("===\n");
    fflush(stdout);
    system("lstopo --ps --of ascii --no-io");
}

void fill_machine(struct machine_s* machine)
{
    // topo_pu_depth:
    machine->topo_pu_depth = hwloc_get_type_depth(machine->topology, HWLOC_OBJ_CORE);
    assert(machine->topo_pu_depth != HWLOC_TYPE_DEPTH_UNKNOWN);

    // nb_pus:
    machine->nb_pus = hwloc_get_nbobjs_by_depth(machine->topology, machine->topo_pu_depth);

    // l3_size:
#if HWLOC_API_VERSION >= 0x00020000
    hwloc_obj_t l3_obj = hwloc_get_obj_by_type(machine->topology, HWLOC_OBJ_L3CACHE, 0);
#else
    int cache_depth = hwloc_get_cache_type_depth(machine->topology, 3, HWLOC_OBJ_CACHE_UNIFIED);
    assert(cache_depth > 0);
    hwloc_obj_t l3_obj = hwloc_get_obj_by_depth(machine->topology, cache_depth, 0);
#endif
    machine->l3_size = l3_obj->attr->cache.size;

    // nb_workers:
    machine->nb_workers = get_worker_count();

    // nb_numa:
    machine->nb_numa = hwloc_get_nbobjs_by_type(machine->topology, HWLOC_OBJ_NUMANODE);

    // nb_sockets:
    machine->nb_sockets = hwloc_get_nbobjs_by_type(machine->topology, HWLOC_OBJ_PACKAGE);
}
