/* This program is a minimal version of
 *   bench_openmp --compute_bench=memset --nt
 * to see the difference between measuring the bandwidth or the throughput.
 *
 * The output has to be converted with plot/basic_openmp_memset2paje.py and can
 * then be visualized with ViTE.
 **/
#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include <x86intrin.h>

#include "malloc.h"
#include "cli.h"

#define MEMSET_TYPE int
#define NB_ITER 3
#define THROUGHPUT_DURATION 1500000 // 1.5 sec
#define NB_MAX_EVENTS 100
#define COMM_BANDWIDTH_BUFFER_SIZE 16777216 // 64 MB is enough to see impact on network bandwidth


struct event_s {
    puk_tick_t time_start;
    puk_tick_t time_end;
};


int main(int argc, char* argv[])
{
    int measure_throughput = 0;
    int nb_threads = get_nb_openmp_threads();
    const int array_size = COMM_BANDWIDTH_BUFFER_SIZE;
    struct machine_s machine;
    struct params_s params;
    MEMSET_TYPE scalar = 3;
    MEMSET_TYPE **a;
    puk_tick_t time_origin;

    init_params();
    disable_all_params();
    enable_param(BIND_MEMORY_COMP);
    parse_args(argc, argv, &params);

    hwloc_topology_init(&machine.topology);
    hwloc_topology_load(machine.topology);

    fill_machine(&machine);
    init_malloc(&machine, params.memory_comp_numa_nodes, params.memory_comp_numa_nodes_nb, -1);

    for (int i = 1; i < argc; i++)
    {
	if (!strcmp(argv[i], "--throughput"))
	{
	    measure_throughput = 1;
	    break;
	}
    }

    printf("# Nb workers: %d\n", nb_threads);
    printf("# Will measure %s\n", measure_throughput ? "throughput" : "bandwidth");


    int* nb_events_per_thread = malloc(nb_threads * sizeof(int));
    struct event_s** events_per_thread = malloc(nb_threads * sizeof(struct event_s*));
    a = malloc(nb_threads*sizeof(MEMSET_TYPE*));

    #pragma omp parallel for
    for (int i = 0; i < nb_threads; i++)
    {
	nb_events_per_thread[i] = 0;
	events_per_thread[i] = malloc(NB_MAX_EVENTS * sizeof(struct event_s));

	a[i] = data_malloc(
	    array_size*sizeof(MEMSET_TYPE),
	    params.memory_comp_numa_nodes_nb == 0 ? -1 : get_numa_node_comp_for_id(i % params.memory_comp_numa_nodes_nb));
	for (int j = 0; j < array_size; j++)
	{
	    a[i][j] = 1;
	}
    }


    unsigned* nb_iter_per_thread = malloc(nb_threads * sizeof(unsigned));

    PUK_GET_TICK(time_origin);
    if (measure_throughput)
    {
	#pragma omp parallel for
	for (int i = 0; i < nb_threads; i++)
	{
	    nb_iter_per_thread[i] = 0;
	    puk_tick_t start_iter_time, end_iter_time;
	    do
	    {
		PUK_GET_TICK(start_iter_time);
		int value = scalar * (nb_iter_per_thread[i]+1);
		for (int k = 0; k < array_size; k++)
		{
		    _mm_stream_si32(a[i]+k, value);
		}
		PUK_GET_TICK(end_iter_time);
		nb_iter_per_thread[i]++;
		
		events_per_thread[i][nb_events_per_thread[i]].time_start = start_iter_time;
		events_per_thread[i][nb_events_per_thread[i]].time_end = end_iter_time;
		nb_events_per_thread[i]++;
	    } while (PUK_TIMING_DELAY(time_origin, end_iter_time) < THROUGHPUT_DURATION);
	}
    }
    else
    {
	#pragma omp parallel for
	for (int i = 0; i < nb_threads; i++)
	{
	    for (int j = 1; j <= NB_ITER; j++)
	    {
		puk_tick_t start_iter_time, end_iter_time;
		int value = scalar * j;
		PUK_GET_TICK(start_iter_time);
		for (int k = 0; k < array_size; k++)
		{
		    _mm_stream_si32(a[i]+k, value);
		}
		PUK_GET_TICK(end_iter_time);

		events_per_thread[i][nb_events_per_thread[i]].time_start = start_iter_time;
		events_per_thread[i][nb_events_per_thread[i]].time_end = end_iter_time;
		nb_events_per_thread[i]++;
	    }
	}
    }

    /* Print trace: */
    int* thread_binding = malloc(nb_threads * sizeof(int));
    memset(thread_binding, 0, nb_threads * sizeof(int));
    get_worker_binding_ids(machine.topology, nb_threads, thread_binding);

    printf("Thread\tCore\tTime start\tTime end\n");
    for (int i = 0; i < nb_threads; i++)
    {
	for (int j = 0; j < nb_events_per_thread[i]; j++)
	{
	    printf("%2d\t%2d\t% 10.1f\t% 10.1f\n",
		    i, thread_binding[i],
		    PUK_TIMING_DELAY(time_origin, events_per_thread[i][j].time_start),
		    PUK_TIMING_DELAY(time_origin, events_per_thread[i][j].time_end));
	}
    }
    free(thread_binding);

    /* Release everything: */
    hwloc_topology_destroy(machine.topology);

    for (int i = 0; i < nb_threads; i++)
    {
	free(events_per_thread[i]);
	data_free(
	    a[i],
	    array_size*sizeof(MEMSET_TYPE),
	    params.memory_comp_numa_nodes_nb == 0 ? -1 : get_numa_node_comp_for_id(i % params.memory_comp_numa_nodes_nb));
    }
    free(a);
    free(nb_events_per_thread);
    free(events_per_thread);

    return EXIT_SUCCESS;
}
