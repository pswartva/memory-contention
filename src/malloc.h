#ifndef __MALLOC_H
#define __MALLOC_H

#include <hwloc.h>
#include "helper.h"

struct machine_s;
void init_malloc(struct machine_s* machine, int* _numa_nodes_comp, unsigned _nb_numa_nodes_comp, int _memory_comm_numa_node);
void* comm_malloc(size_t size);
void* comp_malloc(size_t size);
void* data_malloc(size_t size, int numa_node);
int get_numa_node_comp_for_id(int id);
void comm_free(void* ptr, size_t size);
void comp_free(void* ptr, size_t size);
void data_free(void* ptr, size_t size, int numa_node);

#endif /* __MALLOC_H */
