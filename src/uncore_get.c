#include <stdio.h>
#include <stdlib.h>
#include <likwid.h>

int main()
{
	topology_init();
	CpuTopology_t topo = get_cpuTopology();

	printf("Uncore minimum frequency: %lu MHz\n", freq_getUncoreFreqMin(0));
	printf("Uncore maximum frequency: %lu MHz\n", freq_getUncoreFreqMax(0));

	for (int s = 0; s < topo->numSockets; s++)
	{
		printf("Uncore frequency of socket #%d: %lu MHz\n", s, freq_getUncoreFreqCur(s));
	}

	topology_finalize();
}
