#ifndef __FREQ_METER_H
#define __FREQ_METER_H

#include <hwloc.h>

void freq_meter_init(int _nb_pus, hwloc_topology_t _topology);
void freq_meter_release();
void freq_meter_event(const char* str);
void freq_meter_set_bind_str(char* bind_str[64]);

#endif /* __FREQ_METER_H */
