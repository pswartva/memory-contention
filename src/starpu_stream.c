#include <float.h>
#include <stdlib.h>
#include <stdio.h>
#include <starpu.h>
#include <hwloc.h>

#include "stream.h"
#include "helper_stream.h"


extern struct machine_s machine;
static unsigned nb_workers;

static ARRAY_TYPE *a, *b, *c;
static starpu_data_handle_t handle_a, handle_b, handle_c;

/* We provide a default value, but the value will be set according to the
 * number of workers and the size of the L3 cache */
static unsigned stream_array_size = 235929600; // * 8 Bytes (double) =~ 1800 MB


static void init_task(void *buffers[], void *cl_arg)
{
    unsigned n = STARPU_VECTOR_GET_NX(buffers[0]);
    ARRAY_TYPE* a = (ARRAY_TYPE*) STARPU_VECTOR_GET_PTR(buffers[0]);
    ARRAY_TYPE* b = (ARRAY_TYPE*) STARPU_VECTOR_GET_PTR(buffers[1]);
    ARRAY_TYPE* c = (ARRAY_TYPE*) STARPU_VECTOR_GET_PTR(buffers[2]);

    for (unsigned i = 0; i < n; i++)
    {
	a[i] = 1.0;
	b[i] = 2.0;
	c[i] = 0.0;
    }
}

static void copy_task(void *buffers[], void *cl_arg)
{
    unsigned n = STARPU_VECTOR_GET_NX(buffers[0]);
    ARRAY_TYPE* a = (ARRAY_TYPE*) STARPU_VECTOR_GET_PTR(buffers[0]);
    ARRAY_TYPE* c = (ARRAY_TYPE*) STARPU_VECTOR_GET_PTR(buffers[1]);

    for (unsigned i = 0; i < n; i++)
    {
	c[i] = a[i];
    }
}

static void scale_task(void *buffers[], void *cl_arg)
{
    ARRAY_TYPE scalar = 3.0;
    unsigned n = STARPU_VECTOR_GET_NX(buffers[0]);
    ARRAY_TYPE* b = (ARRAY_TYPE*) STARPU_VECTOR_GET_PTR(buffers[0]);
    ARRAY_TYPE* c = (ARRAY_TYPE*) STARPU_VECTOR_GET_PTR(buffers[1]);

    for (unsigned i = 0; i < n; i++)
    {
	b[i] = scalar*c[i];
    }
}

static void add_task(void *buffers[], void *cl_arg)
{
    unsigned n = STARPU_VECTOR_GET_NX(buffers[0]);
    ARRAY_TYPE* a = (ARRAY_TYPE*) STARPU_VECTOR_GET_PTR(buffers[0]);
    ARRAY_TYPE* b = (ARRAY_TYPE*) STARPU_VECTOR_GET_PTR(buffers[1]);
    ARRAY_TYPE* c = (ARRAY_TYPE*) STARPU_VECTOR_GET_PTR(buffers[2]);

    for (unsigned i = 0; i < n; i++)
    {
	c[i] = a[i] + b[i];
    }
}

static void triad_task(void *buffers[], void *cl_arg)
{
    ARRAY_TYPE scalar = 3.0;
    unsigned n = STARPU_VECTOR_GET_NX(buffers[0]);
    ARRAY_TYPE* a = (ARRAY_TYPE*) STARPU_VECTOR_GET_PTR(buffers[0]);
    ARRAY_TYPE* b = (ARRAY_TYPE*) STARPU_VECTOR_GET_PTR(buffers[1]);
    ARRAY_TYPE* c = (ARRAY_TYPE*) STARPU_VECTOR_GET_PTR(buffers[2]);

    for (unsigned i = 0; i < n; i++)
    {
	a[i] = b[i] + scalar*c[i];
    }
}

static struct starpu_codelet init_cl =
{
    .cpu_funcs = {init_task},
    .cpu_funcs_name = {"init_task"},
    .nbuffers = 3,
    .modes = {STARPU_W, STARPU_W, STARPU_W},
    .name = "init_arrays",
    .color = 0x800000 // maroon
};

static struct starpu_codelet copy_cl =
{
    .cpu_funcs = {copy_task},
    .cpu_funcs_name = {"copy_task"},
    .nbuffers = 2,
    .modes = {STARPU_R, STARPU_W},
    .name = "copy",
    .color = 0xffff00 // yellow
};

static struct starpu_codelet scale_cl =
{
    .cpu_funcs = {scale_task},
    .cpu_funcs_name = {"scale_task"},
    .nbuffers = 2,
    .modes = {STARPU_W, STARPU_R},
    .name = "scale",
    .color = 0x808000 // olive
};

static struct starpu_codelet add_cl =
{
    .cpu_funcs = {add_task},
    .cpu_funcs_name = {"add_task"},
    .nbuffers = 3,
    .modes = {STARPU_R, STARPU_R, STARPU_W},
    .name = "add",
    .color = 0x008000 // green
};

static struct starpu_codelet triad_cl =
{
    .cpu_funcs = {triad_task},
    .cpu_funcs_name = {"triad_task"},
    .nbuffers = 3,
    .modes = {STARPU_W, STARPU_R, STARPU_W},
    .name = "triad",
    .color = 0x00ffff // cyan
};

int stream_init()
{
    int ret = 0;
    nb_workers = starpu_worker_get_count();

    stream_array_size = machine.l3_size * 2 * nb_workers;

    starpu_malloc((void**) &a, stream_array_size * sizeof(ARRAY_TYPE));
    starpu_malloc((void**) &b, stream_array_size * sizeof(ARRAY_TYPE));
    starpu_malloc((void**) &c, stream_array_size * sizeof(ARRAY_TYPE));

    starpu_vector_data_register(&handle_a, STARPU_MAIN_RAM, (uintptr_t) a, stream_array_size, sizeof(ARRAY_TYPE));
    starpu_vector_data_register(&handle_b, STARPU_MAIN_RAM, (uintptr_t) b, stream_array_size, sizeof(ARRAY_TYPE));
    starpu_vector_data_register(&handle_c, STARPU_MAIN_RAM, (uintptr_t) c, stream_array_size, sizeof(ARRAY_TYPE));

    struct starpu_data_filter f = {
	.filter_func = starpu_vector_filter_block,
	.nchildren = nb_workers
    };
    starpu_data_partition(handle_a, &f);
    starpu_data_partition(handle_b, &f);
    starpu_data_partition(handle_c, &f);


    /* Initialize arrays */
    for (int i = 0; i < starpu_data_get_nb_children(handle_a); i++)
    {
	starpu_data_handle_t subhandle_a = starpu_data_get_sub_data(handle_a, 1, i);
	starpu_data_handle_t subhandle_b = starpu_data_get_sub_data(handle_b, 1, i);
	starpu_data_handle_t subhandle_c = starpu_data_get_sub_data(handle_c, 1, i);

	struct starpu_task* task = starpu_task_create();
	task->handles[0] = subhandle_a;
	task->handles[1] = subhandle_b;
	task->handles[2] = subhandle_c;
	task->cl = &init_cl;

	ret = starpu_task_submit(task);
	if (ret != 0)
	{
	    printf("Error on starpu_task_submit()\n");
	    return ret;
	}
    }

    starpu_task_wait_for_all();

    return ret;
}

static int stream_run_kernel(double avgtime[4], double maxtime[4], double mintime[4])
{
    int ret;
    double start_time, end_time;
    double durations[4];

    /* COPY */
    if (bench_to_run[COPY])
    {
	starpu_pause();

	for (int i = 0; i < starpu_data_get_nb_children(handle_a); i++)
	{
	    starpu_data_handle_t subhandle_a = starpu_data_get_sub_data(handle_a, 1, i);
	    starpu_data_handle_t subhandle_c = starpu_data_get_sub_data(handle_c, 1, i);

	    struct starpu_task* task = starpu_task_create();
	    task->handles[0] = subhandle_a;
	    task->handles[1] = subhandle_c;
	    task->cl = &copy_cl;

	    ret = starpu_task_submit(task);
	    if (ret != 0)
	    {
		printf("Error on starpu_task_submit()\n");
		return ret;
	    }
	}

	start_time = starpu_timing_now();
	starpu_resume();
	starpu_task_wait_for_all();
	end_time = starpu_timing_now();
	durations[COPY] = (end_time - start_time) / 1000000.0f;
    }


    /* SCALE */
    if (bench_to_run[SCALE])
    {
	starpu_pause();

	for (int i = 0; i < starpu_data_get_nb_children(handle_a); i++)
	{
	    starpu_data_handle_t subhandle_b = starpu_data_get_sub_data(handle_b, 1, i);
	    starpu_data_handle_t subhandle_c = starpu_data_get_sub_data(handle_c, 1, i);

	    struct starpu_task* task = starpu_task_create();
	    task->handles[0] = subhandle_b;
	    task->handles[1] = subhandle_c;
	    task->cl = &scale_cl;

	    ret = starpu_task_submit(task);
	    if (ret != 0)
	    {
		printf("Error on starpu_task_submit()\n");
		return ret;
	    }
	}

	start_time = starpu_timing_now();
	starpu_resume();
	starpu_task_wait_for_all();
	end_time = starpu_timing_now();
	durations[SCALE] = (end_time - start_time) / 1000000.0f;
    }


    /* ADD */
    if (bench_to_run[ADD])
    {
	starpu_pause();

	for (int i = 0; i < starpu_data_get_nb_children(handle_a); i++)
	{
	    starpu_data_handle_t subhandle_a = starpu_data_get_sub_data(handle_a, 1, i);
	    starpu_data_handle_t subhandle_b = starpu_data_get_sub_data(handle_b, 1, i);
	    starpu_data_handle_t subhandle_c = starpu_data_get_sub_data(handle_c, 1, i);

	    struct starpu_task* task = starpu_task_create();
	    task->handles[0] = subhandle_a;
	    task->handles[1] = subhandle_b;
	    task->handles[2] = subhandle_c;
	    task->cl = &add_cl;

	    ret = starpu_task_submit(task);
	    if (ret != 0)
	    {
		printf("Error on starpu_task_submit()\n");
		return ret;
	    }
	}

	start_time = starpu_timing_now();
	starpu_resume();
	starpu_task_wait_for_all();
	end_time = starpu_timing_now();
	durations[ADD] = (end_time - start_time) / 1000000.0f;
    }


    /* TRIAD */
    if (bench_to_run[TRIAD])
    {
	starpu_pause();

	for (int i = 0; i < starpu_data_get_nb_children(handle_a); i++)
	{
	    starpu_data_handle_t subhandle_a = starpu_data_get_sub_data(handle_a, 1, i);
	    starpu_data_handle_t subhandle_b = starpu_data_get_sub_data(handle_b, 1, i);
	    starpu_data_handle_t subhandle_c = starpu_data_get_sub_data(handle_c, 1, i);

	    struct starpu_task* task = starpu_task_create();
	    task->handles[0] = subhandle_a;
	    task->handles[1] = subhandle_b;
	    task->handles[2] = subhandle_c;
	    task->cl = &triad_cl;

	    ret = starpu_task_submit(task);
	    if (ret != 0)
	    {
		printf("Error on starpu_task_submit()\n");
		return ret;
	    }
	}

	start_time = starpu_timing_now();
	starpu_resume();
	starpu_task_wait_for_all();
	end_time = starpu_timing_now();
	durations[TRIAD] = (end_time - start_time) / 1000000.0f;
    }

    for (int j = 0; j < 4; j++)
    {
	if (bench_to_run[j])
	{
	    avgtime[j] = avgtime[j] + durations[j];
	    mintime[j] = MIN(mintime[j], durations[j]);
	    maxtime[j] = MAX(maxtime[j], durations[j]);
	}
    }

    return 0;
}

int stream_run(int nb_runs, enum bench_type bench_type)
{
    if (bench_type != WARMUP && stream_bench_done[bench_type])
    {
	printf("Warning: this bench was already done.\n");
    }

    int ret = 0;
    int real_nb_runs = nb_runs;
    double avgtime[4] = {0}, maxtime[4] = {0}, mintime[4] = {FLT_MAX,FLT_MAX,FLT_MAX,FLT_MAX};
    double bytes[4] = {
	2 * sizeof(ARRAY_TYPE) * stream_array_size,
	2 * sizeof(ARRAY_TYPE) * stream_array_size,
	3 * sizeof(ARRAY_TYPE) * stream_array_size,
	3 * sizeof(ARRAY_TYPE) * stream_array_size
    };

    for (int k = 0; k < nb_runs; k++)
    {
	ret = stream_run_kernel(avgtime, maxtime, mintime);
    }

    if (bench_type == WITH_COMM && stream_comm_bench_ended != NULL)
    {
	/* Keep computing while we need more pingpongs: */
	while (!*stream_comm_bench_ended)
	{
	    ret = stream_run_kernel(avgtime, maxtime, mintime);
	    real_nb_runs++;
	}
    }

    if (stream_bench_done[bench_type])
    {
	return 0;
    }

    for (int j = 0; j < 4; j++)
    {
	if (bench_to_run[j])
	{
	    avgtime[j] = avgtime[j]/(double)(real_nb_runs);

	    if (bench_type == WARMUP)
	    {
		stream_perfs_warmup[j][0] = 1.0E-06 * bytes[j] / mintime[j];
		stream_perfs_warmup[j][1] = 1.0E-06 * bytes[j] / avgtime[j];
		stream_perfs_warmup[j][2] = 1.0E-06 * bytes[j] / maxtime[j];
	    }
	    else if (bench_type == WITH_COMM)
	    {
		stream_perfs_comm[j][0] = 1.0E-06 * bytes[j] / mintime[j];
		stream_perfs_comm[j][1] = 1.0E-06 * bytes[j] / avgtime[j];
		stream_perfs_comm[j][2] = 1.0E-06 * bytes[j] / maxtime[j];
	    }
	    else if (bench_type == WITHOUT_COMM)
	    {
		stream_perfs_no_comm[j][0] = 1.0E-06 * bytes[j] / mintime[j];
		stream_perfs_no_comm[j][1] = 1.0E-06 * bytes[j] / avgtime[j];
		stream_perfs_no_comm[j][2] = 1.0E-06 * bytes[j] / maxtime[j];
	    }
	    else
	    {
		abort();
	    }
	}
    }

    stream_bench_done[bench_type] = 1;

    return ret;
}

void stream_release()
{
    starpu_data_unpartition(handle_a, STARPU_MAIN_RAM);
    starpu_data_unpartition(handle_b, STARPU_MAIN_RAM);
    starpu_data_unpartition(handle_c, STARPU_MAIN_RAM);
    starpu_data_unregister(handle_a);
    starpu_data_unregister(handle_b);
    starpu_data_unregister(handle_c);

    starpu_free_noflag(a, stream_array_size * sizeof(ARRAY_TYPE));
    starpu_free_noflag(b, stream_array_size * sizeof(ARRAY_TYPE));
    starpu_free_noflag(c, stream_array_size * sizeof(ARRAY_TYPE));
}

struct computing_functions stream_get_functions()
{
    struct computing_functions s = {
	.init = &stream_init,
	.run = &stream_run,
	.print_results = &stream_print_results,
	.print_params = &stream_print_params,
	.release = &stream_release,
	.parse_arg = &stream_parse_arg,
	.man = &stream_man,
	.get_nb_runs = &stream_get_nb_runs,
	.set_comm_bench_ended_ptr = &stream_set_comm_bench_ended_ptr,
	.name = "stream"
    };

    return s;
}
