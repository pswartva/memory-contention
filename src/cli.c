#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "config.h"
#include "cli.h"

#ifdef CLI_DISABLE_KERNELS
    #define NB_KERNELS 0
#else
#include "stream.h"
#include "prime.h"
#include "cursor.h"

#if WITH_STARPU == 1
    #if HAVE_MKL
        #include "cholesky.h"
        #define NB_KERNELS 4
    #else
        #define NB_KERNELS 3
    #endif
#elif defined(_OPENMP)
    #include "scalar.h"
    #include "stream_weak.h"
    #include "cursor_weak.h"
    #ifdef HAVE_SIMD_AVX
        #include "scalar_avx.h"
        #define NB_KERNELS 8
    #else
        #define NB_KERNELS 7
    #endif
#endif
#endif

#define CHECK_PARAM(param) if (!enabled_params[param]) { printf("Parameter '%s' not available\n", argv[i]); continue; }

static int enabled_params[_LAST_PARAM];
static struct computing_functions computing_kernels[NB_KERNELS];

void init_params()
{
    for (int i = 0; i < _LAST_PARAM; i++)
    {
	enabled_params[i] = 1;
    }
#if WITH_STARPU == 1
    // Not yet supported in the StarPU version
    enabled_params[BIND_MEMORY_COMM] = 0;
    enabled_params[BIND_MEMORY_COMP] = 0;
#endif

#ifndef CLI_DISABLE_KERNELS
    computing_kernels[0] = prime_get_functions();
    computing_kernels[1] = stream_get_functions();
    computing_kernels[2] = cursor_get_functions();
#if WITH_STARPU == 1
    #if HAVE_MKL
    computing_kernels[3] = cholesky_get_functions();
    #endif
#elif defined(_OPENMP)
    computing_kernels[3] = stream_weak_get_functions();
    computing_kernels[4] = scalar_get_functions();
    computing_kernels[5] = memset_get_functions();
    computing_kernels[6] = cursor_weak_get_functions();
    #ifdef HAVE_SIMD_AVX
    computing_kernels[7] = scalar_avx_get_functions();
    #endif
#endif
#endif
}

void disable_param(enum param_e param)
{
    enabled_params[param] = 0;
}

void enable_param(enum param_e param)
{
    enabled_params[param] = 1;
}

void disable_all_params()
{
    for (int i = 0; i < _LAST_PARAM; i++)
    {
	enabled_params[i] = 0;
    }
}



void parse_args(int argc, char* argv[], struct params_s* params)
{
    int i = 0;

    // Set default params:
    memset(params, 0, sizeof(struct params_s));
    params->enable_comm = 1;
    params->display_comm = 1;
    params->do_warmup = 1;
    params->do_alone = DO_COMP_ALONE | DO_COMM_ALONE;
    params->comm_bench_type = LATENCY;
#ifndef CLI_DISABLE_KERNELS
    params->computing = stream_get_functions();
#endif
    params->ping_thread_location = LAST;
    params->memory_comp_numa_nodes_nb = 0;
    params->memory_comm_numa_node = -1;
    params->pingpong_type = PING_PONG;

    for (i = 1; i < argc; i++)
    {
	if (strcmp(argv[i], "--disable_comms") == 0)
	{
	    CHECK_PARAM(DISABLE_COMM);
	    params->enable_comm = 0;
	}
	if (strcmp(argv[i], "--only_ping") == 0)
	{
	    CHECK_PARAM(ONLY_PING);
	    params->pingpong_type = PING;
	}
	if (strcmp(argv[i], "--only_pong") == 0)
	{
	    CHECK_PARAM(ONLY_PONG);
	    params->pingpong_type = PONG;
	}
	else if (strcmp(argv[i], "--ping_thread=first") == 0)
	{
	    CHECK_PARAM(PING_THREAD);
	    params->ping_thread_location = FIRST;
	    assert(params->ping_thread_pu == 0); // ping_thread_pu is already set to 0 with the memset
	}
	else if (strcmp(argv[i], "--ping_thread=after") == 0)
	{
	    CHECK_PARAM(PING_THREAD);
	    params->ping_thread_location = AFTER;
	}
	else if (strcmp(argv[i], "--ping_thread=last") == 0)
	{
	    CHECK_PARAM(PING_THREAD);
	    params->ping_thread_location = LAST;
	}
	else if (strncmp(argv[i], "--ping_thread=", 14) == 0)
	{
	    CHECK_PARAM(PING_THREAD);
	    params->ping_thread_location = CUSTOM;
	    sscanf(argv[i], "--ping_thread=%d", &params->ping_thread_pu);
	}
	else if (strcmp(argv[i], "--bench=bandwidth") == 0)
	{
	    CHECK_PARAM(BENCH);
	    params->comm_bench_type = BANDWIDTH;
	}
	else if (strncmp(argv[i], "--compute_bench=", 16) == 0)
	{
	    CHECK_PARAM(COMPUTE_BENCH);
	    for (int j = 0; j < NB_KERNELS; j++)
	    {
		if (strcmp(argv[i]+16, computing_kernels[j].name) == 0)
		{
		    params->computing = computing_kernels[j];
		    break;
		}
	    }
	}
	else if (strcmp(argv[i], "--hide_comms") == 0)
	{
	    CHECK_PARAM(HIDE_COMMS);
	    params->display_comm = 0;
	}
	else if (strcmp(argv[i], "--duplex") == 0)
	{
	    CHECK_PARAM(DUPLEX);
	    params->comm_duplex = 1;
	}
	else if (strcmp(argv[i], "--debug") == 0)
	{
	    CHECK_PARAM(DEBUG);
	    params->debug = 1;
	}
	else if (strcmp(argv[i], "--no_warmup") == 0)
	{
	    CHECK_PARAM(NO_WARMUP);
	    params->do_warmup = 0;
	}
	else if (strncmp(argv[i], "--no_alone", 10) == 0)
	{
	    CHECK_PARAM(NO_WARMUP);
	    if (strcmp(argv[i], "--no_alone") == 0 || strcmp(argv[i], "--no_alone=all") == 0)
	    {
		params->do_alone = 0;
	    }
	    else if (strcmp(argv[i], "--no_alone=comp") == 0)
	    {
		params->do_alone = DO_COMM_ALONE;
	    }
	    else if (strcmp(argv[i], "--no_alone=comm") == 0)
	    {
		params->do_alone = DO_COMP_ALONE;
	    }
	}
	else if (strcmp(argv[i], "--display_binding") == 0)
	{
	    CHECK_PARAM(DISPLAY_BINDING);
	    params->display_binding = 1;
	}
	else if (strncmp(argv[i], "--pingpong_size=", 16) == 0)
	{
	    CHECK_PARAM(PINGPONG_SIZE);
	    params->pingpong_size = atoi(argv[i] + 16);
	}
	else if (strncmp(argv[i], "--bind_memory_comp=", 19) == 0)
	{
	    CHECK_PARAM(BIND_MEMORY_COMP);

	    const char delim[] = "-,";
	    char* sub = strtok(argv[i]+19, delim);
	    for (; sub != NULL; sub = strtok(NULL, delim))
	    {
		if (params->memory_comp_numa_nodes_nb >= MAX_NUMA_NODES)
		{
		    fprintf(stderr, "Maximum number of NUMA nodes reached (%d)\n", MAX_NUMA_NODES);
		    abort();
		}
		params->memory_comp_numa_nodes[params->memory_comp_numa_nodes_nb++] = atoi(sub);
	    }
	}
	else if (strncmp(argv[i], "--bind_memory_comm=", 19) == 0)
	{
	    CHECK_PARAM(BIND_MEMORY_COMM);
	    params->memory_comm_numa_node = atoi(argv[i] + 19);
	}
	else if (strcmp(argv[i], "--one_computing_rank") == 0)
	{
	    CHECK_PARAM(ONE_COMPUTING_RANK);
	    params->one_computing_rank = 1;
	}
	else if (strcmp(argv[i], "--per_thread") == 0)
	{
	    CHECK_PARAM(PER_THREAD_PERF);
	    params->per_thread_perf = 1;
	}
#if (WITH_LIKWID == 1) || (WITH_DIRECT_CPU_FREQ == 1)
	else if (strcmp(argv[i], "--freq") == 0)
	{
	    CHECK_PARAM(FREQ);
	    params->measure_freq = 1;
	}
#endif
    }

    // From here, computing functions are set according to user's wish.
    // So we can test options related to bench type:
    for (i = 1; i < argc; i++)
    {
        if (!(params->computing.parse_arg != NULL && (params->computing.parse_arg)(argv[i])) && (strcmp(argv[i], "--help") == 0 || strcmp(argv[i], "-h") == 0))
	{
	    params->display_help = 1;
	}
    }

    if (params->pingpong_size == 0)
    {
	params->pingpong_size = (params->comm_bench_type == LATENCY ? COMM_LATENCY_BUFFER_SIZE : COMM_BANDWIDTH_BUFFER_SIZE);
    }
}

void print_params(struct params_s params)
{
    if (enabled_params[DISABLE_COMM])
    {
	printf("# Will %sdo communication\n", params.enable_comm ? "" : "not ");
    }

    if (params.enable_comm)
    {
	if (enabled_params[PING_THREAD])
	{
	    if (params.ping_thread_location == FIRST)
	    {
		printf("# PingPong thread will be bounded on first core\n");
	    }
	    else if (params.ping_thread_location == AFTER)
	    {
		printf("# PingPong thread will be bounded on the core just after those used by workers\n");
	    }
	    else if (params.ping_thread_location == LAST)
	    {
		printf("# PingPong thread will be bounded on last core\n");
	    }
	    else if (params.ping_thread_location == CUSTOM)
	    {
		printf("# PingPong thread will be bounded on a custom core\n");
	    }
	    else
	    {
		abort();
	    }
	}

	if (enabled_params[BENCH])
	{
	    printf("# Will measure communication %s\n", params.comm_bench_type == LATENCY ? "latency" : "bandwidth");
	}

	if (enabled_params[HIDE_COMMS])
	{
	    printf("# Will %s communication results\n", params.display_comm ? "show" : "hide");
	}

	if (enabled_params[DUPLEX])
	{
	    printf("# Will do %s-duplex communications\n", params.comm_duplex ? "full" : "half");
	}

	if (enabled_params[BIND_MEMORY_COMM])
	{
	    if (params.memory_comm_numa_node == -1)
	    {
		printf("# Will not bind memory used for communications\n");
	    }
	    else
	    {
		printf("# Will bind memory used for communications to NUMA node %d\n", params.memory_comm_numa_node);
	    }
	}

	if (enabled_params[ONLY_PING] || enabled_params[ONLY_PONG])
	{
	    switch (params.pingpong_type)
	    {
		case PING_PONG:
		    printf("# Both ranks will do pingpongs\n");
		    break;
		case PING:
		    printf("# Only pings: rank 0 will only send to rank 1\n");
		    break;
		case PONG:
		    printf("# Only pong: rank 0 will only receive from rank 1\n");
		    break;
		default:
		    fprintf(stderr, "Unknown pingong_type\n");
		    abort();
	    }
	}
    }

    if (enabled_params[COMPUTE_BENCH])
    {
	printf("# Computing kernel: %s\n", params.computing.name);
    }

    if (enabled_params[PER_THREAD_PERF])
    {
	printf("# Will %sdisplay per-thread performances\n", params.per_thread_perf ? "" : "not ");
    }

    if (enabled_params[DEBUG])
    {
	printf("# Debug mode %sabled\n", params.debug ? "en" : "dis");
    }

    if (enabled_params[NO_WARMUP])
    {
	printf("# Warmup runs %sabled\n", params.do_warmup ? "en" : "dis");
    }

    if (enabled_params[NO_ALONE])
    {
	printf("# Individual runs of communications %sabled\n", (params.do_alone & DO_COMM_ALONE) ? "en" : "dis");
	printf("# Individual runs of computations %sabled\n", (params.do_alone & DO_COMP_ALONE) ? "en" : "dis");
    }

    if (enabled_params[DISPLAY_BINDING])
    {
	printf("# Will %sdisplay binding\n", params.display_binding ? "" : "not ");
    }

    if (enabled_params[BIND_MEMORY_COMP])
    {
	if (params.memory_comp_numa_nodes_nb == 0)
	{
	    printf("# Will not bind memory used for computations\n");
	}
	else
	{
	    printf("# Will bind memory used for computations to NUMA node%s: ", (params.memory_comp_numa_nodes_nb == 1) ? "" : "s");
	    for (unsigned i = 0; i < params.memory_comp_numa_nodes_nb; i++)
	    {
		printf("%u ", params.memory_comp_numa_nodes[i]);
	    }
	    printf("\n");
	}
    }

    if (enabled_params[ONE_COMPUTING_RANK])
    {
	if (params.one_computing_rank)
	{
	    printf("# Only one rank will do computations\n");
	}
	else
	{
	    printf("# Both ranks will do computations\n");
	}
    }

#if (WITH_LIKWID == 1) || (WITH_DIRECT_CPU_FREQ == 1)
    if (enabled_params[FREQ])
    {
	printf("# Will %smeasure frequencies\n", params.measure_freq ? "" : "not ");
    }
#endif

    if (params.computing.print_params != NULL)
    {
	params.computing.print_params();
    }
}

void print_help(struct params_s params)
{
    if (enabled_params[PING_THREAD])
    {
	printf("--ping_thread={first,after,<core logical number>} to precise where the ping thread will be bounded (last core by default).\n");
    }
    if (enabled_params[BENCH])
    {
	printf("--bench={latency,bandwidth} to precise which communication benchmark to perform.\n");
    }
    if (enabled_params[COMPUTE_BENCH])
    {
	printf("--compute_bench={");
	for (int i = 0; i < NB_KERNELS; i++)
	{
	    printf("%s", computing_kernels[i].name);
	    if (i < (NB_KERNELS-1))
	    {
		printf(",");
	    }
	}
	printf("} to precise which computing benchmark to perform.\n");
    }
    if (enabled_params[DUPLEX])
    {
	printf("--duplex to do full-duplex communications.\n");
    }
    if (enabled_params[DISABLE_COMM])
    {
	printf("--disable_comms to disable communication benchmark.\n");
    }
    if (enabled_params[HIDE_COMMS])
    {
	printf("--hide_comms to hide all communication durations.\n");
    }
    if (enabled_params[DEBUG])
    {
	printf("--debug to print some debug informations.\n");
    }
    if (enabled_params[DISPLAY_BINDING])
    {
	printf("--display_binding to report thread binding.\n");
    }
    if (enabled_params[PINGPONG_SIZE])
    {
	printf("--pingpong_size=<nb floats to send/recv in pingpongs>\n");
    }
    if (enabled_params[NO_WARMUP])
    {
	printf("--no_warmup to disable warmup runs\n");
    }
    if (enabled_params[NO_ALONE])
    {
	printf("--no_alone={all,comm,comp} to disable individual runs\n");
    }
    if (enabled_params[BIND_MEMORY_COMP])
    {
	printf("--bind_memory_comp=<numa node> on which NUMA node(s) bind memory allocated for computations (separate with commas)\n");
    }
    if (enabled_params[BIND_MEMORY_COMM])
    {
	printf("--bind_memory_comm=<numa node> on which NUMA node bind memory allocated for communications\n");
    }
    if (enabled_params[ONE_COMPUTING_RANK])
    {
	printf("--one_computing_rank: only one rank will do computations\n");
    }
    if (enabled_params[ONLY_PING])
    {
	printf("--only_ping: the rank 0 will only send data through the network\n");
    }
    if (enabled_params[ONLY_PONG])
    {
	printf("--only_pong: the rank 0 will only receive data from the network\n");
    }
    if (enabled_params[PER_THREAD_PERF])
    {
	printf("--per_thread: will also display per-thread performance\n");
    }
#if (WITH_LIKWID == 1) || (WITH_DIRECT_CPU_FREQ == 1)
    if (enabled_params[FREQ])
    {
	printf("--freq to measure frequencies.\n");
    }
#endif
    printf("--help -h display this message\n");
    if (params.computing.man != NULL)
    {
	(params.computing.man)();
    }
}

void print_machine(struct machine_s* machine, struct params_s* params)
{
    printf("# Nb PUs: %d\n", machine->nb_pus);
    printf("# Nb workers: %d\n", machine->nb_workers);
    printf("# L3 cache size: %lu MB\n", machine->l3_size / (1024 * 1024));
    printf("# Nb NUMA nodes: %d\n", machine->nb_numa);
    printf("# Nb sockets: %d\n", machine->nb_sockets);

    if (params->enable_comm)
    {
	int ping_thread_os_pu = hwloc_get_obj_by_type(machine->topology, HWLOC_OBJ_CORE, params->ping_thread_pu)->os_index;
	printf("# PingPong thread is bound to PU L#%d / P#%d\n", params->ping_thread_pu, ping_thread_os_pu);
    }
}

void ping_thread_location_to_pu(struct params_s* params, struct machine_s* machine)
{
    if (params->ping_thread_location == AFTER)
    {
	params->ping_thread_pu = machine->nb_workers; // --ping_thread=after
    }
    else if (params->ping_thread_location == LAST)
    {
	params->ping_thread_pu = machine->nb_pus-1; // By default, bind ping thread on last core
    }
    assert(params->ping_thread_pu < machine->nb_pus);
}
