#include <stdlib.h>
#include <stdio.h>

#include "prime.h"

#define NB_RUNS 10


int main(int argc, char* argv[])
{
    int nb_prime = 0;

    for (int i = 0; i < NB_RUNS; i++)
    {
	nb_prime += nb_prime_numbers();
    }

    nb_prime /= NB_RUNS;

    printf("There are %d prime numbers in this interval.\n", nb_prime);

    return EXIT_SUCCESS;
}
