#ifndef __HELPER_STREAM_H
#define __HELPER_STREAM_H

#include "helper.h"

// arrays indexed first by stream_bench_type enum, and then {max, avg, min}:
extern double stream_perfs_warmup[4][3];
extern double stream_perfs_no_comm[4][3];
extern double stream_perfs_comm[4][3];
extern double stream_time_warmup[4][3];
extern double stream_time_no_comm[4][3];
extern double stream_time_comm[4][3];

// array to be accessed with bench_type enum to know if we already launched this bench
// to prevent erasing old data bench
extern char stream_bench_done[3];

// array to be accessed with stream_bench_type enum to know if we launch this bench:
extern char bench_to_run[4];

extern volatile int* stream_comm_bench_ended;


void stream_set_comm_bench_ended_ptr(volatile int* _comm_bench_ended);
int stream_parse_arg(char* arg);
void stream_man();
void stream_print_results();
void stream_print_params();
int stream_get_nb_runs(enum comm_bench_type comm_bench_type);

#endif /* __HELPER_STREAM_H */
