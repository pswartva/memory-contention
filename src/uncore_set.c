#include <stdio.h>
#include <stdlib.h>
#include <likwid.h>

int main(int argc, char* argv[])
{
	if (argc != 2 && argc != 3)
	{
		fprintf(stderr, "Usage: %s <(minimum) uncore frequency in MHz> [<maximum uncore frequency in MHz>]\n", argv[0]);
		return EXIT_FAILURE;
	}

	uint64_t min_uncore_freq = atol(argv[1]);
	uint64_t max_uncore_freq = min_uncore_freq;
	
	if (argc == 3)
	{
		max_uncore_freq = atol(argv[2]);
	}

	topology_init();
	CpuTopology_t topo = get_cpuTopology();

	int error = 0;

	for (int s = 0; s < topo->numSockets; s++)
	{
		if (freq_setUncoreFreqMin(s, min_uncore_freq) != 0)
		{
			fprintf(stderr, "Error while setting minimum uncore frequency\n");
			error = 1;
		}
		if (freq_setUncoreFreqMax(s, max_uncore_freq) != 0)
		{
			fprintf(stderr, "Error while setting maximum uncore frequency\n");
			error = 1;
		}
	}

	if (!error)
	{
		printf("OK\n");
	}

	topology_finalize();

	return EXIT_SUCCESS;
}
