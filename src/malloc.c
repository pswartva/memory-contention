#include "malloc.h"

#if HWLOC_API_VERSION < 0x00010b00
#define HWLOC_OBJ_NUMANODE HWLOC_OBJ_NODE
#endif

static hwloc_topology_t topology;
static int memory_comm_numa_node;

/* Array describing on which NUMA nodes memory for computations can be allocated: */
static int* numa_nodes_comp;
/* Number of NUMA nodes on which memory for computations can be allocated: */
static unsigned nb_numa_nodes_comp = 0;

void init_malloc(struct machine_s* machine, int* _numa_nodes_comp, unsigned _nb_numa_nodes_comp, int _memory_comm_numa_node)
{
    topology = machine->topology;
    memory_comm_numa_node = _memory_comm_numa_node;
    numa_nodes_comp = _numa_nodes_comp;
    nb_numa_nodes_comp = _nb_numa_nodes_comp;

    // Just check the given ids are valid:
    for (unsigned i = 0; i < nb_numa_nodes_comp; i++)
    {
	if (numa_nodes_comp[i] >= machine->nb_numa)
	{
	    fprintf(stderr, "This logical id of a NUMA node (%d) is superior to the number of NUMA nodes (%d)!\n", numa_nodes_comp[i], machine->nb_numa);
	    abort();
	}
    }
}

int get_numa_node_comp_for_id(int id)
{
    if (id >= nb_numa_nodes_comp)
    {
	fprintf(stderr, "This id (%d) is superior to the number of provided NUMA nodes in the options (%d) !\n", id, nb_numa_nodes_comp);
	abort();
    }

    return numa_nodes_comp[id];
}

void* data_malloc(size_t size, int numa_node)
{
    if (numa_node == -1)
    {
	return malloc(size);
    }
    else
    {
	hwloc_obj_t obj = hwloc_get_obj_by_type(topology, HWLOC_OBJ_NUMANODE, numa_node);
	if (obj == NULL)
	{
	    fprintf(stderr, "Cannot find NUMA node %d\n", numa_node);
	    abort();
	}

	/* HWLOC_MEMBIND_BIND: Allocate memory on the specified nodes.
	 * HWLOC_MEMBIND_BYNODESET: Consider the bitmap argument as a nodeset.
	 * HWLOC_MEMBIND_NOCPUBIND: Avoid any effect on CPU binding. */
#if HWLOC_API_VERSION >= 0x00020000
	return hwloc_alloc_membind(topology, size, obj->nodeset, HWLOC_MEMBIND_BIND, HWLOC_MEMBIND_BYNODESET | HWLOC_MEMBIND_NOCPUBIND);
#else
	return hwloc_alloc_membind_nodeset(topology, size, obj->nodeset, HWLOC_MEMBIND_BIND, HWLOC_MEMBIND_NOCPUBIND);
#endif
    }
}

void* comm_malloc(size_t size)
{
    return data_malloc(size, memory_comm_numa_node);
}

void* comp_malloc(size_t size)
{
    return data_malloc(size, (nb_numa_nodes_comp == 0) ? -1 : numa_nodes_comp[0]);
}

void data_free(void* ptr, size_t size, int numa_node)
{
    if (numa_node == -1)
    {
	free(ptr);
    }
    else
    {
	hwloc_free(topology, ptr, size);
    }
}

void comm_free(void* ptr, size_t size)
{
    data_free(ptr, size, memory_comm_numa_node);
}

void comp_free(void* ptr, size_t size)
{
    return data_free(ptr, size, (nb_numa_nodes_comp == 0) ? -1 : numa_nodes_comp[0]);
}
