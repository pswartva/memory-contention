#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <starpu.h>
#include <starpu_mpi.h>

#include "helper.h"


static float* send_buffers[COMM_BUFFER_POOL_SIZE];
static float* recv_buffers[COMM_BUFFER_POOL_SIZE];
static starpu_data_handle_t send_buffer_handles[COMM_BUFFER_POOL_SIZE];
static starpu_data_handle_t recv_buffer_handles[COMM_BUFFER_POOL_SIZE];
static int current_comm_buffer = 0;
static int buffer_size;
static int rank;
static enum pingpong_type_e pingpong_type;


void ping_pong_init(int _rank, enum pingpong_type_e _pingpong_type, int _buffer_size)
{
    buffer_size = _buffer_size;
    rank = _rank;
    pingpong_type = _pingpong_type;

    for (int i = 0; i < COMM_BUFFER_POOL_SIZE; i++)
    {
	if ((pingpong_type == PING_PONG) || (pingpong_type == PING && rank == 0) || (pingpong_type == PONG && rank == 1))
	{
	    starpu_malloc((void**) &send_buffers[i], buffer_size * sizeof(float));
	    if (send_buffers[i] == NULL)
	    {
		abort();
	    }

	    for (int j = 0; j < buffer_size; j++)
	    {
		send_buffers[i][j] = 3.14;
	    }

	    starpu_vector_data_register(&send_buffer_handles[i], STARPU_MAIN_RAM, (uintptr_t) send_buffers[i], buffer_size, sizeof(float));
	}

	if ((pingpong_type == PING_PONG) || (pingpong_type == PING && rank == 1) || (pingpong_type == PONG && rank == 0))
	{
	    starpu_malloc((void**) &recv_buffers[i], buffer_size * sizeof(float));
	    if (recv_buffers[i] == NULL)
	    {
		abort();
	    }

	    memset(recv_buffers[i], 0, buffer_size * sizeof(float));

	    starpu_vector_data_register(&recv_buffer_handles[i], STARPU_MAIN_RAM, (uintptr_t) recv_buffers[i], buffer_size, sizeof(float));
	}
    }
}

void ping_pong_free()
{
    for (int i = 0; i < COMM_BUFFER_POOL_SIZE; i++)
    {
	if ((pingpong_type == PING_PONG) || (pingpong_type == PING && rank == 0) || (pingpong_type == PONG && rank == 1))
	{
	    starpu_data_unregister(send_buffer_handles[i]);
	    starpu_free_noflag(send_buffers[i], buffer_size*sizeof(float));
	}

	if ((pingpong_type == PING_PONG) || (pingpong_type == PING && rank == 1) || (pingpong_type == PONG && rank == 0))
	{
	    starpu_data_unregister(recv_buffer_handles[i]);
	    starpu_free_noflag(recv_buffers[i], buffer_size*sizeof(float));
	}
    }
}

double ping_pong(int comm_duplex)
{
    double start_time, end_time;
    double duration = 0;
    starpu_mpi_req send_req, recv_req;
    int other_rank = (rank == 1) ? 0 : 1;

    if (rank == 0)
    {
	start_time = starpu_timing_now();
	if (comm_duplex)
	{
	    starpu_mpi_isend(send_buffer_handles[current_comm_buffer], &send_req, other_rank, 1, MPI_COMM_WORLD);
	    starpu_mpi_irecv(recv_buffer_handles[current_comm_buffer], &recv_req, other_rank, 2, MPI_COMM_WORLD);
	    starpu_mpi_wait(&send_req, MPI_STATUS_IGNORE);
	    starpu_mpi_wait(&recv_req, MPI_STATUS_IGNORE);
	}
	else
	{
	    if (pingpong_type == PING_PONG || pingpong_type == PING)
	    {
		starpu_mpi_send(send_buffer_handles[current_comm_buffer], other_rank, 1, MPI_COMM_WORLD);
	    }
	    if (pingpong_type == PING_PONG || pingpong_type == PONG)
	    {
		starpu_mpi_recv(recv_buffer_handles[current_comm_buffer], other_rank, 2, MPI_COMM_WORLD, NULL);
	    }
	}
	end_time = starpu_timing_now();

	duration = (end_time - start_time);
	if (pingpong_type == PING_PONG)
	{
	    duration /= 2;
	}
    }
    else // rank == 1
    {
	if (comm_duplex)
	{
	    starpu_mpi_irecv(recv_buffer_handles[current_comm_buffer], &recv_req, other_rank, 1, MPI_COMM_WORLD);
	    starpu_mpi_isend(send_buffer_handles[current_comm_buffer], &send_req, other_rank, 2, MPI_COMM_WORLD);
	    starpu_mpi_wait(&recv_req, MPI_STATUS_IGNORE);
	    starpu_mpi_wait(&send_req, MPI_STATUS_IGNORE);
	}
	else
	{
	    if (pingpong_type == PING_PONG || pingpong_type == PING)
	    {
		starpu_mpi_recv(recv_buffer_handles[current_comm_buffer], other_rank, 1, MPI_COMM_WORLD, NULL);
	    }
	    if (pingpong_type == PING_PONG || pingpong_type == PONG)
	    {
		starpu_mpi_send(send_buffer_handles[current_comm_buffer], other_rank, 2, MPI_COMM_WORLD);
	    }
	}
    }

    current_comm_buffer = (current_comm_buffer + 1) % COMM_BUFFER_POOL_SIZE;

    return duration;
}

void bind_thread(struct machine_s machine, int thread_pu, char* name)
{
    if (starpu_bind_thread_on(thread_pu, 0, name) < 0)
    {
	printf("Error while binding PingPong thread\n");
    }
}

void runtime_shutdown(hwloc_topology_t topo, int comm_enabled)
{
    if (comm_enabled)
    {
	starpu_mpi_shutdown();
    }
    else
    {
	starpu_shutdown();
    }
}
