#include "helper_openmp_weak.h"

ARRAY_TYPE **weak_a, **weak_b, **weak_c;
int weak_array_size;
int nb_threads;
extern struct machine_s machine;

int weak_init()
{
    nb_threads = get_worker_count();
    weak_array_size = 2 * machine.l3_size / sizeof(ARRAY_TYPE);

    printf("# Each thread will work on an array of %d items (%ld KB)\n", weak_array_size, weak_array_size*sizeof(ARRAY_TYPE)/1024);

    weak_a = malloc(nb_threads*sizeof(ARRAY_TYPE*));
    weak_b = malloc(nb_threads*sizeof(ARRAY_TYPE*));
    weak_c = malloc(nb_threads*sizeof(ARRAY_TYPE*));

    #pragma omp parallel for
    for (int i = 0; i < nb_threads; i++)
    {
	weak_a[i] = comp_malloc(weak_array_size*sizeof(ARRAY_TYPE));
	weak_b[i] = comp_malloc(weak_array_size*sizeof(ARRAY_TYPE));
	weak_c[i] = comp_malloc(weak_array_size*sizeof(ARRAY_TYPE));

	for (int j = 0; j < weak_array_size; j++)
	{
	    weak_a[i][j] = 1.0;
	    weak_b[i][j] = 2.7;
	    weak_c[i][j] = 0.0;
	}
    }

    return 0;
}

void weak_release()
{
    for (int i = 0; i < nb_threads; i++)
    {
	comp_free(weak_a[i], weak_array_size*sizeof(ARRAY_TYPE));
	comp_free(weak_b[i], weak_array_size*sizeof(ARRAY_TYPE));
	comp_free(weak_c[i], weak_array_size*sizeof(ARRAY_TYPE));
    }

    free(weak_a);
    free(weak_b);
    free(weak_c);
}
