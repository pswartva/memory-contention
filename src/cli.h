#ifndef __CLI_H
#define __CLI_H

#include "helper.h"

#define DO_COMP_ALONE (1 << 0)
#define DO_COMM_ALONE (1 << 1)

#define MAX_NUMA_NODES 16

enum param_e {
    DISABLE_COMM,
    PING_THREAD,
    BENCH,
    COMPUTE_BENCH,
    HIDE_COMMS,
    DUPLEX,
    DEBUG,
    NO_WARMUP,
    NO_ALONE,
    DISPLAY_BINDING,
    PINGPONG_SIZE,
    BIND_MEMORY_COMP,
    BIND_MEMORY_COMM,
    ONE_COMPUTING_RANK,
    ONLY_PING,
    ONLY_PONG,
    PER_THREAD_PERF,
#if (WITH_LIKWID == 1) || (WITH_DIRECT_CPU_FREQ == 1)
    FREQ,
#endif
    _LAST_PARAM
};

struct params_s {
    int enable_comm;
    int ping_thread_pu;
    enum comm_bench_type comm_bench_type;
    struct computing_functions computing;
    int display_comm;
    int comm_duplex;
    int debug;
    int do_warmup;
    int do_alone;
    int display_binding;
    int pingpong_size;
    int display_help;
    int memory_comp_numa_nodes[MAX_NUMA_NODES];
    unsigned memory_comp_numa_nodes_nb;
    int memory_comm_numa_node;
    int one_computing_rank;
    int per_thread_perf;
#if (WITH_LIKWID == 1) || (WITH_DIRECT_CPU_FREQ == 1)
    int measure_freq;
#endif
    enum {
        FIRST,
        AFTER,
        LAST,
        CUSTOM
    } ping_thread_location;
    enum pingpong_type_e pingpong_type;
};

void init_params();
void parse_args(int argc, char* argv[], struct params_s* params);
void print_params(struct params_s params);
void print_help(struct params_s params);
void disable_param(enum param_e param);
void enable_param(enum param_e param);
void disable_all_params();
void print_machine(struct machine_s* machine, struct params_s* params);
void ping_thread_location_to_pu(struct params_s* params, struct machine_s* machine);

#define RANK_DOES_COMPUTATIONS (!params.one_computing_rank || rank == 0)

#endif /* __CLI_H */
