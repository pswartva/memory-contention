#include <float.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <omp.h>
#include <x86intrin.h>

#include "timing.h"
#include "scalar_avx.h"

#define ARRAY_SIZE 128

// array to be accessed with bench_type enum to know if we already launched this bench
// to prevent erasing old data bench
static char bench_done[] = {0, 0, 0};

static double scalar_avx_perfs_warmup[] = {FLT_MAX, 0, 0};
static double scalar_avx_perfs_no_comm[] = {FLT_MAX, 0, 0};
static double scalar_avx_perfs_comm[] = {FLT_MAX, 0, 0};

static volatile int* comm_bench_ended = NULL;


void scalar_avx_set_comm_bench_ended_ptr(volatile int* _comm_bench_ended)
{
    comm_bench_ended = _comm_bench_ended;
}

static float __attribute__((optimize("O0"))) scalar_avx_product(float* a, float* b)
{
    /* From https://github.com/lemire/Code-used-on-Daniel-Lemire-s-blog/blob/master/2018/09/04/scalar.c
     * See https://lemire.me/blog/2018/09/04/per-core-frequency-scaling-and-avx-512-an-experiment/
     *
     * Make many times some weird scalar product. Just to do AVX instructions, we don't care about the
     * result. */

#if defined(__AVX512F__)
#define VECTOR_LENGTH 16
    __m512 vsum1 = _mm512_setzero_ps();
    __m512 vsum2 = _mm512_setzero_ps();
    __m512 vsum3 = _mm512_setzero_ps();
    __m512 vsum4 = _mm512_setzero_ps();
    __m512 vsum5 = _mm512_setzero_ps();
    __m512 vsum6 = _mm512_setzero_ps();
    __m512 vsum7 = _mm512_setzero_ps();
    __m512 vsum8 = _mm512_setzero_ps();
    __m512 vtriple = _mm512_set_ps(3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0);
    __m512 vdivide = _mm512_set_ps(0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3);
    float buffer[VECTOR_LENGTH];
    __m512 a0 = _mm512_loadu_ps(a);
    __m512 a1 = _mm512_loadu_ps(a + 16);
    __m512 a2 = _mm512_loadu_ps(a + 32);
    __m512 a3 = _mm512_loadu_ps(a + 48);
    __m512 a4 = _mm512_loadu_ps(a + 64);
    __m512 a5 = _mm512_loadu_ps(a + 80);
    __m512 a6 = _mm512_loadu_ps(a + 96);
    __m512 a7 = _mm512_loadu_ps(a + 112);
    __m512 b0 = _mm512_loadu_ps(b);
    __m512 b1 = _mm512_loadu_ps(b + 16);
    __m512 b2 = _mm512_loadu_ps(b + 32);
    __m512 b3 = _mm512_loadu_ps(b + 48);
    __m512 b4 = _mm512_loadu_ps(b + 64);
    __m512 b5 = _mm512_loadu_ps(b + 80);
    __m512 b6 = _mm512_loadu_ps(b + 96);
    __m512 b7 = _mm512_loadu_ps(b + 112);
    __m512 vsum;

    // multiply a and divide b, just to add more AVX instructions

    for (int j = 0; j < 3000000; j++)
    {
	vsum1 = _mm512_fmadd_ps(_mm512_mul_ps(vtriple, a0), _mm512_mul_ps(vdivide, b0), vsum1);
	vsum2 = _mm512_fmadd_ps(_mm512_mul_ps(vtriple, a1), _mm512_mul_ps(vdivide, b1), vsum2);
	vsum3 = _mm512_fmadd_ps(_mm512_mul_ps(vtriple, a2), _mm512_mul_ps(vdivide, b2), vsum3);
	vsum4 = _mm512_fmadd_ps(_mm512_mul_ps(vtriple, a3), _mm512_mul_ps(vdivide, b3), vsum4);
	vsum5 = _mm512_fmadd_ps(_mm512_mul_ps(vtriple, a4), _mm512_mul_ps(vdivide, b4), vsum5);
	vsum6 = _mm512_fmadd_ps(_mm512_mul_ps(vtriple, a5), _mm512_mul_ps(vdivide, b5), vsum6);
	vsum7 = _mm512_fmadd_ps(_mm512_mul_ps(vtriple, a6), _mm512_mul_ps(vdivide, b6), vsum7);
	vsum8 = _mm512_fmadd_ps(_mm512_mul_ps(vtriple, a7), _mm512_mul_ps(vdivide, b7), vsum8);
	vsum = _mm512_add_ps(
	    _mm512_add_ps(_mm512_add_ps(vsum1, vsum2), _mm512_add_ps(vsum3, vsum4)),
	    _mm512_add_ps(_mm512_add_ps(vsum5, vsum6), _mm512_add_ps(vsum7, vsum8))
	);
	_mm512_storeu_ps(buffer, vsum);
    }

#elif defined(__AVX2__)
#define VECTOR_LENGTH 8
    __m256 vsum1 = _mm256_setzero_ps();
    __m256 vsum2 = _mm256_setzero_ps();
    __m256 vsum3 = _mm256_setzero_ps();
    __m256 vsum4 = _mm256_setzero_ps();
    __m256 vsum5 = _mm256_setzero_ps();
    __m256 vsum6 = _mm256_setzero_ps();
    __m256 vsum7 = _mm256_setzero_ps();
    __m256 vsum8 = _mm256_setzero_ps();
    __m256 vtriple = _mm256_set_ps(3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0);
    __m256 vdivide = _mm256_set_ps(0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3);
    float buffer[VECTOR_LENGTH];
    __m256 a0 = _mm256_loadu_ps(a);
    __m256 a1 = _mm256_loadu_ps(a + 8);
    __m256 a2 = _mm256_loadu_ps(a + 16);
    __m256 a3 = _mm256_loadu_ps(a + 24);
    __m256 a4 = _mm256_loadu_ps(a + 32);
    __m256 a5 = _mm256_loadu_ps(a + 40);
    __m256 a6 = _mm256_loadu_ps(a + 48);
    __m256 a7 = _mm256_loadu_ps(a + 56);
    __m256 b0 = _mm256_loadu_ps(b);
    __m256 b1 = _mm256_loadu_ps(b + 8);
    __m256 b2 = _mm256_loadu_ps(b + 16);
    __m256 b3 = _mm256_loadu_ps(b + 24);
    __m256 b4 = _mm256_loadu_ps(b + 32);
    __m256 b5 = _mm256_loadu_ps(b + 40);
    __m256 b6 = _mm256_loadu_ps(b + 48);
    __m256 b7 = _mm256_loadu_ps(b + 56);
    __m256 vsum;

    for (int j = 0; j < 3000000; j++)
    {
	vsum1 = _mm256_fmadd_ps(_mm256_mul_ps(vtriple, a0), _mm256_mul_ps(vdivide, b0), vsum1);
	vsum2 = _mm256_fmadd_ps(_mm256_mul_ps(vtriple, a1), _mm256_mul_ps(vdivide, b1), vsum2);
	vsum3 = _mm256_fmadd_ps(_mm256_mul_ps(vtriple, a2), _mm256_mul_ps(vdivide, b2), vsum3);
	vsum4 = _mm256_fmadd_ps(_mm256_mul_ps(vtriple, a3), _mm256_mul_ps(vdivide, b3), vsum4);
	vsum5 = _mm256_fmadd_ps(_mm256_mul_ps(vtriple, a4), _mm256_mul_ps(vdivide, b4), vsum5);
	vsum6 = _mm256_fmadd_ps(_mm256_mul_ps(vtriple, a5), _mm256_mul_ps(vdivide, b5), vsum6);
	vsum7 = _mm256_fmadd_ps(_mm256_mul_ps(vtriple, a6), _mm256_mul_ps(vdivide, b6), vsum7);
	vsum8 = _mm256_fmadd_ps(_mm256_mul_ps(vtriple, a7), _mm256_mul_ps(vdivide, b7), vsum8);
	vsum = _mm256_add_ps(
	    _mm256_add_ps(_mm256_add_ps(vsum1, vsum2), _mm256_add_ps(vsum3, vsum4)),
	    _mm256_add_ps(_mm256_add_ps(vsum5, vsum6), _mm256_add_ps(vsum7, vsum8))
	);
	_mm256_storeu_ps(buffer, vsum);
    }

#else
#error "No AVX instruction set"
#endif

#undef VECTOR_LENGTH
    return buffer[0];
}

static void __attribute__((optimize("O0"))) dummy(void* var)
{
    (void) var;
}

static double scalar_avx_run_kernel()
{
    puk_tick_t start_time, end_time;

    PUK_GET_TICK(start_time);
    #pragma omp parallel
    {
	float* a = comp_malloc(ARRAY_SIZE * sizeof(float));
	float* b = comp_malloc(ARRAY_SIZE * sizeof(float));

	for (int i = 0; i < ARRAY_SIZE; i++)
	{
	    a[i] = i;
	    b[i] = i;
	}

	float result = scalar_avx_product(a, b);
	dummy((void*) &result);

	comp_free(a, ARRAY_SIZE * sizeof(float));
	comp_free(b, ARRAY_SIZE * sizeof(float));
    }
    PUK_GET_TICK(end_time);

    return PUK_TIMING_DELAY(start_time, end_time);
}

int scalar_avx_run(int nb_runs, enum bench_type bench_type)
{
    if (bench_type != WARMUP && bench_done[bench_type])
    {
	printf("Warning: this bench was already done.\n");
    }

    double avgtime = 0, maxtime = 0, mintime = FLT_MAX;
    int real_nb_runs = nb_runs;
    double duration;

    for (int k = 0; k < nb_runs; k++)
    {
	duration = scalar_avx_run_kernel();

	mintime = MIN(mintime, duration);
	avgtime += duration;
	maxtime = MAX(maxtime, duration);
    }

    if (bench_type == WITH_COMM && comm_bench_ended != NULL)
    {
	/* Keep computing while we need more pingpongs: */
	while (!*comm_bench_ended)
	{
	    duration = scalar_avx_run_kernel();

	    mintime = MIN(mintime, duration);
	    avgtime += duration;
	    maxtime = MAX(maxtime, duration);

	    real_nb_runs++;
	}
    }

    if (bench_done[bench_type])
    {
	return 0;
    }

    if (bench_type == WARMUP)
    {
	scalar_avx_perfs_warmup[0] = mintime;
	scalar_avx_perfs_warmup[1] = avgtime / (double) (real_nb_runs);
	scalar_avx_perfs_warmup[2] = maxtime;
    }
    else if (bench_type == WITH_COMM)
    {
	scalar_avx_perfs_comm[0] = mintime;
	scalar_avx_perfs_comm[1] = avgtime / (double) (real_nb_runs);
	scalar_avx_perfs_comm[2] = maxtime;
    }
    else if (bench_type == WITHOUT_COMM)
    {
	scalar_avx_perfs_no_comm[0] = mintime;
	scalar_avx_perfs_no_comm[1] = avgtime / (double) (real_nb_runs);
	scalar_avx_perfs_no_comm[2] = maxtime;
    }
    else
    {
	abort();
    }

    bench_done[bench_type] = 1;

    return 0;
}

void scalar_avx_print_results()
{
    printf("# scalar_avx results: Time ms (min, avg, max)\n");
    if (bench_done[WARMUP])
    {
	printf("# warmup                 "COMP_TIME_FORMAT"\t"COMP_TIME_FORMAT"\t"COMP_TIME_FORMAT"\n",
		scalar_avx_perfs_warmup[0] / 1000.0f, scalar_avx_perfs_warmup[1] / 1000.0f, scalar_avx_perfs_warmup[2] / 1000.0f);
    }
    if (bench_done[WITH_COMM])
    {
	printf("# with communications    "COMP_TIME_FORMAT"\t"COMP_TIME_FORMAT"\t"COMP_TIME_FORMAT"\n",
		scalar_avx_perfs_comm[0] / 1000.0f, scalar_avx_perfs_comm[1] / 1000.0f, scalar_avx_perfs_comm[2] / 1000.0f);
    }
    if (bench_done[WITHOUT_COMM])
    {
	printf("# without communications "COMP_TIME_FORMAT"\t"COMP_TIME_FORMAT"\t"COMP_TIME_FORMAT"\n",
		scalar_avx_perfs_no_comm[0] / 1000.0f, scalar_avx_perfs_no_comm[1] / 1000.0f, scalar_avx_perfs_no_comm[2] / 1000.0f);
    }
}


int scalar_avx_get_nb_runs(enum comm_bench_type comm_bench_type)
{
    return 10;
}


int scalar_avx_init()
{
#if defined(__AVX512F__)
    printf("# Using AVX512 instructions\n");
#elif defined(__AVX2__)
    printf("# Using AVX2 instructions\n");
#else
#error "No AVX instruction set"
#endif

    return 0;
}

struct computing_functions scalar_avx_get_functions()
{
    struct computing_functions s = {
	.init = &scalar_avx_init,
	.run = &scalar_avx_run,
	.print_results = &scalar_avx_print_results,
	.print_params = NULL,
	.release = NULL,
	.parse_arg = NULL,
	.man = NULL,
	.get_nb_runs = &scalar_avx_get_nb_runs,
	.set_comm_bench_ended_ptr = &scalar_avx_set_comm_bench_ended_ptr,
	.name = "scalar_avx"
    };

    return s;
}
