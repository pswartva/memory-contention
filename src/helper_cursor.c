#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <float.h>

#include "helper_cursor.h"


char cursor_bench_done[] = {0, 0, 0};

double cursor_perfs_warmup[] = {FLT_MAX, 0, 0};
double cursor_perfs_no_comm[] = {FLT_MAX, 0, 0};
double cursor_perfs_comm[] = {FLT_MAX, 0, 0};

volatile int* cursor_comm_bench_ended = NULL;

int cursor = 0;


void cursor_set_comm_bench_ended_ptr(volatile int* _comm_bench_ended)
{
    cursor_comm_bench_ended = _comm_bench_ended;
}

void cursor_man()
{
    printf("Cursor-related options:\n");
    printf("\t--cursor=<cursor>\n");
    printf("\tWhen cursor is 0, the bench is full memory-bound\n");
    printf("\tThe higher the cursor is, the more the bench is CPU-bound\n");
}

int cursor_get_nb_runs(enum comm_bench_type comm_bench_type)
{
    return 20;
}

void cursor_print_results()
{
    printf("# cursor results: Time ms (min, avg, max)\n");
    if (cursor_bench_done[WARMUP])
    {
	printf("# warmup                 "COMP_TIME_FORMAT"\t"COMP_TIME_FORMAT"\t"COMP_TIME_FORMAT"\n", cursor_perfs_warmup[0] / 1000.0f, cursor_perfs_warmup[1] / 1000.0f, cursor_perfs_warmup[2] / 1000.0f);
    }
    if (cursor_bench_done[WITH_COMM])
    {
	printf("# with communications    "COMP_TIME_FORMAT"\t"COMP_TIME_FORMAT"\t"COMP_TIME_FORMAT"\n", cursor_perfs_comm[0] / 1000.0f, cursor_perfs_comm[1] / 1000.0f, cursor_perfs_comm[2] / 1000.0f);
    }
    if (cursor_bench_done[WITHOUT_COMM])
    {
	printf("# without communications "COMP_TIME_FORMAT"\t"COMP_TIME_FORMAT"\t"COMP_TIME_FORMAT"\n", cursor_perfs_no_comm[0] / 1000.0f, cursor_perfs_no_comm[1] / 1000.0f, cursor_perfs_no_comm[2] / 1000.0f);
    }
}

void cursor_print_params()
{
    printf("# Cursor is %d\n", cursor);
}

int cursor_parse_arg(char* arg)
{
    if (strncmp(arg, "--cursor=", 9) == 0)
    {
	sscanf(arg, "--cursor=%d", &cursor);
	return 1;
    }

    return 0;
}
