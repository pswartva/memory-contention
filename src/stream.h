#ifndef __STREAM_H
#define __STREAM_H

#include "helper.h"

int stream_init();
int stream_run(int nb_runs, enum bench_type bench_type);
void stream_release();
void stream_print_results();
struct computing_functions stream_get_functions();

#endif // __STREAM_H
