#include <float.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <omp.h>

#include "config.h"

#if HAVE_NON_TEMPORAL_STORE
#if HAVE_DECL__MM_STREAM_SI32
#include <x86intrin.h>
#elif HAVE_ARM_NT_STORE
#include <arm_neon.h>
#endif
#endif

#include "cli.h"
#include "timing.h"

#define MEMSET_TYPE int
#define NB_ITER 3
#define THROUGHPUT_DURATION 3000000 // 3 sec

static MEMSET_TYPE **a;
static int array_size;
static int nb_threads;
static int use_non_temporal = 0;
static int measure_throughput = 0;
extern struct machine_s machine;
extern struct params_s params;

char memset_bench_done[] = {0, 0, 0};

// All in microseconds:
static double memset_perfs_warmup[] = {FLT_MAX, 0, 0};
static double memset_perfs_no_comm[] = {FLT_MAX, 0, 0};
static double memset_perfs_comm[] = {FLT_MAX, 0, 0};
static double* memset_perfs_per_thread_no_comm;
static double* memset_perfs_per_thread_comm;

volatile int* memset_comm_bench_ended = NULL;


#if HAVE_NON_TEMPORAL_STORE
static inline __attribute__((always_inline)) void non_temporal_store(int *p, int a)
{
#if HAVE_DECL__MM_STREAM_SI32
    _mm_stream_si32(p, a);
#elif HAVE_ARM_NT_STORE
    vst1q_lane_s32((int32_t *) p, vdupq_n_s32(a), 0);
#endif
}
#endif


static void memset_set_comm_bench_ended_ptr(volatile int* _comm_bench_ended)
{
    memset_comm_bench_ended = _comm_bench_ended;
}

static int memset_get_nb_runs(enum comm_bench_type comm_bench_type)
{
    return measure_throughput ? 3 : 20;
}

static inline double time_to_bw(double t) // microsec
{
    return (sizeof(MEMSET_TYPE) * array_size * nb_threads) / t; // MB/s
}

static inline double time_to_bw_per_thread(double t) // microsec
{
    return (sizeof(MEMSET_TYPE) * array_size) / t; // MB/s
}

static void memset_print_results()
{
    if (params.per_thread_perf)
    {
	int* thread_binding = malloc(nb_threads * sizeof(int));
	memset(thread_binding, 0, nb_threads * sizeof(int));
	get_worker_binding_ids(machine.topology, nb_threads, thread_binding);

	if (memset_bench_done[WITH_COMM])
	{
	    printf("# memset results per thread (with comm): Bandwidth MB/s (max, avg, min) Time ms (min, avg, max)\n");
	    for (int i = 0; i < nb_threads; i++)
	    {
		printf("Thread #%d\t%d\t"COMP_BW_FORMAT"\t"COMP_BW_FORMAT"\t"COMP_BW_FORMAT"\t"COMP_TIME_FORMAT"\t"COMP_TIME_FORMAT"\t"COMP_TIME_FORMAT"\n",
			i, thread_binding[i],
			time_to_bw_per_thread(memset_perfs_per_thread_comm[i*3]), time_to_bw_per_thread(memset_perfs_per_thread_comm[i*3+1]), time_to_bw_per_thread(memset_perfs_per_thread_comm[i*3+2]),
			memset_perfs_per_thread_comm[i*3] / 1000.0f, memset_perfs_per_thread_comm[i*3+1] / 1000.0f, memset_perfs_per_thread_comm[i*3+2] / 1000.0f);
	    }
	}
	if (memset_bench_done[WITHOUT_COMM])
	{
	    printf("# memset results per thread (without comm): Bandwidth MB/s (max, avg, min) Time ms (min, avg, max)\n");
	    for (int i = 0; i < nb_threads; i++)
	    {
		printf("Thread #%d\t%d\t"COMP_BW_FORMAT"\t"COMP_BW_FORMAT"\t"COMP_BW_FORMAT"\t"COMP_TIME_FORMAT"\t"COMP_TIME_FORMAT"\t"COMP_TIME_FORMAT"\n",
			i, thread_binding[i],
			time_to_bw_per_thread(memset_perfs_per_thread_no_comm[i*3]), time_to_bw_per_thread(memset_perfs_per_thread_no_comm[i*3+1]), time_to_bw_per_thread(memset_perfs_per_thread_no_comm[i*3+2]),
			memset_perfs_per_thread_no_comm[i*3] / 1000.0f, memset_perfs_per_thread_no_comm[i*3+1] / 1000.0f, memset_perfs_per_thread_no_comm[i*3+2] / 1000.0f);
	    }
	}

	free(thread_binding);
    }

    printf("# memset results: Bandwidth MB/s (max, avg, min) Time ms (min, avg, max)\n");
    if (memset_bench_done[WARMUP])
    {
	if (measure_throughput)
	{
	    printf("# warmup                 -\t-\t-\t-\t-\t-\n");
	}
	else
	{
	    printf("# warmup                 "COMP_BW_FORMAT"\t"COMP_BW_FORMAT"\t"COMP_BW_FORMAT"\t"COMP_TIME_FORMAT"\t"COMP_TIME_FORMAT"\t"COMP_TIME_FORMAT"\n",
		    time_to_bw(memset_perfs_warmup[0]), time_to_bw(memset_perfs_warmup[1]), time_to_bw(memset_perfs_warmup[2]),
		    memset_perfs_warmup[0] / 1000.0f, memset_perfs_warmup[1] / 1000.0f, memset_perfs_warmup[2] / 1000.0f);
	}
    }
    if (memset_bench_done[WITH_COMM])
    {
	if (measure_throughput)
	{
	    printf("# with communications    -\t-\t-\t-\t-\t-\n");
	}
	else
	{
	    printf("# with communications    "COMP_BW_FORMAT"\t"COMP_BW_FORMAT"\t"COMP_BW_FORMAT"\t"COMP_TIME_FORMAT"\t"COMP_TIME_FORMAT"\t"COMP_TIME_FORMAT"\n",
		    time_to_bw(memset_perfs_comm[0]), time_to_bw(memset_perfs_comm[1]), time_to_bw(memset_perfs_comm[2]),
		    memset_perfs_comm[0] / 1000.0f, memset_perfs_comm[1] / 1000.0f, memset_perfs_comm[2] / 1000.0f);
	}
    }
    if (memset_bench_done[WITHOUT_COMM])
    {
	if (measure_throughput)
	{
	    printf("# without communications -\t-\t-\t-\t-\t-\n");
	}
	else
	{
	    printf("# without communications "COMP_BW_FORMAT"\t"COMP_BW_FORMAT"\t"COMP_BW_FORMAT"\t"COMP_TIME_FORMAT"\t"COMP_TIME_FORMAT"\t"COMP_TIME_FORMAT"\n",
		    time_to_bw(memset_perfs_no_comm[0]), time_to_bw(memset_perfs_no_comm[1]), time_to_bw(memset_perfs_no_comm[2]),
		    memset_perfs_no_comm[0] / 1000.0f, memset_perfs_no_comm[1] / 1000.0f, memset_perfs_no_comm[2] / 1000.0f);
	}
    }
}

static int memset_init()
{
    nb_threads = get_nb_openmp_threads();
    array_size = params.enable_comm ? params.pingpong_size : COMM_BANDWIDTH_BUFFER_SIZE;

    printf("# Each thread will work on an array of %d items (%ld KB)\n", array_size, array_size*sizeof(MEMSET_TYPE)/1024);

    a = malloc(nb_threads*sizeof(MEMSET_TYPE*));

    memset_perfs_per_thread_no_comm = malloc(nb_threads*3*sizeof(double));
    memset_perfs_per_thread_comm = malloc(nb_threads*3*sizeof(double));

    #pragma omp parallel for
    for (int i = 0; i < nb_threads; i++)
    {
	a[i] = data_malloc(
	    array_size*sizeof(MEMSET_TYPE),
	    params.memory_comp_numa_nodes_nb == 0 ? -1 : get_numa_node_comp_for_id(i % params.memory_comp_numa_nodes_nb));

	for (int j = 0; j < array_size; j++)
	{
	    a[i][j] = 1;
	}

	memset_perfs_per_thread_no_comm[i*3] = FLT_MAX;
	memset_perfs_per_thread_no_comm[i*3+1] = 0.0f;
	memset_perfs_per_thread_no_comm[i*3+2] = 0.0f;
	memset_perfs_per_thread_comm[i*3] = FLT_MAX;
	memset_perfs_per_thread_comm[i*3+1] = 0.0f;
	memset_perfs_per_thread_comm[i*3+2] = 0.0f;
    }

    return 0;
}

static double memset_run_kernel(enum bench_type bench_type)
{
    puk_tick_t start_time, end_time;
    MEMSET_TYPE scalar = 3;
    double* per_thread_perfs = NULL;

    if (bench_type == WITH_COMM)
    {
	per_thread_perfs = memset_perfs_per_thread_comm;
    }
    else if (bench_type == WITHOUT_COMM)
    {
	per_thread_perfs = memset_perfs_per_thread_no_comm;
    }

    puk_tick_t* thread_start_times = malloc(nb_threads * sizeof(puk_tick_t));
    puk_tick_t* thread_end_times = malloc(nb_threads * sizeof(puk_tick_t));
    double* thread_durations = malloc(nb_threads * sizeof(double));
    unsigned* nb_iter_per_thread = malloc(nb_threads * sizeof(unsigned));

    PUK_GET_TICK(start_time);
    if (measure_throughput)
    {
	#pragma omp parallel for
	for (int i = 0; i < nb_threads; i++)
	{
	    nb_iter_per_thread[i] = 0;
	    thread_durations[i] = 0;

	    puk_tick_t start_iter_time, end_iter_time;
	    double last_iter_duration = 0;
	    do
	    {
#if HAVE_NON_TEMPORAL_STORE
		if (use_non_temporal)
		{
		    PUK_GET_TICK(start_iter_time);
		    int value = scalar * (nb_iter_per_thread[i]+1);
		    for (int k = 0; k < array_size; k++)
		    {
			non_temporal_store(a[i]+k, value);
		    }
		    PUK_GET_TICK(end_iter_time);
		}
		else
#endif
		{
		    PUK_GET_TICK(start_iter_time);
		    memset(a[i], scalar * (nb_iter_per_thread[i]+1), array_size*sizeof(MEMSET_TYPE));
		    PUK_GET_TICK(end_iter_time);
		}
		if (nb_iter_per_thread[i] > 0)
		{
		    last_iter_duration = PUK_TIMING_DELAY(start_iter_time, end_iter_time);
		    thread_durations[i] += last_iter_duration;
		}
		nb_iter_per_thread[i]++;
	    } while (PUK_TIMING_DELAY(start_time, end_iter_time) < THROUGHPUT_DURATION);
	    nb_iter_per_thread[i] -= 2;
	    thread_durations[i] -= last_iter_duration;
	    thread_durations[i] /= nb_iter_per_thread[i];
	}
    }
    else
    {
	#pragma omp parallel for
	for (int i = 0; i < nb_threads; i++)
	{
#if HAVE_NON_TEMPORAL_STORE
	    if (use_non_temporal)
	    {
		PUK_GET_TICK(thread_start_times[i]);
		for (int j = 1; j <= NB_ITER; j++)
		{
		    int value = scalar * j;
		    for (int k = 0; k < array_size; k++)
		    {
			non_temporal_store(a[i]+k, value);
		    }
		}
		PUK_GET_TICK(thread_end_times[i]);
	    }
	    else
#endif
	    {
		PUK_GET_TICK(thread_start_times[i]);
		for (int j = 1; j <= NB_ITER; j++)
		{
		    memset(a[i], scalar*j, array_size*sizeof(MEMSET_TYPE));
		}
		PUK_GET_TICK(thread_end_times[i]);
	    }
	}
    }
    PUK_GET_TICK(end_time);

    if (params.per_thread_perf && (bench_type == WITH_COMM || bench_type == WITHOUT_COMM))
    {
	for (int i = 0; i < nb_threads; i++)
	{
	    double thread_duration;
	    if (measure_throughput)
	    {
		thread_duration = thread_durations[i];
	    }
	    else
	    {
		thread_duration = PUK_TIMING_DELAY(thread_start_times[i], thread_end_times[i]) / NB_ITER;
	    }
	    per_thread_perfs[i*3] = MIN(per_thread_perfs[i*3], thread_duration);
	    per_thread_perfs[i*3+1] += thread_duration;
	    per_thread_perfs[i*3+2] = MAX(per_thread_perfs[i*3+2], thread_duration);
	}
    }

    if (measure_throughput)
    {
	for (int i = 0; i < nb_threads; i++)
	{
	    if (nb_iter_per_thread[i] < NB_ITER)
	    {
		fprintf(stderr, "# Warning: thread %d did less than %d iterations (%u).\n", i, NB_ITER, nb_iter_per_thread[i]);
	    }
	}
    }

    free(thread_start_times);
    free(thread_end_times);
    free(nb_iter_per_thread);

    return PUK_TIMING_DELAY(start_time, end_time) / NB_ITER;
}

static int memset_run(int nb_runs, enum bench_type bench_type)
{
    if (bench_type != WARMUP && memset_bench_done[bench_type])
    {
	printf("Warning: this bench was already done.\n");
    }

    double avgtime = 0, maxtime = 0, mintime = FLT_MAX;
    int real_nb_runs = nb_runs;
    double duration;

    for (int k = 0; k < nb_runs; k++)
    {
	duration = memset_run_kernel(bench_type);

	mintime = MIN(mintime, duration);
	avgtime += duration;
	maxtime = MAX(maxtime, duration);
    }

    if (bench_type == WITH_COMM && memset_comm_bench_ended != NULL)
    {
	/* Keep computing while we need more pingpongs: */
	while (!*memset_comm_bench_ended)
	{
	    duration = memset_run_kernel(bench_type);

	    mintime = MIN(mintime, duration);
	    avgtime += duration;
	    maxtime = MAX(maxtime, duration);

	    real_nb_runs++;
	}
    }

    if (memset_bench_done[bench_type])
    {
	return 0;
    }

    if (bench_type == WARMUP)
    {
	memset_perfs_warmup[0] = mintime;
	memset_perfs_warmup[1] = avgtime / (double) (real_nb_runs);
	memset_perfs_warmup[2] = maxtime;
    }
    else if (bench_type == WITH_COMM)
    {
	memset_perfs_comm[0] = mintime;
	memset_perfs_comm[1] = avgtime / (double) (real_nb_runs);
	memset_perfs_comm[2] = maxtime;

	if (params.per_thread_perf)
	{
	    for (int i = 0; i < nb_threads; i++)
	    {
		memset_perfs_per_thread_comm[i*3+1] /= (double) (real_nb_runs);
	    }
	}
    }
    else if (bench_type == WITHOUT_COMM)
    {
	memset_perfs_no_comm[0] = mintime;
	memset_perfs_no_comm[1] = avgtime / (double) (real_nb_runs);
	memset_perfs_no_comm[2] = maxtime;

	if (params.per_thread_perf)
	{
	    for (int i = 0; i < nb_threads; i++)
	    {
		memset_perfs_per_thread_no_comm[i*3+1] /= (double) (real_nb_runs);
	    }
	}
    }
    else
    {
	abort();
    }

    memset_bench_done[bench_type] = 1;

    return 0;
}

static void memset_release()
{
    for (int i = 0; i < nb_threads; i++)
    {
	data_free(
	    a[i],
	    array_size*sizeof(MEMSET_TYPE),
	    params.memory_comp_numa_nodes_nb == 0 ? -1 : get_numa_node_comp_for_id(i % params.memory_comp_numa_nodes_nb));
    }

    free(a);
    free(memset_perfs_per_thread_no_comm);
    free(memset_perfs_per_thread_comm);
}

static void memset_man()
{
    printf("Memset-related options:\n");
#if HAVE_NON_TEMPORAL_STORE
    printf("\t--nt\tuse non-temporal stores to bypass the LLC\n");
#endif
    printf("\t--throughput\tmeasure memory throughput\n");
}

static void memset_print_params()
{
    printf("# Will %suse non-temporal memset\n", use_non_temporal ? "" : "not ");
    printf("# Will %smeasure memory bandwidth with throughput\n", measure_throughput ? "" : "not ");
}

static int memset_parse_arg(char* arg)
{
#if HAVE_NON_TEMPORAL_STORE
    if (strcmp(arg, "--nt") == 0)
    {
	use_non_temporal = 1;
	return 1;
    }
#endif
    if (strcmp(arg, "--throughput") == 0)
    {
	measure_throughput = 1;
	return 1;
    }

    return 0;
}

struct computing_functions memset_get_functions()
{
    struct computing_functions s = {
	.init = &memset_init,
	.run = &memset_run,
	.print_results = &memset_print_results,
	.print_params = &memset_print_params,
	.release = &memset_release,
	.parse_arg = &memset_parse_arg,
	.man = &memset_man,
	.get_nb_runs = &memset_get_nb_runs,
	.set_comm_bench_ended_ptr = &memset_set_comm_bench_ended_ptr,
	.name = "memset"
    };

    return s;
}
