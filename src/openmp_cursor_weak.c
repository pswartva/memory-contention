#include <float.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <omp.h>

#include "timing.h"
#include "helper_openmp_weak.h"
#include "cursor.h"
#include "helper_cursor.h"

extern ARRAY_TYPE **weak_a, **weak_b, **weak_c;
extern int weak_array_size;
extern int nb_threads;


static double cursor_weak_run_kernel()
{
    puk_tick_t start_time, end_time;

    PUK_GET_TICK(start_time);
    #pragma omp parallel for
    for (int i = 0; i < nb_threads; i++)
    {
	for (int j = 0; j < weak_array_size; j++)
	{
	    for (int k = 0; k < cursor + 1; k++)
	    {
		weak_b[i][j] += (weak_a[i][j] + k * weak_c[i][j]);
	    }
	}
    }
    PUK_GET_TICK(end_time);

    return PUK_TIMING_DELAY(start_time, end_time);
}

static int cursor_weak_run(int nb_runs, enum bench_type bench_type)
{
    if (bench_type != WARMUP && cursor_bench_done[bench_type])
    {
	printf("Warning: this bench was already done.\n");
    }

    double avgtime = 0, maxtime = 0, mintime = FLT_MAX;
    int real_nb_runs = nb_runs;
    double duration;

    for (int k = 0; k < nb_runs; k++)
    {
	duration = cursor_weak_run_kernel();

	mintime = MIN(mintime, duration);
	avgtime += duration;
	maxtime = MAX(maxtime, duration);
    }

    if (bench_type == WITH_COMM && cursor_comm_bench_ended != NULL)
    {
	/* Keep computing while we need more pingpongs: */
	while (!*cursor_comm_bench_ended)
	{
	    duration = cursor_weak_run_kernel();

	    mintime = MIN(mintime, duration);
	    avgtime += duration;
	    maxtime = MAX(maxtime, duration);

	    real_nb_runs++;
	}
    }

    if (cursor_bench_done[bench_type])
    {
	return 0;
    }

    if (bench_type == WARMUP)
    {
	cursor_perfs_warmup[0] = mintime;
	cursor_perfs_warmup[1] = avgtime / (double) (real_nb_runs);
	cursor_perfs_warmup[2] = maxtime;
    }
    else if (bench_type == WITH_COMM)
    {
	cursor_perfs_comm[0] = mintime;
	cursor_perfs_comm[1] = avgtime / (double) (real_nb_runs);
	cursor_perfs_comm[2] = maxtime;
    }
    else if (bench_type == WITHOUT_COMM)
    {
	cursor_perfs_no_comm[0] = mintime;
	cursor_perfs_no_comm[1] = avgtime / (double) (real_nb_runs);
	cursor_perfs_no_comm[2] = maxtime;
    }
    else
    {
	abort();
    }

    cursor_bench_done[bench_type] = 1;

    return 0;
}


struct computing_functions cursor_weak_get_functions()
{
    struct computing_functions s = {
	.init = &weak_init,
	.run = &cursor_weak_run,
	.print_results = &cursor_print_results,
	.print_params = &cursor_print_params,
	.release = &weak_release,
	.parse_arg = &cursor_parse_arg,
	.man = &cursor_man,
	.get_nb_runs = &cursor_get_nb_runs,
	.set_comm_bench_ended_ptr = &cursor_set_comm_bench_ended_ptr,
	.name = "cursor_weak"
    };

    return s;
}
