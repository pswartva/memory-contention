#ifndef __HELPER_CURSOR_H
#define __HELPER_CURSOR_H

#include "helper.h"

extern char cursor_bench_done[3];

extern double cursor_perfs_warmup[3];
extern double cursor_perfs_no_comm[3];
extern double cursor_perfs_comm[3];

extern volatile int* cursor_comm_bench_ended;

extern int cursor;



void cursor_man();
int cursor_get_nb_runs(enum comm_bench_type comm_bench_type);
void cursor_print_results();
int cursor_parse_arg(char* arg);
void cursor_print_params();
void cursor_set_comm_bench_ended_ptr(volatile int* _comm_bench_ended);

#endif /* __HELPER_CURSOR_H */
