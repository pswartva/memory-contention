#include <float.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <omp.h>

#include "stream.h"
#include "timing.h"
#include "helper_stream.h"

#define STREAM_ARRAY_SIZE 100000000  // * 8 Bytes (double) =~ 763 MB


#ifdef ALLOC_STATIC
static ARRAY_TYPE a[STREAM_ARRAY_SIZE], b[STREAM_ARRAY_SIZE], c[STREAM_ARRAY_SIZE];
#else
static ARRAY_TYPE *a, *b, *c;
#endif

int stream_init()
{
    ssize_t j;

#ifndef ALLOC_STATIC
    a = comp_malloc(STREAM_ARRAY_SIZE * sizeof(ARRAY_TYPE));
    b = comp_malloc(STREAM_ARRAY_SIZE * sizeof(ARRAY_TYPE));
    c = comp_malloc(STREAM_ARRAY_SIZE * sizeof(ARRAY_TYPE));
#endif

    #pragma omp parallel for
    for (j = 0; j < STREAM_ARRAY_SIZE; j++)
    {
	a[j] = 1.0;
	b[j] = 2.0;
	c[j] = 0.0;
    }

    return 0;
}

void stream_release()
{
#ifndef ALLOC_STATIC
    comp_free(a, STREAM_ARRAY_SIZE * sizeof(ARRAY_TYPE));
    comp_free(b, STREAM_ARRAY_SIZE * sizeof(ARRAY_TYPE));
    comp_free(c, STREAM_ARRAY_SIZE * sizeof(ARRAY_TYPE));
#endif
}

static void stream_run_kernel(double avgtime[4], double maxtime[4], double mintime[4])
{
    puk_tick_t start_time, end_time;
    double durations[4];
    ARRAY_TYPE scalar = 3.0;

    /* COPY */
    if (bench_to_run[COPY])
    {
	PUK_GET_TICK(start_time);
	#pragma omp parallel for
	for (int j = 0; j < STREAM_ARRAY_SIZE; j++)
	    c[j] = a[j];
	PUK_GET_TICK(end_time);
	durations[COPY] = PUK_TIMING_DELAY(start_time, end_time) / 1000000.0f;
    }

    /* SCALE */
    if (bench_to_run[SCALE])
    {
	PUK_GET_TICK(start_time);
	#pragma omp parallel for
	for (int j=0; j<STREAM_ARRAY_SIZE; j++)
	    b[j] = scalar*c[j];
	PUK_GET_TICK(end_time);
	durations[SCALE] = PUK_TIMING_DELAY(start_time, end_time) / 1000000.0f;
    }

    /* ADD */
    if (bench_to_run[ADD])
    {
	PUK_GET_TICK(start_time);
	#pragma omp parallel for
	for (int j=0; j<STREAM_ARRAY_SIZE; j++)
	    c[j] = a[j]+b[j];
	PUK_GET_TICK(end_time);
	durations[ADD] = PUK_TIMING_DELAY(start_time, end_time) / 1000000.0f;
    }

    /* TRIAD */
    if (bench_to_run[TRIAD])
    {
	PUK_GET_TICK(start_time);
	#pragma omp parallel for
	for (int j=0; j<STREAM_ARRAY_SIZE; j++)
	    a[j] = b[j]+scalar*c[j];
	PUK_GET_TICK(end_time);
	durations[TRIAD] = PUK_TIMING_DELAY(start_time, end_time) / 1000000.0f;
    }

    for (int j = 0; j < 4; j++)
    {
	if (bench_to_run[j])
	{
	    avgtime[j] = avgtime[j] + durations[j];
	    mintime[j] = MIN(mintime[j], durations[j]);
	    maxtime[j] = MAX(maxtime[j], durations[j]);
	}
    }
}


int stream_run(int nb_runs, enum bench_type bench_type)
{
    if (bench_type != WARMUP && stream_bench_done[bench_type])
    {
	printf("Warning: this bench was already done.\n");
    }

    int real_nb_runs = nb_runs;
    double avgtime[4] = {0}, maxtime[4] = {0}, mintime[4] = {FLT_MAX,FLT_MAX,FLT_MAX,FLT_MAX};
    double bytes[4] = {
	2 * sizeof(ARRAY_TYPE) * STREAM_ARRAY_SIZE,
	2 * sizeof(ARRAY_TYPE) * STREAM_ARRAY_SIZE,
	3 * sizeof(ARRAY_TYPE) * STREAM_ARRAY_SIZE,
	3 * sizeof(ARRAY_TYPE) * STREAM_ARRAY_SIZE
    };

    for (int k = 0; k < nb_runs; k++)
    {
	stream_run_kernel(avgtime, maxtime, mintime);
    }

    if (bench_type == WITH_COMM && stream_comm_bench_ended != NULL)
    {
	/* Keep computing while we need more pingpongs: */
	while (!*stream_comm_bench_ended)
	{
	    stream_run_kernel(avgtime, maxtime, mintime);
	    real_nb_runs++;
	}
    }

    if (stream_bench_done[bench_type])
    {
	return 0;
    }

    for (int j = 0; j < 4; j++)
    {
	if (bench_to_run[j])
	{
	    avgtime[j] = avgtime[j]/(double)(real_nb_runs);

	    if (bench_type == WARMUP)
	    {
		stream_perfs_warmup[j][0] = 1.0E-06 * bytes[j] / mintime[j];
		stream_perfs_warmup[j][1] = 1.0E-06 * bytes[j] / avgtime[j];
		stream_perfs_warmup[j][2] = 1.0E-06 * bytes[j] / maxtime[j];

		stream_time_warmup[j][0] = mintime[j];
		stream_time_warmup[j][1] = avgtime[j];
		stream_time_warmup[j][2] = maxtime[j];
	    }
	    else if (bench_type == WITH_COMM)
	    {
		stream_perfs_comm[j][0] = 1.0E-06 * bytes[j] / mintime[j];
		stream_perfs_comm[j][1] = 1.0E-06 * bytes[j] / avgtime[j];
		stream_perfs_comm[j][2] = 1.0E-06 * bytes[j] / maxtime[j];

		stream_time_comm[j][0] = mintime[j];
		stream_time_comm[j][1] = avgtime[j];
		stream_time_comm[j][2] = maxtime[j];
	    }
	    else if (bench_type == WITHOUT_COMM)
	    {
		stream_perfs_no_comm[j][0] = 1.0E-06 * bytes[j] / mintime[j];
		stream_perfs_no_comm[j][1] = 1.0E-06 * bytes[j] / avgtime[j];
		stream_perfs_no_comm[j][2] = 1.0E-06 * bytes[j] / maxtime[j];

		stream_time_no_comm[j][0] = mintime[j];
		stream_time_no_comm[j][1] = avgtime[j];
		stream_time_no_comm[j][2] = maxtime[j];
	    }
	    else
	    {
		abort();
	    }
	}
    }

    stream_bench_done[bench_type] = 1;

    return 0;
}

struct computing_functions stream_get_functions()
{
    struct computing_functions s = {
	.init = &stream_init,
	.run = &stream_run,
	.print_results = &stream_print_results,
	.print_params = &stream_print_params,
	.release = &stream_release,
	.parse_arg = &stream_parse_arg,
	.man = &stream_man,
	.get_nb_runs = &stream_get_nb_runs,
	.set_comm_bench_ended_ptr = &stream_set_comm_bench_ended_ptr,
	.name = "stream"
    };

    return s;
}
