#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <mpi.h>

#include "cli.h"


#define NB_COMP_SECOND_WARMUP 4


static int rank = 0, other_rank = 1;
static pthread_barrier_t thread_barrier;
static int nb_comm_todo;
static double med_comm_duration, seq_comm_time, parallel_comm_time;
struct params_s params;
struct machine_s machine;


static void* ping_pong_thread_func(void* arg)
{
    int i = 0;
    time_type start_time, end_time;

#if WITH_STARPU == 1
    starpu_pause();
#endif

    bind_thread(machine, params.ping_thread_pu, "PingPong");

    if (rank == 0 && params.display_binding)
    {
	hwloc_display_binding();
    }

    ping_pong_init(rank, params.pingpong_type, params.pingpong_size);

    /**** Communication warmup, to register buffers for rcache */
    PRINTF_RANK0("# Communication warmup...\n");

    int nb_comm_warmup = COMM_BUFFER_POOL_SIZE * 2;
    for (int i = 0; i < nb_comm_warmup; i++)
    {
	ping_pong(params.comm_duplex); // register buffers for rcache
    }

    double* durations = malloc(nb_comm_warmup * sizeof(double)); // get communication time
    for (int i = 0; i < nb_comm_warmup; i++)
    {
	durations[i] = ping_pong(params.comm_duplex);
    }

    qsort(durations, nb_comm_warmup, sizeof(double), &comp_double);
    med_comm_duration = 2 * durations[(int) floor(nb_comm_warmup / 2)]; // we want the *pingpong* duration, not just the ping

    double warmup_bw = (sizeof(float) / (1024*1024*(med_comm_duration / 1000000))) * params.pingpong_size;
    PRINTF_RANK0("# Communication warmup (%d iterations): med duration (full round-trip): %.3lf ms - med bandwidth: %.3lf MB/s\n",
	    nb_comm_warmup, (med_comm_duration / 1000), warmup_bw);
    free(durations);
    /*** End of communication warmup */

    pthread_barrier_wait(&thread_barrier); // confirm to main thread this thread is launched

    // Sequential
    pthread_barrier_wait(&thread_barrier); // wait for starting pingpongs for the sequential part
    timing_now(start_time);
    for (i = 0; i < nb_comm_todo; i++)
    {
	ping_pong(params.comm_duplex);
    }
    timing_now(end_time);
    pthread_barrier_wait(&thread_barrier); // notifiy main thread pingpongs are done
    seq_comm_time = TIME_DIFF(start_time, end_time);
#if WITH_STARPU == 1
    starpu_resume(); // let computing benchmarks manage their StarPU's state
#endif


    // Parallel
    pthread_barrier_wait(&thread_barrier); // wait for starting pingpongs for the parallel part
    timing_now(start_time);
    for (i = 0; i < nb_comm_todo; i++)
    {
	ping_pong(params.comm_duplex);
    }
    timing_now(end_time);
    pthread_barrier_wait(&thread_barrier); // notifiy main thread pingpongs are done
    parallel_comm_time = TIME_DIFF(start_time, end_time);

    return NULL;
}


int main(int argc, char* argv[])
{
    int ret = 0;
    int worldsize;
    double computing_duration = 0, seq_time, parallel_time, seq_comp_time, parallel_comp_time;
    time_type start_time, end_time, end_time_bis;

    init_params();
    disable_param(DISABLE_COMM);
    disable_param(HIDE_COMMS);
    disable_param(NO_WARMUP);
    disable_param(NO_ALONE);
    parse_args(argc, argv, &params);

#if WITH_STARPU == 1
    ret = starpu_mpi_init_conf(&argc, &argv, /* let StarPU initialize MPI */ 1, MPI_COMM_WORLD, NULL);
    if (ret != 0)
    {
	printf("Error in initialization of StarPU\n");
	return EXIT_FAILURE;
    }

    starpu_mpi_comm_rank(MPI_COMM_WORLD, &rank);
    starpu_mpi_comm_size(MPI_COMM_WORLD, &worldsize);
    machine.topology = starpu_get_hwloc_topology();

    PRINTF_RANK0("# StarPU version\n");
#elif defined(_OPENMP)
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &worldsize);

    hwloc_topology_init(&machine.topology);
    hwloc_topology_load(machine.topology);

    PRINTF_RANK0("# OpenMP version\n");
#endif

    fill_machine(&machine);
    init_malloc(&machine, params.memory_comp_numa_nodes, params.memory_comp_numa_nodes_nb, params.memory_comm_numa_node);

    if (params.display_help)
    {
	if (rank == 0)
	{
	    print_help(params);
	}

	runtime_shutdown(machine.topology, /* enabled comms */ 1);
	return EXIT_FAILURE;
    }

    if (rank == 0)
    {
	print_params(params);
    }

    if (worldsize != 2)
    {
	fprintf(stderr, "We need two MPI processes.\n");
	runtime_shutdown(machine.topology, /* enabled comms */ 1);
	return EXIT_FAILURE;
    }

    other_rank = (rank == 1) ? 0 : 1;

    ping_thread_location_to_pu(&params, &machine);
    if (rank == 0)
    {
	print_machine(&machine, &params);
    }

    PRINTF_RANK0("# Will do pingpongs moving %d floats (%lu KB)\n", params.pingpong_size, params.pingpong_size * sizeof(float) / 1024);

    int nb_computing_run = params.computing.get_nb_runs(params.comm_bench_type);

    /*** Launch thread which will do ping pongs */
    pthread_t ping_pong_thread;
    pthread_barrier_init(&thread_barrier, NULL, 2);
    pthread_create(&ping_pong_thread, NULL, ping_pong_thread_func, NULL);
    pthread_barrier_wait(&thread_barrier); // thread is well launched


    /*** Computing init and warmup */
    PRINTF_RANK0("# Computing init...\n");
    if (RANK_DOES_COMPUTATIONS)
    {
	if (params.computing.init != NULL)
	{
	    ret = params.computing.init();
	    fflush(stdout);
	    if (ret != 0)
	    {
		printf("Error in computing.init()\n");
		runtime_shutdown(machine.topology, /* enabled comms */ 1);
		return EXIT_FAILURE;
	    }
	}

	ret = params.computing.run(3, WARMUP);
	if (ret != 0)
	{
	    printf("Error in computing.run()\n");
	    runtime_shutdown(machine.topology, /* enabled comms */ 1);
	    return EXIT_FAILURE;
	}

	timing_now(start_time);
	ret = params.computing.run(NB_COMP_SECOND_WARMUP, WARMUP);
	if (ret != 0)
	{
	    printf("Error in computing.run()\n");
	    runtime_shutdown(machine.topology, /* enabled comms */ 1);
	    return EXIT_FAILURE;
	}
	timing_now(end_time);
	computing_duration = TIME_DIFF(start_time, end_time) / NB_COMP_SECOND_WARMUP;
    }
    if (rank == 0)
    {
	assert(computing_duration > EPSILON);
	nb_comm_todo = (computing_duration / med_comm_duration) * nb_computing_run;
	printf("# Will do %d pingpongs\n", nb_comm_todo);
	MPI_Send(&nb_comm_todo, 1, MPI_INT, other_rank, 3, MPI_COMM_WORLD);
    }
    else
    {
	MPI_Recv(&nb_comm_todo, 1, MPI_INT, other_rank, 3, MPI_COMM_WORLD, NULL);
    }
    /*** End of computing init and warmup */


    /*** Sequential */
    PRINTF_RANK0("# Sequential...\n");

    timing_now(start_time);
    if (RANK_DOES_COMPUTATIONS)
    {
	params.computing.run(nb_computing_run, WITHOUT_COMM);
    }
    timing_now(end_time);
    pthread_barrier_wait(&thread_barrier); // start pingpongs
    pthread_barrier_wait(&thread_barrier); // wait for the end of pingpongs
    timing_now(end_time_bis);
    seq_time = TIME_DIFF(start_time, end_time_bis);
    seq_comp_time = TIME_DIFF(start_time, end_time);
    seq_comm_time = TIME_DIFF(end_time, end_time_bis);
    /*** End of sequential part */


    /*** Parallel */
    PRINTF_RANK0("# Parallel...\n");

    timing_now(start_time);
    pthread_barrier_wait(&thread_barrier); // start pingpongs
    if (RANK_DOES_COMPUTATIONS)
    {
	params.computing.run(nb_computing_run, WITH_COMM);
    }
    timing_now(end_time);
    pthread_barrier_wait(&thread_barrier); // wait for the end of pingpongs
    timing_now(end_time_bis);
    parallel_time = TIME_DIFF(start_time, end_time_bis);
    parallel_comp_time = TIME_DIFF(start_time, end_time);
    /*** End of parallel part */

    pthread_join(ping_pong_thread, NULL);
    pthread_barrier_destroy(&thread_barrier);

    if (rank == 0)
    {
	printf("Sequential: %.2lf s (computations: %.2lf s, communications: %.2lf s)\n", seq_time / 1e6, seq_comp_time / 1e6, seq_comm_time / 1e6);
	printf("Parallel:   %.2lf s (computations: %.2lf s, communications: %.2lf s)\n", parallel_time / 1e6, parallel_comp_time / 1e6, parallel_comm_time / 1e6);
	fflush(stdout);
    }

    if (RANK_DOES_COMPUTATIONS && params.computing.release != NULL)
    {
	params.computing.release();
    }


    MPI_Barrier(MPI_COMM_WORLD);
    ping_pong_free();
    runtime_shutdown(machine.topology, /* comm enabled */ 1);

    return EXIT_SUCCESS;
}
