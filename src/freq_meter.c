#define _GNU_SOURCE // for pthread_setname_np
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#if WITH_LIKWID == 1
#include <likwid.h>
#endif

#include "freq_meter.h"

#define PROBE_INTERVAL 50000 // us = 50 ms

static int nb_pus;
static pthread_t freq_thread;
static volatile int shutdown_thread;
static hwloc_topology_t hwloc_topology;
static FILE* out_file;
static pthread_barrier_t barrier;
static pthread_mutex_t file_mutex;
static int logical2os[64];

#if WITH_LIKWID == 1
static CpuTopology_t likwid_topo;


static inline void freq_meter_print()
{
    pthread_mutex_lock(&file_mutex);

    fprintf(out_file, "[freq][values] ");
    for (int c = 0; c < nb_pus; c++)
    {
	fprintf(out_file, "%ld,", freq_getCpuClockCurrent(logical2os[c]) / 1000);
    }
    for (int s = 0; s < likwid_topo->numSockets; s++)
    {
	fprintf(out_file, "%lu", freq_getUncoreFreqCur(s));
	if (s < likwid_topo->numSockets-1)
	{
	    fprintf(out_file, ",");
	}
    }
    fprintf(out_file, "\n");

#if 0
    printf("\nCPU temperatures: ");
    unsigned temp;
    for (int c = 0; c < nb_pus; c++)
    {
	thermal_read(c, &temp);
	printf("%u ", temp);
    }
#endif

    pthread_mutex_unlock(&file_mutex);
}

#elif WITH_DIRECT_CPU_FREQ == 1

static char cur_freq_file[64][255];

static int get_cpu_freq_core(int core_os_index)
{
    FILE* freq_file = fopen(cur_freq_file[core_os_index], "r");
    if (freq_file == NULL)
    {
        return -1;
    }

    size_t n;
    char buff[8];
    int cpu_freq = -1;

    if ((n = fread(buff, 1, sizeof(buff)-1, freq_file)) <= 0)
        return -1;

    buff[n] = '\0';
    if (sscanf(buff, "%d", &cpu_freq) != 1)
        return -1;

    fclose(freq_file);

    return cpu_freq;
}

void freq_meter_print()
{
    pthread_mutex_lock(&file_mutex);

    fprintf(out_file, "[freq][values] ");
    for (int c = 0; c < nb_pus; c++)
    {
	fprintf(out_file, "%d", get_cpu_freq_core(c));
	if (c < nb_pus-1)
	{
	    fprintf(out_file, ",");
	}
    }
    fprintf(out_file, "\n");

    pthread_mutex_unlock(&file_mutex);
}
#endif

static void* thread_func(void* arg)
{
    pthread_setname_np(pthread_self(), "FreqMeter");

    int topo_pu_depth = hwloc_get_type_depth(hwloc_topology, HWLOC_OBJ_CORE);
    hwloc_obj_t pu = hwloc_get_obj_by_depth(hwloc_topology, topo_pu_depth, nb_pus-1);
    hwloc_cpuset_t cpuset = hwloc_bitmap_dup(pu->cpuset);
    hwloc_bitmap_singlify(cpuset);
    hwloc_set_cpubind(hwloc_topology, cpuset, HWLOC_CPUBIND_THREAD);
    hwloc_bitmap_free(cpuset);

    for (int i = 0; i < nb_pus; i++)
    {
	logical2os[i] = hwloc_get_obj_by_type(hwloc_topology, HWLOC_OBJ_CORE, i)->os_index;
    }

    out_file = fopen("freq.out", "w");
    if (out_file == NULL)
    {
	printf("Error while opening freq file\n");
	fflush(stdout);
	abort();
    }

    pthread_mutex_lock(&file_mutex);

    fprintf(out_file, "[freq][header] ");
    for (int c = 0; c < nb_pus; c++)
    {
	fprintf(out_file, "Core %d", c);
	if (c < nb_pus-1)
	{
	    fprintf(out_file, ",");
	}
    }

#if WITH_LIKWID == 1
    topology_init();
    likwid_topo = get_cpuTopology();
    CpuInfo_t info = get_cpuInfo();

#if 0
    for (int c = 0; c < nb_pus; c++)
    {
	thermal_init(c);
    }
#endif

    fprintf(out_file, ",");
    for (int s = 0; s < likwid_topo->numSockets; s++)
    {
	fprintf(out_file, "Uncore %d", s);
	if (s < likwid_topo->numSockets-1)
	{
	    fprintf(out_file, ",");
	}
    }

    fprintf(out_file, "\n");

    fprintf(out_file, "[freq][probe_interval] %d\n", PROBE_INTERVAL);

    pthread_mutex_unlock(&file_mutex);

    printf("# %s with %d sockets and %d CPUs\n", info->name, likwid_topo->numSockets, likwid_topo->numHWThreads);
    printf("# CPU frequencies between %lu and %lu MHz\n", freq_getCpuClockMin(0) / 1000, freq_getCpuClockMax(0) / 1000);
    printf("# Uncore frequency between %lu and %lu MHz\n", freq_getUncoreFreqMin(0), freq_getUncoreFreqMax(0));
    printf("# Turbo mode is %s\n", freq_getTurbo(0) ? "ON" : "OFF");

#elif WITH_DIRECT_CPU_FREQ == 1
    assert(nb_pus < 64);

    for (int c = 0; c < nb_pus; c++)
    {
	sprintf(cur_freq_file[c], "/sys/devices/system/cpu/cpu%d/cpufreq/scaling_cur_freq", c);
    }

    fprintf(out_file, "\n");
#endif

    /* barrier to return the main thread only when the file is opened and initialized */
    pthread_barrier_wait(&barrier);

    while (!shutdown_thread)
    {
	freq_meter_print();
	usleep(PROBE_INTERVAL);
    }
    freq_meter_print();

#if WITH_LIKWID == 1
    topology_finalize();
#endif

    return NULL;
}

void freq_meter_init(int _nb_pus, hwloc_topology_t _topology)
{
    nb_pus = _nb_pus;
    shutdown_thread = 0;
    hwloc_topology = _topology;

    pthread_barrier_init(&barrier, NULL, 2);
    pthread_mutex_init(&file_mutex, NULL);

    pthread_create(&freq_thread, NULL, thread_func, NULL);

    pthread_barrier_wait(&barrier);
}

void freq_meter_release()
{
    shutdown_thread = 1;
    pthread_join(freq_thread, NULL);
    pthread_barrier_destroy(&barrier);
    pthread_mutex_destroy(&file_mutex);
}

void freq_meter_event(const char* str)
{
    pthread_mutex_lock(&file_mutex);
    fprintf(out_file, "[freq][event] %s\n", str);
    pthread_mutex_unlock(&file_mutex);
}

void freq_meter_set_bind_str(char* bind_str[64])
{
    pthread_mutex_lock(&file_mutex);
    fprintf(out_file, "[freq][binding] ");
    for (int c = 0; c < nb_pus; c++)
    {
	if (bind_str[c] != NULL)
	{
	    fprintf(out_file, "%s", bind_str[c]);
	}
	if (c < nb_pus-1)
	{
	    fprintf(out_file, ",");
	}
    }
    fprintf(out_file, "\n");
    pthread_mutex_unlock(&file_mutex);
}

