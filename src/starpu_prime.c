#include <float.h>
#include <stdlib.h>
#include <stdio.h>
#include <starpu.h>

#include "helper.h"
#include "prime.h"


// array to be accessed with bench_type enum to know if we already launched this bench
// to prevent erasing old data bench
static char bench_done[] = {0, 0, 0};

static unsigned nb_workers;

static double prime_perfs_warmup[] = {FLT_MAX, 0, 0};
static double prime_perfs_no_comm[] = {FLT_MAX, 0, 0};
static double prime_perfs_comm[] = {FLT_MAX, 0, 0};

static volatile int* comm_bench_ended = NULL;


void prime_set_comm_bench_ended_ptr(volatile int* _comm_bench_ended)
{
    comm_bench_ended = _comm_bench_ended;
}


static void prime_number_task(void *buffers[], void *cl_arg)
{
    nb_prime_numbers();
}

static struct starpu_codelet prime_number_cl =
{
    .cpu_funcs = {prime_number_task},
    .cpu_funcs_name = {"prime_number_task"},
    .name = "prime_number",
    .color = 0x800000 // maroon
};


int prime_init()
{
    nb_workers = starpu_worker_get_count();

    return 0;
}

static double prime_run_kernel()
{
    int ret = 0;
    double start_time, end_time;

    starpu_pause();

    for (int i = 0; i < nb_workers; i++)
    {
	struct starpu_task* task = starpu_task_create();
	task->cl = &prime_number_cl;

	ret = starpu_task_submit(task);
	if (ret != 0)
	{
	    printf("Error on starpu_task_submit()\n");
	    abort();
	}
    }

    start_time = starpu_timing_now();
    starpu_resume();
    starpu_task_wait_for_all();
    end_time = starpu_timing_now();

    return end_time - start_time;
}



int prime_run(int nb_runs, enum bench_type bench_type)
{
    if (bench_type != WARMUP && bench_done[bench_type])
    {
	printf("Warning: this bench was already done.\n");
    }

    double avgtime = 0, maxtime = 0, mintime = FLT_MAX;
    int real_nb_runs = nb_runs;
    double duration;

    for (int k = 0; k < nb_runs; k++)
    {
	duration = prime_run_kernel();

	mintime = MIN(mintime, duration);
	avgtime += duration;
	maxtime = MAX(maxtime, duration);
    }

    if (bench_type == WITH_COMM && comm_bench_ended != NULL)
    {
	/* Keep computing while we need more pingpongs: */
	while (!*comm_bench_ended)
	{
	    duration = prime_run_kernel();

	    mintime = MIN(mintime, duration);
	    avgtime += duration;
	    maxtime = MAX(maxtime, duration);

	    real_nb_runs++;
	}
    }

    if (bench_done[bench_type])
    {
	return 0;
    }

    if (bench_type == WARMUP)
    {
	prime_perfs_warmup[0] = mintime;
	prime_perfs_warmup[1] = avgtime / (double) (real_nb_runs);
	prime_perfs_warmup[2] = maxtime;
    }
    else if (bench_type == WITH_COMM)
    {
	prime_perfs_comm[0] = mintime;
	prime_perfs_comm[1] = avgtime / (double) (real_nb_runs);
	prime_perfs_comm[2] = maxtime;
    }
    else if (bench_type == WITHOUT_COMM)
    {
	prime_perfs_no_comm[0] = mintime;
	prime_perfs_no_comm[1] = avgtime / (double) (real_nb_runs);
	prime_perfs_no_comm[2] = maxtime;
    }
    else
    {
	abort();
    }

    bench_done[bench_type] = 1;

    return 0;
}

void prime_print_results()
{
    printf("# PRIME results: Time ms (min, avg, max)\n");
    if (bench_done[WARMUP])
    {
	printf("# warmup                 %.5f\t%.5f\t%.5f\n", prime_perfs_warmup[0] / 1000.0f, prime_perfs_warmup[1] / 1000.0f, prime_perfs_warmup[2] / 1000.0f);
    }
    if (bench_done[WITH_COMM])
    {
	printf("# with communications    %.5f\t%.5f\t%.5f\n", prime_perfs_comm[0] / 1000.0f, prime_perfs_comm[1] / 1000.0f, prime_perfs_comm[2] / 1000.0f);
    }
    if (bench_done[WITHOUT_COMM])
    {
	printf("# without communications %.5f\t%.5f\t%.5f\n", prime_perfs_no_comm[0] / 1000.0f, prime_perfs_no_comm[1] / 1000.0f, prime_perfs_no_comm[2] / 1000.0f);
    }
}

int prime_get_nb_runs(enum comm_bench_type comm_bench_type)
{
    return 10;
}

struct computing_functions prime_get_functions()
{
    struct computing_functions s = {
	.init = &prime_init,
	.run = &prime_run,
	.print_results = &prime_print_results,
	.print_params = NULL,
	.release = NULL,
	.parse_arg = NULL,
	.man = NULL,
	.get_nb_runs = &prime_get_nb_runs,
	.set_comm_bench_ended_ptr = &prime_set_comm_bench_ended_ptr,
	.name = "prime"
    };

    return s;
}
