#ifndef __HELPER_H
#define __HELPER_H

#include <hwloc.h>

#include "config.h"
#include "malloc.h"

#if HWLOC_API_VERSION < 0x00010b00
#define HWLOC_OBJ_NUMANODE HWLOC_OBJ_NODE
#endif

#define COMM_LATENCY_BUFFER_SIZE 1 // 1 float, so 4 Bytes, to really measure the latency
#define COMM_BANDWIDTH_BUFFER_SIZE 16777216 // 64 MB is enough to see impact on network bandwidth

#define COMM_BUFFER_POOL_SIZE 10

#define EPSILON 1e-6

#define MIN(x,y) ((x)<=(y)?(x):(y))
#define MAX(x,y) ((x)>=(y)?(x):(y))

#define PRINTF_RANK0(msg, ...) do { if (rank == 0) { printf(msg, ## __VA_ARGS__); fflush(stdout); } } while (0)

#define COMP_TIME_FORMAT "%6.1f"
#define COMP_BW_FORMAT "%8.1f"

#define ARRAY_TYPE double

enum bench_type {
    WARMUP = 0,
    WITH_COMM = 1,
    WITHOUT_COMM = 2
};

enum comm_bench_type {
    LATENCY,
    BANDWIDTH
};

enum pingpong_type_e {
    PING_PONG,
    PING,
    PONG
};

enum stream_bench_type {
    COPY = 0,
    SCALE = 1,
    ADD = 2,
    TRIAD = 3
};

struct computing_functions {
    int (*init)(void);
    int (*run)(int, enum bench_type);
    void (*print_results)(void);
    void (*print_params)(void);
    void (*release)(void);
    int (*parse_arg)(char*);
    void (*man)(void);
    int (*get_nb_runs)(enum comm_bench_type);
    void (*set_comm_bench_ended_ptr)(volatile int*);
    char* name;
};

struct machine_s {
    hwloc_topology_t topology;
    int nb_pus;
    int topo_pu_depth;
    uint64_t l3_size;
    int nb_workers;
    int nb_numa;
    int nb_sockets;
};


int comp_double(const void* _a, const void* _b);

void runtime_shutdown(hwloc_topology_t topology, int comm_enabled);

double ping_pong(int comm_duplex);
void ping_pong_init(int _rank, enum pingpong_type_e _pingpong_type, int _buffer_size);
void ping_pong_free();

void get_worker_binding(hwloc_topology_t topo, char* binding_str[64]);
void get_worker_binding_ids(hwloc_topology_t topo, int nb_threads, int* thread_binding);
void hwloc_display_binding();
void bind_thread(struct machine_s machine, int thread_pu, char* name);
void fill_machine(struct machine_s* machine);

#if defined(_OPENMP)
int get_nb_openmp_threads();

struct computing_functions memset_get_functions();
#endif

#if WITH_STARPU == 1
#include <starpu.h>
#include <starpu_mpi.h>

#define init_malloc(_a, _b, _c, _d)
#define get_worker_count starpu_worker_get_count
#define timing_now(var) var = starpu_timing_now()
#define time_type double
#define TIME_DIFF(__start, __end) (__end - __start)

#ifndef HAVE_STARPU_FREE_NOFLAG
#define starpu_free_noflag(_a, _b) starpu_free(_a)
#endif

#elif defined(_OPENMP)
#include "timing.h"
#define get_worker_count get_nb_openmp_threads
#define timing_now(__var) PUK_GET_TICK(__var)
#define time_type puk_tick_t
#define TIME_DIFF(__start, __end) PUK_TIMING_DELAY(__start, __end)
#endif

#endif /* __HELPER_H */
