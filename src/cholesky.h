#ifndef __CHOLESKY_H
#define __CHOLESKY_H

#include "helper.h"

int cholesky_init();
int cholesky_run(int nb_runs, enum bench_type bench_type);
void cholesky_release();
void cholesky_print_results();
struct computing_functions cholesky_get_functions();

#endif // __CHOLESKY_H
