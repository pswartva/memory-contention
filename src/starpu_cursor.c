#include <float.h>
#include <stdlib.h>
#include <stdio.h>
#include <starpu.h>
#include <hwloc.h>

#include "cursor.h"
#include "helper_cursor.h"


extern struct machine_s machine;
static unsigned nb_workers;

static ARRAY_TYPE *a, *b, *c;
static starpu_data_handle_t handle_a, handle_b, handle_c;

/* We provide a default value, but the value will be set according to the
 * number of workers and the size of the L3 cache */
static unsigned stream_array_size = 235929600; // * 8 Bytes (double) =~ 1800 MB


static void init_task(void *buffers[], void *cl_arg)
{
    unsigned n = STARPU_VECTOR_GET_NX(buffers[0]);
    ARRAY_TYPE* a = (ARRAY_TYPE*) STARPU_VECTOR_GET_PTR(buffers[0]);
    ARRAY_TYPE* b = (ARRAY_TYPE*) STARPU_VECTOR_GET_PTR(buffers[1]);
    ARRAY_TYPE* c = (ARRAY_TYPE*) STARPU_VECTOR_GET_PTR(buffers[2]);

    for (unsigned i = 0; i < n; i++)
    {
	a[i] = 2.7;
	b[i] = 0.0;
	c[i] = 3.14;
    }
}

static void cursor_task(void *buffers[], void *cl_arg)
{
    unsigned n = STARPU_VECTOR_GET_NX(buffers[0]);
    ARRAY_TYPE* a = (ARRAY_TYPE*) STARPU_VECTOR_GET_PTR(buffers[0]);
    ARRAY_TYPE* b = (ARRAY_TYPE*) STARPU_VECTOR_GET_PTR(buffers[1]);
    ARRAY_TYPE* c = (ARRAY_TYPE*) STARPU_VECTOR_GET_PTR(buffers[2]);

    for (unsigned i = 0; i < n; i++)
    {
	for (int j = 0; j < cursor + 1; j++)
	{
	    b[i] += (a[i] + j * c[i]);
	}
    }
}

static struct starpu_codelet init_cl =
{
    .cpu_funcs = {init_task},
    .cpu_funcs_name = {"init_task"},
    .nbuffers = 3,
    .modes = {STARPU_W, STARPU_W, STARPU_W},
    .name = "init_arrays",
    .color = 0x800000 // maroon
};

static struct starpu_codelet cursor_cl =
{
    .cpu_funcs = {cursor_task},
    .cpu_funcs_name = {"cursor_task"},
    .nbuffers = 3,
    .modes = {STARPU_R, STARPU_W, STARPU_R},
    .name = "cursor",
    .color = 0x008000 // green
};


int cursor_init()
{
    int ret = 0;
    nb_workers = starpu_worker_get_count();

    stream_array_size = machine.l3_size * 2 * nb_workers;

    starpu_malloc((void**) &a, stream_array_size * sizeof(ARRAY_TYPE));
    starpu_malloc((void**) &b, stream_array_size * sizeof(ARRAY_TYPE));
    starpu_malloc((void**) &c, stream_array_size * sizeof(ARRAY_TYPE));

    starpu_vector_data_register(&handle_a, STARPU_MAIN_RAM, (uintptr_t) a, stream_array_size, sizeof(ARRAY_TYPE));
    starpu_vector_data_register(&handle_b, STARPU_MAIN_RAM, (uintptr_t) b, stream_array_size, sizeof(ARRAY_TYPE));
    starpu_vector_data_register(&handle_c, STARPU_MAIN_RAM, (uintptr_t) c, stream_array_size, sizeof(ARRAY_TYPE));

    struct starpu_data_filter f = {
	.filter_func = starpu_vector_filter_block,
	.nchildren = nb_workers
    };
    starpu_data_partition(handle_a, &f);
    starpu_data_partition(handle_b, &f);
    starpu_data_partition(handle_c, &f);


    /* Initialize arrays */
    for (int i = 0; i < starpu_data_get_nb_children(handle_a); i++)
    {
	starpu_data_handle_t subhandle_a = starpu_data_get_sub_data(handle_a, 1, i);
	starpu_data_handle_t subhandle_b = starpu_data_get_sub_data(handle_b, 1, i);
	starpu_data_handle_t subhandle_c = starpu_data_get_sub_data(handle_c, 1, i);

	struct starpu_task* task = starpu_task_create();
	task->handles[0] = subhandle_a;
	task->handles[1] = subhandle_b;
	task->handles[2] = subhandle_c;
	task->cl = &init_cl;

	ret = starpu_task_submit(task);
	if (ret != 0)
	{
	    printf("Error on starpu_task_submit()\n");
	    return ret;
	}
    }

    starpu_task_wait_for_all();

    return ret;
}

static double cursor_run_kernel()
{
    int ret;
    double start_time, end_time;

    starpu_pause();

    for (int i = 0; i < starpu_data_get_nb_children(handle_a); i++)
    {
	starpu_data_handle_t subhandle_a = starpu_data_get_sub_data(handle_a, 1, i);
	starpu_data_handle_t subhandle_b = starpu_data_get_sub_data(handle_b, 1, i);
	starpu_data_handle_t subhandle_c = starpu_data_get_sub_data(handle_c, 1, i);

	struct starpu_task* task = starpu_task_create();
	task->handles[0] = subhandle_a;
	task->handles[1] = subhandle_b;
	task->handles[2] = subhandle_c;
	task->cl = &cursor_cl;

	ret = starpu_task_submit(task);
	if (ret != 0)
	{
	    printf("Error on starpu_task_submit()\n");
	    abort();
	}
    }

    start_time = starpu_timing_now();
    starpu_resume();
    starpu_task_wait_for_all();
    end_time = starpu_timing_now();

    return end_time - start_time;
}

int cursor_run(int nb_runs, enum bench_type bench_type)
{
    if (bench_type != WARMUP && cursor_bench_done[bench_type])
    {
	printf("Warning: this bench was already done.\n");
    }

    double avgtime = 0, maxtime = 0, mintime = FLT_MAX;
    int real_nb_runs = nb_runs;
    double duration;

    for (int k = 0; k < nb_runs; k++)
    {
	duration = cursor_run_kernel();

	mintime = MIN(mintime, duration);
	avgtime += duration;
	maxtime = MAX(maxtime, duration);
    }

    if (bench_type == WITH_COMM && cursor_comm_bench_ended != NULL)
    {
	/* Keep computing while we need more pingpongs: */
	while (!*cursor_comm_bench_ended)
	{
	    duration = cursor_run_kernel();

	    mintime = MIN(mintime, duration);
	    avgtime += duration;
	    maxtime = MAX(maxtime, duration);

	    real_nb_runs++;
	}
    }

    if (cursor_bench_done[bench_type])
    {
	return 0;
    }

    if (bench_type == WARMUP)
    {
	cursor_perfs_warmup[0] = mintime;
	cursor_perfs_warmup[1] = avgtime / (double) (real_nb_runs);
	cursor_perfs_warmup[2] = maxtime;
    }
    else if (bench_type == WITH_COMM)
    {
	cursor_perfs_comm[0] = mintime;
	cursor_perfs_comm[1] = avgtime / (double) (real_nb_runs);
	cursor_perfs_comm[2] = maxtime;
    }
    else if (bench_type == WITHOUT_COMM)
    {
	cursor_perfs_no_comm[0] = mintime;
	cursor_perfs_no_comm[1] = avgtime / (double) (real_nb_runs);
	cursor_perfs_no_comm[2] = maxtime;
    }
    else
    {
	abort();
    }

    cursor_bench_done[bench_type] = 1;

    return 0;
}

void cursor_release()
{
    starpu_data_unpartition(handle_a, STARPU_MAIN_RAM);
    starpu_data_unpartition(handle_b, STARPU_MAIN_RAM);
    starpu_data_unpartition(handle_c, STARPU_MAIN_RAM);
    starpu_data_unregister(handle_a);
    starpu_data_unregister(handle_b);
    starpu_data_unregister(handle_c);

    starpu_free_noflag(a, stream_array_size * sizeof(ARRAY_TYPE));
    starpu_free_noflag(b, stream_array_size * sizeof(ARRAY_TYPE));
    starpu_free_noflag(c, stream_array_size * sizeof(ARRAY_TYPE));
}


struct computing_functions cursor_get_functions()
{
    struct computing_functions s = {
	.init = &cursor_init,
	.run = &cursor_run,
	.print_results = &cursor_print_results,
	.print_params = &cursor_print_params,
	.release = &cursor_release,
	.parse_arg = &cursor_parse_arg,
	.man = &cursor_man,
	.get_nb_runs = &cursor_get_nb_runs,
	.set_comm_bench_ended_ptr = &cursor_set_comm_bench_ended_ptr,
	.name = "cursor"
    };

    return s;
}
