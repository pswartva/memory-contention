#ifndef __SCALAR_AVX_H
#define __SCALAR_AVX_H

#include "helper.h"

struct computing_functions scalar_avx_get_functions();

#endif // __SCALAR_AVX_H
