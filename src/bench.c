#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <mpi.h>

#include "cli.h"

#if (WITH_LIKWID == 1) || (WITH_DIRECT_CPU_FREQ == 1)
#define FREQ_MONITORING
#endif

#ifdef FREQ_MONITORING
#include "freq_meter.h"
#define FREQ_EVENT(event) if (params.measure_freq) { freq_meter_event(event); }
#else
#define FREQ_EVENT(event)
#endif


#define SLEEP(__seconds)  do { if (params.debug) printf("# will sleep %d seconds\n", (__seconds)); sleep(__seconds); if (params.debug) printf("# end of sleep\n"); } while (0)

#define NB_PINGPONG_LATENCY 20000
#define NB_PINGPONG_BANDWIDTH 50


static int rank = 0, other_rank = 1;
static pthread_barrier_t thread_barrier;
static volatile int compute_bench_ended = 0;
static volatile int comm_bench_ended = 0;
struct params_s params;
struct machine_s machine;


static inline int get_min_comm_todo()
{
    int nb = NB_PINGPONG_LATENCY;
    int i = 1;
    int d;

    while (i <= params.pingpong_size)
    {
        d = nb * i;
        if (d > COMM_BANDWIDTH_BUFFER_SIZE)
	{
	    nb = COMM_BANDWIDTH_BUFFER_SIZE / i;
	    if (nb < NB_PINGPONG_BANDWIDTH)
	    {
		nb = NB_PINGPONG_BANDWIDTH;
	    }
	}
        i *= 2;
    }

    return nb;
}

static void* ping_pong_thread_func(void* arg)
{
    int real_nb_comm_todo;
    int min_comm_todo = get_min_comm_todo();

    int comm_durations_size = min_comm_todo;
    double* comm_durations = malloc(comm_durations_size * sizeof(double));

    bind_thread(machine, params.ping_thread_pu, "PingPong");

    if (rank == 0 && params.display_binding)
    {
	hwloc_display_binding();
    }

    pthread_barrier_wait(&thread_barrier);

    int k = !(params.do_alone & DO_COMM_ALONE);
    /* k = 0: comm without computation
     * k = 1: comm with computation */
    for (; k < 2; k++)
    {
	if (k == 0) // without computing
	{
	    FREQ_EVENT("Comm without computing start");
#if WITH_STARPU == 1
	    starpu_pause(); // we pause StarPU's workers to avoid their impact on communications
#endif
	    real_nb_comm_todo = min_comm_todo;
	}
	else // with computing
	{
	    /* ping pongs will be stopped at the end of the computing benchmark */
	    real_nb_comm_todo = INT_MAX;
	}

	int i = 0;
	for (i = 0; i < real_nb_comm_todo; i++)
	{
	    if (k == 1)
	    {
		if (i == min_comm_todo)
		{
		    comm_bench_ended = 1; // we made enough comm, so say it to computing thread
		}

		int end = 0;
		if (rank == 0 && compute_bench_ended && comm_bench_ended)
		{
		    /* when doing computations and comm, we stop comm just after the end of computation on node 0 if we made enough comm */
		    end = 1;
		}

		/* tell node 1 if we continue communication benchmark: */
		if (rank == 0)
		{
		    MPI_Send(&end, 1, MPI_INT, other_rank, 3, MPI_COMM_WORLD);
		}
		else
		{
		    MPI_Recv(&end, 1, MPI_INT, other_rank, 3, MPI_COMM_WORLD, NULL);
		}

		if (end)
		{
		    break;
		}
	    }

	    double duration = ping_pong(params.comm_duplex);
	    if (params.display_comm && rank == 0)
	    {
		comm_durations[i] = duration;

		if (i == (comm_durations_size-1))
		{
		    comm_durations_size *= 2;
		    comm_durations = realloc(comm_durations, comm_durations_size * sizeof(double));
		}
	    }
	}

	if (params.display_comm && rank == 0)
	{
	    for (int j = 0; j < i; j++)
	    {
		printf("%.3lf MB/s - %.3lf us\n", params.pingpong_size * sizeof(float) / (1024*1024*(comm_durations[j] / 1000000)), comm_durations[j]);
	    }
	    fflush(stdout);
	}

	if (k == 0)
	{
#if WITH_STARPU == 1
	    starpu_resume(); // let computing benchmarks manage their StarPU's state
#endif
	    pthread_barrier_wait(&thread_barrier);
	    FREQ_EVENT("Comm without computing end");
	    SLEEP(10);
	    pthread_barrier_wait(&thread_barrier);
	}
    }

    free(comm_durations);

    return NULL;
}


int main(int argc, char* argv[])
{
    int ret = 0;
    int worldsize;
    double med_comm_duration = 1;
    double computing_duration;
    time_type start_time;
    time_type end_time;

#ifdef FREQ_MONITORING
    char* binding_str[64];
    for (int i = 0; i < 64; i++)
    {
	binding_str[i] = NULL;
    }
#endif

    init_params();
    parse_args(argc, argv, &params);

#if WITH_STARPU == 1
    if (params.enable_comm)
    {
	ret = starpu_mpi_init_conf(&argc, &argv, /* let StarPU initialize MPI */ 1, MPI_COMM_WORLD, NULL);
	if (ret != 0)
	{
	    printf("Error in initialization of StarPU\n");
	    return EXIT_FAILURE;
	}

	starpu_mpi_comm_rank(MPI_COMM_WORLD, &rank);
	starpu_mpi_comm_size(MPI_COMM_WORLD, &worldsize);
    }
    else
    {
	ret = starpu_init(NULL);
	if (ret != 0)
	{
	    printf("Error in initialization of StarPU\n");
	    return EXIT_FAILURE;
	}
    }

    machine.topology = starpu_get_hwloc_topology();

    PRINTF_RANK0("# StarPU version\n");
#elif defined(_OPENMP)
    if (params.enable_comm)
    {
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &worldsize);
    }

    hwloc_topology_init(&machine.topology);
    hwloc_topology_load(machine.topology);

    PRINTF_RANK0("# OpenMP version\n");
#endif

    fill_machine(&machine);
    init_malloc(&machine, params.memory_comp_numa_nodes, params.memory_comp_numa_nodes_nb, params.memory_comm_numa_node);

    if (params.display_help)
    {
	if (params.enable_comm == 0 || rank == 0)
	{
	    print_help(params);
	}
	runtime_shutdown(machine.topology, params.enable_comm);
	return EXIT_FAILURE;
    }

    if (rank == 0)
    {
	print_params(params);
    }

#ifdef FREQ_MONITORING
    if (params.measure_freq)
    {
	// Only rank 0 measures frequencies, if enabled:
	if (rank == 0)
	{
	    binding_str[machine.nb_pus-1] = "FreqMeter";
	}
	else
	{
	    params.measure_freq = 0;
	}
    }
#endif

    if (params.enable_comm)
    {
	if (worldsize != 2)
	{
	    fprintf(stderr, "We need two MPI processes.\n");
	    runtime_shutdown(machine.topology, params.enable_comm);
	    return EXIT_FAILURE;
	}

	other_rank = (rank == 1) ? 0 : 1;

	ping_thread_location_to_pu(&params, &machine);
#ifdef FREQ_MONITORING
	if (params.measure_freq && params.ping_thread_location == LAST)
	{
	    // FIXME: what about ranks != 0 ?
	    params.ping_thread_pu--;
	}
#endif
	assert(params.ping_thread_pu >= 0);
    }

    if (rank == 0)
    {
	print_machine(&machine, &params);
    }

#ifdef FREQ_MONITORING
    if (params.measure_freq)
    {
	freq_meter_init(machine.nb_pus, machine.topology);
	get_worker_binding(machine.topology, binding_str);
    }
#endif

    if (params.enable_comm)
    {
#ifdef FREQ_MONITORING
	if (params.measure_freq)
	{
	    binding_str[params.ping_thread_pu] = "PingPong";
	}
#endif

	if (params.computing.set_comm_bench_ended_ptr != NULL)
	{
	    params.computing.set_comm_bench_ended_ptr(&comm_bench_ended);
	}

	PRINTF_RANK0("# Will do %d pingpongs moving %d floats (%lu KB)\n", get_min_comm_todo(), params.pingpong_size, params.pingpong_size * sizeof(float) / 1024);
	ping_pong_init(rank, params.pingpong_type, params.pingpong_size);

	/**** Communication warmup, to register buffers for rcache */
	if (params.do_warmup)
	{
	    PRINTF_RANK0("# Communication warmup...\n");

	    FREQ_EVENT("Communication warmup start");

	    int nb_comm_warmup = COMM_BUFFER_POOL_SIZE * 3;
	    double* durations = malloc(nb_comm_warmup * sizeof(double));
	    for (int i = 0; i < nb_comm_warmup; i++)
	    {
		durations[i] = ping_pong(params.comm_duplex);
	    }

	    if (rank == 0)
	    {
		qsort(durations, nb_comm_warmup, sizeof(double), &comp_double);
		med_comm_duration = durations[(int) floor(nb_comm_warmup / 2)];

		double warmup_bw = (sizeof(float) / (1024*1024*(med_comm_duration / 1000000))) * params.pingpong_size;

		printf("# Communication warmup (%d iterations): med duration: %.3lf ms - med bandwidth: %.3lf MB/s\n",
			nb_comm_warmup, (med_comm_duration / 1000), warmup_bw);
	    }
	    free(durations);
	}
	/*** End of communication warmup */
    }
    else // communications are disabled
    {
	if (rank == 0 && params.display_binding)
	{
	    hwloc_display_binding();
	}
    }


    /*** Computing init and warmup */
    PRINTF_RANK0("# Computing init...\n");
    if (RANK_DOES_COMPUTATIONS && params.computing.init != NULL)
    {
	ret = params.computing.init();
	fflush(stdout);
	if (ret != 0)
	{
	    printf("Error in computing.init()\n");
	    runtime_shutdown(machine.topology, params.enable_comm);
	    return EXIT_FAILURE;
	}
    }

    if (RANK_DOES_COMPUTATIONS && params.do_warmup)
    {
	FREQ_EVENT("Computing warmup start");

	ret = params.computing.run(3, WARMUP);
	if (ret != 0)
	{
	    printf("Error in computing.run()\n");
	    runtime_shutdown(machine.topology, params.enable_comm);
	    return EXIT_FAILURE;
	}

	FREQ_EVENT("Computing warmup end");
	SLEEP(10);
    }
    /*** End of computing init and warmup */


    int nb_computing_run = params.computing.get_nb_runs(params.comm_bench_type);

    /*** Computing without communication */
    if (RANK_DOES_COMPUTATIONS && (params.do_alone & DO_COMP_ALONE))
    {
	PRINTF_RANK0("# Computing without communications...\n");
	FREQ_EVENT("Computing without comm start");

	timing_now(start_time);
	ret = params.computing.run(nb_computing_run, WITHOUT_COMM);
	if (ret != 0)
	{
	    printf("Error in computing.run()\n");
	    runtime_shutdown(machine.topology, params.enable_comm);
	    return EXIT_FAILURE;
	}
	timing_now(end_time);
	computing_duration = TIME_DIFF(start_time, end_time);

	FREQ_EVENT("Computing without comm end");

	if (params.enable_comm)
	{
	    SLEEP(10);
	}

	if (rank == 0)
	{
	    printf("# End of computing without communication\n");
	    printf("# Computing without communication duration: %.3lf ms\n", (computing_duration / 1000));
	}
    }
    /*** End of computing without communication */

    if (params.enable_comm)
    {
	/*** Launch thread which will do ping pongs */
	pthread_t ping_pong_thread;
	pthread_barrier_init(&thread_barrier, NULL, 2);
	if (params.do_alone & DO_COMM_ALONE)
	{
	    PRINTF_RANK0("# Communications without computing\n");
	}
	pthread_create(&ping_pong_thread, NULL, ping_pong_thread_func, NULL);
	if (params.do_alone & DO_COMM_ALONE)
	{
	    pthread_barrier_wait(&thread_barrier);
	    pthread_barrier_wait(&thread_barrier);
	}
	PRINTF_RANK0("# Communications with computing\n");
	pthread_barrier_wait(&thread_barrier);

	FREQ_EVENT("Comm with computing start");

	if (RANK_DOES_COMPUTATIONS)
	{
	    ret = params.computing.run(nb_computing_run, WITH_COMM);
	    if (ret != 0)
	    {
		printf("Error in computing.run()\n");
		runtime_shutdown(machine.topology, params.enable_comm);
		return EXIT_FAILURE;
	    }
	    compute_bench_ended = 1;
	}

	pthread_join(ping_pong_thread, NULL);
	pthread_barrier_destroy(&thread_barrier);

	FREQ_EVENT("Comm with computing end");

	PRINTF_RANK0("# End of computing with communications\n");
    }

    if (rank == 0)
    {
	params.computing.print_results();
	fflush(stdout);
    }

    if (RANK_DOES_COMPUTATIONS && params.computing.release != NULL)
    {
	params.computing.release();
    }

    if (params.enable_comm)
    {
	MPI_Barrier(MPI_COMM_WORLD);
	ping_pong_free();
    }

    runtime_shutdown(machine.topology, params.enable_comm);

#ifdef FREQ_MONITORING
    if (params.measure_freq)
    {
	/* Print binding only at the end, to catch all bound threads */
	freq_meter_set_bind_str(binding_str);
	freq_meter_release();
    }
#endif

    return EXIT_SUCCESS;
}
