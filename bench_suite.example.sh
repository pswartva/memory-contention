#!/bin/bash

# lstopo --no-factorize --of png > lstopo.png

RUNMPI="mpirun -n 2 -nodelist henri0,henri1"
OMP_OPT="-DOMP_PROC_BIND=true -DOMP_PLACES=cores"
BASIC_STREAM="$HOME/memory-bottleneck/one-source/bench_openmp --compute_bench=stream --no_stream_add --no_stream_scale"
BASIC_PRIME="$HOME/memory-bottleneck/one-source/bench_openmp --compute_bench=prime"
STARPU_STREAM="$HOME/memory-bottleneck/one-source/bench_starpu --compute_bench=stream --no_stream_add --no_stream_scale"
STARPU_CHOLESKY="$HOME/memory-bottleneck/one-source/bench_starpu --compute_bench=cholesky"
STARPU_PRIME="$HOME/memory-bottleneck/one-source/bench_starpu --compute_bench=prime"

ncores=$(nproc)
echo "Number of cores = " $ncores;

nbnuma=$(numactl --hardware | grep available | cut -d " " -f 2)
last_numa_node=$(($nbnuma-1))
echo "Last NUMA node =  " $last_numa_node;

saving_folder=$1
mkdir $saving_folder

export STARPU_BACKOFF_MAX=10000


echo "**** MadMPI ****"
mkdir $saving_folder/madmpi

    echo "*** OpenMP STREAM + MPI ***"

    mkdir $saving_folder/madmpi/openmp_stream

        echo "** Memory not bound **"

        sf=$saving_folder/madmpi/openmp_stream/mem_not_bound
        mkdir $sf

            echo "* Latency *"
            date

                echo "Thread first"
                for i in $(seq 1 $((ncores-1)));
                do
                    echo $i;
                    $RUNMPI -DOMP_NUM_THREADS=$i $OMP_OPT hwloc-bind --cpubind core:1-$i $BASIC_STREAM --ping_thread=first > $sf/latency_thread_first_$((i))_threads.out;
                done

                date

                echo "Thread last"
                for i in $(seq 1 $((ncores-1)));
                do
                    echo $i;
                    $RUNMPI -DOMP_NUM_THREADS=$i $OMP_OPT hwloc-bind --cpubind core:0-$((i-1)) $BASIC_STREAM --ping_thread=last > $sf/latency_thread_last_$((i))_threads.out;
                done


            echo "* Bandwidth *"
            date

                echo "Thread first"
                for i in $(seq 1 $((ncores-1)));
                do
                    echo $i;
                    $RUNMPI -DOMP_NUM_THREADS=$i $OMP_OPT hwloc-bind --cpubind core:1-$i $BASIC_STREAM --ping_thread=first --bench=bandwidth > $sf/bandwidth_thread_first_$((i))_threads.out;
                done

                echo "Thread last"
                for i in $(seq 1 $((ncores-1)));
                do
                    echo $i;
                    $RUNMPI -DOMP_NUM_THREADS=$i $OMP_OPT hwloc-bind --cpubind core:0-$((i-1)) $BASIC_STREAM --ping_thread=last --bench=bandwidth > $sf/bandwidth_thread_last_$((i))_threads.out;
                done


        echo "** Memory bound first **"

        sf=$saving_folder/madmpi/openmp_stream/mem_bound_first
        mkdir $sf

            echo "* Latency *"
            date

                echo "Thread first"
                for i in $(seq 1 $((ncores-1)));
                do
                    echo $i;
                    $RUNMPI -DOMP_NUM_THREADS=$i $OMP_OPT hwloc-bind --membind node:0 --cpubind core:1-$i $BASIC_STREAM --ping_thread=first > $sf/latency_thread_first_$((i))_threads.out;
                done

                date

                echo "Thread last"
                for i in $(seq 1 $((ncores-1)));
                do
                    echo $i;
                    $RUNMPI -DOMP_NUM_THREADS=$i $OMP_OPT hwloc-bind --membind node:0 --cpubind core:0-$((i-1)) $BASIC_STREAM --ping_thread=last > $sf/latency_thread_last_$((i))_threads.out;
                done


            echo "* Bandwidth *"
            date

                echo "Thread first"
                for i in $(seq 1 $((ncores-1)));
                do
                    echo $i;
                    $RUNMPI -DOMP_NUM_THREADS=$i $OMP_OPT hwloc-bind --membind node:0 --cpubind core:1-$i $BASIC_STREAM --ping_thread=first --bench=bandwidth > $sf/bandwidth_thread_first_$((i))_threads.out;
                done

                echo "Thread last"
                for i in $(seq 1 $((ncores-1)));
                do
                    echo $i;
                    $RUNMPI -DOMP_NUM_THREADS=$i $OMP_OPT hwloc-bind --membind node:0 --cpubind core:0-$((i-1)) $BASIC_STREAM --ping_thread=last --bench=bandwidth > $sf/bandwidth_thread_last_$((i))_threads.out;
                done

        echo "** Memory bound last **"

        sf=$saving_folder/madmpi/openmp_stream/mem_bound_last
        mkdir $sf

            echo "* Latency *"
            date

                echo "Thread first"
                for i in $(seq 1 $((ncores-1)));
                do
                    echo $i;
                    $RUNMPI -DOMP_NUM_THREADS=$i $OMP_OPT hwloc-bind --membind node:$last_numa_node --cpubind core:1-$i $BASIC_STREAM --ping_thread=first > $sf/latency_thread_first_$((i))_threads.out;
                done

                date

                echo "Thread last"
                for i in $(seq 1 $((ncores-1)));
                do
                    echo $i;
                    $RUNMPI -DOMP_NUM_THREADS=$i $OMP_OPT hwloc-bind --membind node:$last_numa_node --cpubind core:0-$((i-1)) $BASIC_STREAM --ping_thread=last > $sf/latency_thread_last_$((i))_threads.out;
                done


            echo "* Bandwidth *"
            date

                echo "Thread first"
                for i in $(seq 1 $((ncores-1)));
                do
                    echo $i;
                    $RUNMPI -DOMP_NUM_THREADS=$i $OMP_OPT hwloc-bind --membind node:$last_numa_node --cpubind core:1-$i $BASIC_STREAM --ping_thread=first --bench=bandwidth > $sf/bandwidth_thread_first_$((i))_threads.out;
                done

                echo "Thread last"
                for i in $(seq 1 $((ncores-1)));
                do
                    echo $i;
                    $RUNMPI -DOMP_NUM_THREADS=$i $OMP_OPT hwloc-bind --membind node:$last_numa_node --cpubind core:0-$((i-1)) $BASIC_STREAM --ping_thread=last --bench=bandwidth > $sf/bandwidth_thread_last_$((i))_threads.out;
                done
            date




    echo "==========================================="




    echo "*** OpenMP Prime + MPI ***"

    mkdir $saving_folder/madmpi/openmp_prime

        echo "** Memory not bound **"

        sf=$saving_folder/madmpi/openmp_prime/mem_not_bound
        mkdir $sf

            echo "* Latency *"
            date

                echo "Thread first"
                for i in $(seq 1 $((ncores-1)));
                do
                    echo $i;
                    $RUNMPI -DOMP_NUM_THREADS=$i $OMP_OPT hwloc-bind --cpubind core:1-$i $BASIC_PRIME --ping_thread=first > $sf/latency_thread_first_$((i))_threads.out;
                done

                date

                echo "Thread last"
                for i in $(seq 1 $((ncores-1)));
                do
                    echo $i;
                    $RUNMPI -DOMP_NUM_THREADS=$i $OMP_OPT hwloc-bind --cpubind core:0-$((i-1)) $BASIC_PRIME --ping_thread=last > $sf/latency_thread_last_$((i))_threads.out;
                done


            echo "* Bandwidth *"
            date

                echo "Thread first"
                for i in $(seq 1 $((ncores-1)));
                do
                    echo $i;
                    $RUNMPI -DOMP_NUM_THREADS=$i $OMP_OPT hwloc-bind --cpubind core:1-$i $BASIC_PRIME --ping_thread=first --bench=bandwidth > $sf/bandwidth_thread_first_$((i))_threads.out;
                done

                echo "Thread last"
                for i in $(seq 1 $((ncores-1)));
                do
                    echo $i;
                    $RUNMPI -DOMP_NUM_THREADS=$i $OMP_OPT hwloc-bind --cpubind core:0-$((i-1)) $BASIC_PRIME --ping_thread=last --bench=bandwidth > $sf/bandwidth_thread_last_$((i))_threads.out;
                done

            date
            exit 0



    echo "==========================================="





    echo "*** StarPU STREAM + MPI ***"

    mkdir $saving_folder/madmpi/starpu_stream

        echo "** Memory not bound **"

        sf=$saving_folder/madmpi/starpu_stream/mem_not_bound
        mkdir $sf

            echo "* Latency *"
            date

                echo "Thread first, main bound last, MPI thread last"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=1- -DSTARPU_MAIN_THREAD_CPUID=$((ncores-2)) -DSTARPU_MPI_THREAD_CPUID=$((ncores-1)) $STARPU_STREAM --ping_thread=first > $sf/latency_thread_first_main_last_mpi_last_$((i-1))_threads.out;
                done

                date

                echo "Thread first, main bound last, MPI thread first"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=2- -DSTARPU_MAIN_THREAD_CPUID=$((ncores-1)) -DSTARPU_MPI_THREAD_CPUID=1 $STARPU_STREAM --ping_thread=first > $sf/latency_thread_first_main_last_mpi_first_$((i-1))_threads.out;
                done

                date

                echo "Thread first, main bound first, MPI thread last"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=2- -DSTARPU_MAIN_THREAD_CPUID=1 -DSTARPU_MPI_THREAD_CPUID=$((ncores-1)) $STARPU_STREAM --ping_thread=first > $sf/latency_thread_first_main_first_mpi_last_$((i-1))_threads.out;
                done

                date

                echo "Thread first, main bound first, MPI thread first"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=3- -DSTARPU_MAIN_THREAD_CPUID=1 -DSTARPU_MPI_THREAD_CPUID=2 $STARPU_STREAM --ping_thread=first > $sf/latency_thread_first_main_first_mpi_first_$((i-1))_threads.out;
                done

                date

                echo "Thread last, main bound last, MPI thread last"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=0- -DSTARPU_MAIN_THREAD_CPUID=$((ncores-3)) -DSTARPU_MPI_THREAD_CPUID=$((ncores-2)) $STARPU_STREAM --ping_thread=last > $sf/latency_thread_last_main_last_mpi_last_$((i-1))_threads.out;
                done

                date

                echo "Thread last, main bound last, MPI thread first"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=1- -DSTARPU_MAIN_THREAD_CPUID=$((ncores-2)) -DSTARPU_MPI_THREAD_CPUID=0 $STARPU_STREAM --ping_thread=last > $sf/latency_thread_last_main_last_mpi_first_$((i-1))_threads.out;
                done

                date

                echo "Thread last, main bound first, MPI thread last"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=1- -DSTARPU_MAIN_THREAD_CPUID=0 -DSTARPU_MPI_THREAD_CPUID=$((ncores-2)) $STARPU_STREAM --ping_thread=last > $sf/latency_thread_last_main_first_mpi_last_$((i-1))_threads.out;
                done

                date

                echo "Thread last, main bound first, MPI thread first"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=2- -DSTARPU_MAIN_THREAD_CPUID=0 -DSTARPU_MPI_THREAD_CPUID=1 $STARPU_STREAM --ping_thread=last > $sf/latency_thread_last_main_first_mpi_first_$((i-1))_threads.out;
                done

            echo "* Bandwidth *"
            date

                echo "Thread first, main bound last, MPI thread last"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=1- -DSTARPU_MAIN_THREAD_CPUID=$((ncores-2)) -DSTARPU_MPI_THREAD_CPUID=$((ncores-1)) $STARPU_STREAM --ping_thread=first --bench=bandwidth > $sf/bandwidth_thread_first_main_last_mpi_last_$((i-1))_threads.out;
                done

                date

                echo "Thread first, main bound last, MPI thread first"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=2- -DSTARPU_MAIN_THREAD_CPUID=$((ncores-1)) -DSTARPU_MPI_THREAD_CPUID=1 $STARPU_STREAM --ping_thread=first --bench=bandwidth > $sf/bandwidth_thread_first_main_last_mpi_first_$((i-1))_threads.out;
                done

                date

                echo "Thread first, main bound first, MPI thread last"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=2- -DSTARPU_MAIN_THREAD_CPUID=1 -DSTARPU_MPI_THREAD_CPUID=$((ncores-1)) $STARPU_STREAM --ping_thread=first --bench=bandwidth > $sf/bandwidth_thread_first_main_first_mpi_last_$((i-1))_threads.out;
                done

                date

                echo "Thread first, main bound first, MPI thread first"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=3- -DSTARPU_MAIN_THREAD_CPUID=1 -DSTARPU_MPI_THREAD_CPUID=2 $STARPU_STREAM --ping_thread=first --bench=bandwidth > $sf/bandwidth_thread_first_main_first_mpi_first_$((i-1))_threads.out;
                done

                date

                echo "Thread last, main bound last, MPI thread last"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=0- -DSTARPU_MAIN_THREAD_CPUID=$((ncores-3)) -DSTARPU_MPI_THREAD_CPUID=$((ncores-2)) $STARPU_STREAM --ping_thread=last --bench=bandwidth > $sf/bandwidth_thread_last_main_last_mpi_last_$((i-1))_threads.out;
                done

                date

                echo "Thread last, main bound last, MPI thread first"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=1- -DSTARPU_MAIN_THREAD_CPUID=$((ncores-2)) -DSTARPU_MPI_THREAD_CPUID=0 $STARPU_STREAM --ping_thread=last --bench=bandwidth > $sf/bandwidth_thread_last_main_last_mpi_first_$((i-1))_threads.out;
                done

                date

                echo "Thread last, main bound first, MPI thread last"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=1- -DSTARPU_MAIN_THREAD_CPUID=0 -DSTARPU_MPI_THREAD_CPUID=$((ncores-2)) $STARPU_STREAM --ping_thread=last --bench=bandwidth > $sf/bandwidth_thread_last_main_first_mpi_last_$((i-1))_threads.out;
                done

                date

                echo "Thread last, main bound first, MPI thread first"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=2- -DSTARPU_MAIN_THREAD_CPUID=0 -DSTARPU_MPI_THREAD_CPUID=1 $STARPU_STREAM --ping_thread=last --bench=bandwidth > $sf/bandwidth_thread_last_main_first_mpi_first_$((i-1))_threads.out;
                done

        echo "** Memory bound first **"

        sf=$saving_folder/madmpi/starpu_stream/mem_bound_first
        mkdir $sf

            echo "* Latency *"
            date

                echo "Thread first, main bound last, MPI thread last"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=1- -DSTARPU_MAIN_THREAD_CPUID=$((ncores-2)) -DSTARPU_MPI_THREAD_CPUID=$((ncores-1)) hwloc-bind --membind node:0 $STARPU_STREAM --ping_thread=first > $sf/latency_thread_first_main_last_mpi_last_$((i-1))_threads.out;
                done

                date

                echo "Thread first, main bound last, MPI thread first"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=2- -DSTARPU_MAIN_THREAD_CPUID=$((ncores-1)) -DSTARPU_MPI_THREAD_CPUID=1 hwloc-bind --membind node:0 $STARPU_STREAM --ping_thread=first > $sf/latency_thread_first_main_last_mpi_first_$((i-1))_threads.out;
                done

                date

                echo "Thread first, main bound first, MPI thread last"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=2- -DSTARPU_MAIN_THREAD_CPUID=1 -DSTARPU_MPI_THREAD_CPUID=$((ncores-1)) hwloc-bind --membind node:0 $STARPU_STREAM --ping_thread=first > $sf/latency_thread_first_main_first_mpi_last_$((i-1))_threads.out;
                done

                date

                echo "Thread first, main bound first, MPI thread first"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=3- -DSTARPU_MAIN_THREAD_CPUID=1 -DSTARPU_MPI_THREAD_CPUID=2 hwloc-bind --membind node:0 $STARPU_STREAM --ping_thread=first > $sf/latency_thread_first_main_first_mpi_first_$((i-1))_threads.out;
                done

                date

                echo "Thread last, main bound last, MPI thread last"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=0- -DSTARPU_MAIN_THREAD_CPUID=$((ncores-3)) -DSTARPU_MPI_THREAD_CPUID=$((ncores-2)) hwloc-bind --membind node:0 $STARPU_STREAM --ping_thread=last > $sf/latency_thread_last_main_last_mpi_last_$((i-1))_threads.out;
                done

                date

                echo "Thread last, main bound last, MPI thread first"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=1- -DSTARPU_MAIN_THREAD_CPUID=$((ncores-2)) -DSTARPU_MPI_THREAD_CPUID=0 hwloc-bind --membind node:0 $STARPU_STREAM --ping_thread=last > $sf/latency_thread_last_main_last_mpi_first_$((i-1))_threads.out;
                done

                date

                echo "Thread last, main bound first, MPI thread last"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=1- -DSTARPU_MAIN_THREAD_CPUID=0 -DSTARPU_MPI_THREAD_CPUID=$((ncores-2)) hwloc-bind --membind node:0 $STARPU_STREAM --ping_thread=last > $sf/latency_thread_last_main_first_mpi_last_$((i-1))_threads.out;
                done

                date

                echo "Thread last, main bound first, MPI thread first"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=2- -DSTARPU_MAIN_THREAD_CPUID=0 -DSTARPU_MPI_THREAD_CPUID=1 hwloc-bind --membind node:0 $STARPU_STREAM --ping_thread=last > $sf/latency_thread_last_main_first_mpi_first_$((i-1))_threads.out;
                done

            echo "* Bandwidth *"
            date

                echo "Thread first, main bound last, MPI thread last"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=1- -DSTARPU_MAIN_THREAD_CPUID=$((ncores-2)) -DSTARPU_MPI_THREAD_CPUID=$((ncores-1)) hwloc-bind --membind node:0 $STARPU_STREAM --ping_thread=first --bench=bandwidth > $sf/bandwidth_thread_first_main_last_mpi_last_$((i-1))_threads.out;
                done

                date

                echo "Thread first, main bound last, MPI thread first"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=2- -DSTARPU_MAIN_THREAD_CPUID=$((ncores-1)) -DSTARPU_MPI_THREAD_CPUID=1 hwloc-bind --membind node:0 $STARPU_STREAM --ping_thread=first --bench=bandwidth > $sf/bandwidth_thread_first_main_last_mpi_first_$((i-1))_threads.out;
                done

                date

                echo "Thread first, main bound first, MPI thread last"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=2- -DSTARPU_MAIN_THREAD_CPUID=1 -DSTARPU_MPI_THREAD_CPUID=$((ncores-1)) hwloc-bind --membind node:0 $STARPU_STREAM --ping_thread=first --bench=bandwidth > $sf/bandwidth_thread_first_main_first_mpi_last_$((i-1))_threads.out;
                done

                date

                echo "Thread first, main bound first, MPI thread first"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=3- -DSTARPU_MAIN_THREAD_CPUID=1 -DSTARPU_MPI_THREAD_CPUID=2 hwloc-bind --membind node:0 $STARPU_STREAM --ping_thread=first --bench=bandwidth > $sf/bandwidth_thread_first_main_first_mpi_first_$((i-1))_threads.out;
                done

                date

                echo "Thread last, main bound last, MPI thread last"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=0- -DSTARPU_MAIN_THREAD_CPUID=$((ncores-3)) -DSTARPU_MPI_THREAD_CPUID=$((ncores-2)) hwloc-bind --membind node:0 $STARPU_STREAM --ping_thread=last --bench=bandwidth > $sf/bandwidth_thread_last_main_last_mpi_last_$((i-1))_threads.out;
                done

                date

                echo "Thread last, main bound last, MPI thread first"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=1- -DSTARPU_MAIN_THREAD_CPUID=$((ncores-2)) -DSTARPU_MPI_THREAD_CPUID=0 hwloc-bind --membind node:0 $STARPU_STREAM --ping_thread=last --bench=bandwidth > $sf/bandwidth_thread_last_main_last_mpi_first_$((i-1))_threads.out;
                done

                date

                echo "Thread last, main bound first, MPI thread last"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=1- -DSTARPU_MAIN_THREAD_CPUID=0 -DSTARPU_MPI_THREAD_CPUID=$((ncores-2)) hwloc-bind --membind node:0 $STARPU_STREAM --ping_thread=last --bench=bandwidth > $sf/bandwidth_thread_last_main_first_mpi_last_$((i-1))_threads.out;
                done

                date

                echo "Thread last, main bound first, MPI thread first"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=2- -DSTARPU_MAIN_THREAD_CPUID=0 -DSTARPU_MPI_THREAD_CPUID=1 hwloc-bind --membind node:0 $STARPU_STREAM --ping_thread=last --bench=bandwidth > $sf/bandwidth_thread_last_main_first_mpi_first_$((i-1))_threads.out;
                done

        echo "** Memory bound last **"

        sf=$saving_folder/madmpi/starpu_stream/mem_bound_last
        mkdir $sf

            echo "* Latency *"
            date

                echo "Thread first, main bound last, MPI thread last"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=1- -DSTARPU_MAIN_THREAD_CPUID=$((ncores-2)) -DSTARPU_MPI_THREAD_CPUID=$((ncores-1)) hwloc-bind --membind node:$last_numa_node $STARPU_STREAM --ping_thread=first > $sf/latency_thread_first_main_last_mpi_last_$((i-1))_threads.out;
                done

                date

                echo "Thread first, main bound last, MPI thread first"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=2- -DSTARPU_MAIN_THREAD_CPUID=$((ncores-1)) -DSTARPU_MPI_THREAD_CPUID=1 hwloc-bind --membind node:$last_numa_node $STARPU_STREAM --ping_thread=first > $sf/latency_thread_first_main_last_mpi_first_$((i-1))_threads.out;
                done

                date

                echo "Thread first, main bound first, MPI thread last"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=2- -DSTARPU_MAIN_THREAD_CPUID=1 -DSTARPU_MPI_THREAD_CPUID=$((ncores-1)) hwloc-bind --membind node:$last_numa_node $STARPU_STREAM --ping_thread=first > $sf/latency_thread_first_main_first_mpi_last_$((i-1))_threads.out;
                done

                date

                echo "Thread first, main bound first, MPI thread first"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=3- -DSTARPU_MAIN_THREAD_CPUID=1 -DSTARPU_MPI_THREAD_CPUID=2 hwloc-bind --membind node:$last_numa_node $STARPU_STREAM --ping_thread=first > $sf/latency_thread_first_main_first_mpi_first_$((i-1))_threads.out;
                done

                date

                echo "Thread last, main bound last, MPI thread last"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=0- -DSTARPU_MAIN_THREAD_CPUID=$((ncores-3)) -DSTARPU_MPI_THREAD_CPUID=$((ncores-2)) hwloc-bind --membind node:$last_numa_node $STARPU_STREAM --ping_thread=last > $sf/latency_thread_last_main_last_mpi_last_$((i-1))_threads.out;
                done

                date

                echo "Thread last, main bound last, MPI thread first"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=1- -DSTARPU_MAIN_THREAD_CPUID=$((ncores-2)) -DSTARPU_MPI_THREAD_CPUID=0 hwloc-bind --membind node:$last_numa_node $STARPU_STREAM --ping_thread=last > $sf/latency_thread_last_main_last_mpi_first_$((i-1))_threads.out;
                done

                date

                echo "Thread last, main bound first, MPI thread last"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=1- -DSTARPU_MAIN_THREAD_CPUID=0 -DSTARPU_MPI_THREAD_CPUID=$((ncores-2)) hwloc-bind --membind node:$last_numa_node $STARPU_STREAM --ping_thread=last > $sf/latency_thread_last_main_first_mpi_last_$((i-1))_threads.out;
                done

                date

                echo "Thread last, main bound first, MPI thread first"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=2- -DSTARPU_MAIN_THREAD_CPUID=0 -DSTARPU_MPI_THREAD_CPUID=1 hwloc-bind --membind node:$last_numa_node $STARPU_STREAM --ping_thread=last > $sf/latency_thread_last_main_first_mpi_first_$((i-1))_threads.out;
                done

            echo "* Bandwidth *"
            date

                echo "Thread first, main bound last, MPI thread last"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=1- -DSTARPU_MAIN_THREAD_CPUID=$((ncores-2)) -DSTARPU_MPI_THREAD_CPUID=$((ncores-1)) hwloc-bind --membind node:$last_numa_node $STARPU_STREAM --ping_thread=first --bench=bandwidth > $sf/bandwidth_thread_first_main_last_mpi_last_$((i-1))_threads.out;
                done

                date

                echo "Thread first, main bound last, MPI thread first"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=2- -DSTARPU_MAIN_THREAD_CPUID=$((ncores-1)) -DSTARPU_MPI_THREAD_CPUID=1 hwloc-bind --membind node:$last_numa_node $STARPU_STREAM --ping_thread=first --bench=bandwidth > $sf/bandwidth_thread_first_main_last_mpi_first_$((i-1))_threads.out;
                done

                date

                echo "Thread first, main bound first, MPI thread last"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=2- -DSTARPU_MAIN_THREAD_CPUID=1 -DSTARPU_MPI_THREAD_CPUID=$((ncores-1)) hwloc-bind --membind node:$last_numa_node $STARPU_STREAM --ping_thread=first --bench=bandwidth > $sf/bandwidth_thread_first_main_first_mpi_last_$((i-1))_threads.out;
                done

                date

                echo "Thread first, main bound first, MPI thread first"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=3- -DSTARPU_MAIN_THREAD_CPUID=1 -DSTARPU_MPI_THREAD_CPUID=2 hwloc-bind --membind node:$last_numa_node $STARPU_STREAM --ping_thread=first --bench=bandwidth > $sf/bandwidth_thread_first_main_first_mpi_first_$((i-1))_threads.out;
                done

                date

                echo "Thread last, main bound last, MPI thread last"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=0- -DSTARPU_MAIN_THREAD_CPUID=$((ncores-3)) -DSTARPU_MPI_THREAD_CPUID=$((ncores-2)) hwloc-bind --membind node:$last_numa_node $STARPU_STREAM --ping_thread=last --bench=bandwidth > $sf/bandwidth_thread_last_main_last_mpi_last_$((i-1))_threads.out;
                done

                date

                echo "Thread last, main bound last, MPI thread first"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=1- -DSTARPU_MAIN_THREAD_CPUID=$((ncores-2)) -DSTARPU_MPI_THREAD_CPUID=0 hwloc-bind --membind node:$last_numa_node $STARPU_STREAM --ping_thread=last --bench=bandwidth > $sf/bandwidth_thread_last_main_last_mpi_first_$((i-1))_threads.out;
                done

                date

                echo "Thread last, main bound first, MPI thread last"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=1- -DSTARPU_MAIN_THREAD_CPUID=0 -DSTARPU_MPI_THREAD_CPUID=$((ncores-2)) hwloc-bind --membind node:$last_numa_node $STARPU_STREAM --ping_thread=last --bench=bandwidth > $sf/bandwidth_thread_last_main_first_mpi_last_$((i-1))_threads.out;
                done

                date

                echo "Thread last, main bound first, MPI thread first"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=2- -DSTARPU_MAIN_THREAD_CPUID=0 -DSTARPU_MPI_THREAD_CPUID=1 hwloc-bind --membind node:$last_numa_node $STARPU_STREAM --ping_thread=last --bench=bandwidth > $sf/bandwidth_thread_last_main_first_mpi_first_$((i-1))_threads.out;
                done




    echo "==========================================="





    echo "*** StarPU Cholesky + MPI ***"

    mkdir $saving_folder/madmpi/starpu_cholesky

        echo "** Memory not bound **"

        sf=$saving_folder/madmpi/starpu_cholesky/mem_not_bound
        mkdir $sf

            echo "* Latency *"
            date

                echo "Thread first, main bound last, MPI thread last"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=1- -DSTARPU_MAIN_THREAD_CPUID=$((ncores-2)) -DSTARPU_MPI_THREAD_CPUID=$((ncores-1)) $STARPU_CHOLESKY --ping_thread=first > $sf/latency_thread_first_main_last_mpi_last_$((i-1))_threads.out;
                done

                date

                echo "Thread first, main bound last, MPI thread first"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=2- -DSTARPU_MAIN_THREAD_CPUID=$((ncores-1)) -DSTARPU_MPI_THREAD_CPUID=1 $STARPU_CHOLESKY --ping_thread=first > $sf/latency_thread_first_main_last_mpi_first_$((i-1))_threads.out;
                done

                date

                echo "Thread first, main bound first, MPI thread last"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=2- -DSTARPU_MAIN_THREAD_CPUID=1 -DSTARPU_MPI_THREAD_CPUID=$((ncores-1)) $STARPU_CHOLESKY --ping_thread=first > $sf/latency_thread_first_main_first_mpi_last_$((i-1))_threads.out;
                done

                date

                echo "Thread first, main bound first, MPI thread first"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=3- -DSTARPU_MAIN_THREAD_CPUID=1 -DSTARPU_MPI_THREAD_CPUID=2 $STARPU_CHOLESKY --ping_thread=first > $sf/latency_thread_first_main_first_mpi_first_$((i-1))_threads.out;
                done

                date

                echo "Thread last, main bound last, MPI thread last"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=0- -DSTARPU_MAIN_THREAD_CPUID=$((ncores-3)) -DSTARPU_MPI_THREAD_CPUID=$((ncores-2)) $STARPU_CHOLESKY --ping_thread=last > $sf/latency_thread_last_main_last_mpi_last_$((i-1))_threads.out;
                done

                date

                echo "Thread last, main bound last, MPI thread first"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=1- -DSTARPU_MAIN_THREAD_CPUID=$((ncores-2)) -DSTARPU_MPI_THREAD_CPUID=0 $STARPU_CHOLESKY --ping_thread=last > $sf/latency_thread_last_main_last_mpi_first_$((i-1))_threads.out;
                done

                date

                echo "Thread last, main bound first, MPI thread last"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=1- -DSTARPU_MAIN_THREAD_CPUID=0 -DSTARPU_MPI_THREAD_CPUID=$((ncores-2)) $STARPU_CHOLESKY --ping_thread=last > $sf/latency_thread_last_main_first_mpi_last_$((i-1))_threads.out;
                done

                date

                echo "Thread last, main bound first, MPI thread first"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=2- -DSTARPU_MAIN_THREAD_CPUID=0 -DSTARPU_MPI_THREAD_CPUID=1 $STARPU_CHOLESKY --ping_thread=last > $sf/latency_thread_last_main_first_mpi_first_$((i-1))_threads.out;
                done

            echo "* Bandwidth *"
            date

                echo "Thread first, main bound last, MPI thread last"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=1- -DSTARPU_MAIN_THREAD_CPUID=$((ncores-2)) -DSTARPU_MPI_THREAD_CPUID=$((ncores-1)) $STARPU_CHOLESKY --ping_thread=first --bench=bandwidth > $sf/bandwidth_thread_first_main_last_mpi_last_$((i-1))_threads.out;
                done

                date

                echo "Thread first, main bound last, MPI thread first"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=2- -DSTARPU_MAIN_THREAD_CPUID=$((ncores-1)) -DSTARPU_MPI_THREAD_CPUID=1 $STARPU_CHOLESKY --ping_thread=first --bench=bandwidth > $sf/bandwidth_thread_first_main_last_mpi_first_$((i-1))_threads.out;
                done

                date

                echo "Thread first, main bound first, MPI thread last"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=2- -DSTARPU_MAIN_THREAD_CPUID=1 -DSTARPU_MPI_THREAD_CPUID=$((ncores-1)) $STARPU_CHOLESKY --ping_thread=first --bench=bandwidth > $sf/bandwidth_thread_first_main_first_mpi_last_$((i-1))_threads.out;
                done

                date

                echo "Thread first, main bound first, MPI thread first"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=3- -DSTARPU_MAIN_THREAD_CPUID=1 -DSTARPU_MPI_THREAD_CPUID=2 $STARPU_CHOLESKY --ping_thread=first --bench=bandwidth > $sf/bandwidth_thread_first_main_first_mpi_first_$((i-1))_threads.out;
                done

                date

                echo "Thread last, main bound last, MPI thread last"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=0- -DSTARPU_MAIN_THREAD_CPUID=$((ncores-3)) -DSTARPU_MPI_THREAD_CPUID=$((ncores-2)) $STARPU_CHOLESKY --ping_thread=last --bench=bandwidth > $sf/bandwidth_thread_last_main_last_mpi_last_$((i-1))_threads.out;
                done

                date

                echo "Thread last, main bound last, MPI thread first"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=1- -DSTARPU_MAIN_THREAD_CPUID=$((ncores-2)) -DSTARPU_MPI_THREAD_CPUID=0 $STARPU_CHOLESKY --ping_thread=last --bench=bandwidth > $sf/bandwidth_thread_last_main_last_mpi_first_$((i-1))_threads.out;
                done

                date

                echo "Thread last, main bound first, MPI thread last"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=1- -DSTARPU_MAIN_THREAD_CPUID=0 -DSTARPU_MPI_THREAD_CPUID=$((ncores-2)) $STARPU_CHOLESKY --ping_thread=last --bench=bandwidth > $sf/bandwidth_thread_last_main_first_mpi_last_$((i-1))_threads.out;
                done

                date

                echo "Thread last, main bound first, MPI thread first"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=2- -DSTARPU_MAIN_THREAD_CPUID=0 -DSTARPU_MPI_THREAD_CPUID=1 $STARPU_CHOLESKY --ping_thread=last --bench=bandwidth > $sf/bandwidth_thread_last_main_first_mpi_first_$((i-1))_threads.out;
                done




    echo "==========================================="





    echo "*** StarPU Prime + MPI ***"

    mkdir $saving_folder/madmpi/starpu_prime

        echo "** Memory not bound **"

        sf=$saving_folder/madmpi/starpu_prime/mem_not_bound
        mkdir $sf

            echo "* Latency *"
            date

                echo "Thread first, main bound last, MPI thread last"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=1- -DSTARPU_MAIN_THREAD_CPUID=$((ncores-2)) -DSTARPU_MPI_THREAD_CPUID=$((ncores-1)) $STARPU_PRIME --ping_thread=first > $sf/latency_thread_first_main_last_mpi_last_$((i-1))_threads.out;
                done

                date

                echo "Thread first, main bound last, MPI thread first"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=2- -DSTARPU_MAIN_THREAD_CPUID=$((ncores-1)) -DSTARPU_MPI_THREAD_CPUID=1 $STARPU_PRIME --ping_thread=first > $sf/latency_thread_first_main_last_mpi_first_$((i-1))_threads.out;
                done

                date

                echo "Thread first, main bound first, MPI thread last"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=2- -DSTARPU_MAIN_THREAD_CPUID=1 -DSTARPU_MPI_THREAD_CPUID=$((ncores-1)) $STARPU_PRIME --ping_thread=first > $sf/latency_thread_first_main_first_mpi_last_$((i-1))_threads.out;
                done

                date

                echo "Thread first, main bound first, MPI thread first"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=3- -DSTARPU_MAIN_THREAD_CPUID=1 -DSTARPU_MPI_THREAD_CPUID=2 $STARPU_PRIME --ping_thread=first > $sf/latency_thread_first_main_first_mpi_first_$((i-1))_threads.out;
                done

                date

                echo "Thread last, main bound last, MPI thread last"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=0- -DSTARPU_MAIN_THREAD_CPUID=$((ncores-3)) -DSTARPU_MPI_THREAD_CPUID=$((ncores-2)) $STARPU_PRIME --ping_thread=last > $sf/latency_thread_last_main_last_mpi_last_$((i-1))_threads.out;
                done

                date

                echo "Thread last, main bound last, MPI thread first"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=1- -DSTARPU_MAIN_THREAD_CPUID=$((ncores-2)) -DSTARPU_MPI_THREAD_CPUID=0 $STARPU_PRIME --ping_thread=last > $sf/latency_thread_last_main_last_mpi_first_$((i-1))_threads.out;
                done

                date

                echo "Thread last, main bound first, MPI thread last"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=1- -DSTARPU_MAIN_THREAD_CPUID=0 -DSTARPU_MPI_THREAD_CPUID=$((ncores-2)) $STARPU_PRIME --ping_thread=last > $sf/latency_thread_last_main_first_mpi_last_$((i-1))_threads.out;
                done

                date

                echo "Thread last, main bound first, MPI thread first"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=2- -DSTARPU_MAIN_THREAD_CPUID=0 -DSTARPU_MPI_THREAD_CPUID=1 $STARPU_PRIME --ping_thread=last > $sf/latency_thread_last_main_first_mpi_first_$((i-1))_threads.out;
                done

            echo "* Bandwidth *"
            date

                echo "Thread first, main bound last, MPI thread last"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=1- -DSTARPU_MAIN_THREAD_CPUID=$((ncores-2)) -DSTARPU_MPI_THREAD_CPUID=$((ncores-1)) $STARPU_PRIME --ping_thread=first --bench=bandwidth > $sf/bandwidth_thread_first_main_last_mpi_last_$((i-1))_threads.out;
                done

                date

                echo "Thread first, main bound last, MPI thread first"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=2- -DSTARPU_MAIN_THREAD_CPUID=$((ncores-1)) -DSTARPU_MPI_THREAD_CPUID=1 $STARPU_PRIME --ping_thread=first --bench=bandwidth > $sf/bandwidth_thread_first_main_last_mpi_first_$((i-1))_threads.out;
                done

                date

                echo "Thread first, main bound first, MPI thread last"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=2- -DSTARPU_MAIN_THREAD_CPUID=1 -DSTARPU_MPI_THREAD_CPUID=$((ncores-1)) $STARPU_PRIME --ping_thread=first --bench=bandwidth > $sf/bandwidth_thread_first_main_first_mpi_last_$((i-1))_threads.out;
                done

                date

                echo "Thread first, main bound first, MPI thread first"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=3- -DSTARPU_MAIN_THREAD_CPUID=1 -DSTARPU_MPI_THREAD_CPUID=2 $STARPU_PRIME --ping_thread=first --bench=bandwidth > $sf/bandwidth_thread_first_main_first_mpi_first_$((i-1))_threads.out;
                done

                date

                echo "Thread last, main bound last, MPI thread last"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=0- -DSTARPU_MAIN_THREAD_CPUID=$((ncores-3)) -DSTARPU_MPI_THREAD_CPUID=$((ncores-2)) $STARPU_PRIME --ping_thread=last --bench=bandwidth > $sf/bandwidth_thread_last_main_last_mpi_last_$((i-1))_threads.out;
                done

                date

                echo "Thread last, main bound last, MPI thread first"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=1- -DSTARPU_MAIN_THREAD_CPUID=$((ncores-2)) -DSTARPU_MPI_THREAD_CPUID=0 $STARPU_PRIME --ping_thread=last --bench=bandwidth > $sf/bandwidth_thread_last_main_last_mpi_first_$((i-1))_threads.out;
                done

                date

                echo "Thread last, main bound first, MPI thread last"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=1- -DSTARPU_MAIN_THREAD_CPUID=0 -DSTARPU_MPI_THREAD_CPUID=$((ncores-2)) $STARPU_PRIME --ping_thread=last --bench=bandwidth > $sf/bandwidth_thread_last_main_first_mpi_last_$((i-1))_threads.out;
                done

                date

                echo "Thread last, main bound first, MPI thread first"
                for i in $(seq 2 $((ncores-2)));
                do
                    echo $i;
                    $RUNMPI -DSTARPU_NCPU=$i -DSTARPU_MAIN_THREAD_BIND=1 -DSTARPU_WORKERS_CPUID=2- -DSTARPU_MAIN_THREAD_CPUID=0 -DSTARPU_MPI_THREAD_CPUID=1 $STARPU_PRIME --ping_thread=last --bench=bandwidth > $sf/bandwidth_thread_last_main_first_mpi_first_$((i-1))_threads.out;
                done
date
